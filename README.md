# WEB-T Universal plugin Translation Hub Frontend
This project is frontend application for WEB-T Universal plugin Translation Hub. Application is developed with Angular and can be run locally with `ng serve` command.

## Docker

Dockerfile is prepared and stored in root of application and it has some parameters to successfully build it.

### Docker args

**BUILD_ID** - pass this to differentiate versions and avoid caching static files.

### Docker ENV variables

**API_URL** - api base url.
**SUPPORT_EMAIL** - support email, it gets shown in some notification messages.

