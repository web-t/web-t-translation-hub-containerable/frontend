# stage 1 - prepare build image
FROM node:18 AS app-build
ARG JQ_VERSION=1.5
RUN wget --no-check-certificate https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -O /usr/bin/jq
RUN chmod +x /usr/bin/jq

# stage 2 - build app
FROM app-build as release
ARG BUILD_ID

WORKDIR /app

# Download JS widget
ARG WT_WIDGET_URL=https://unpkg.com/@tilde-nlp/website-translator/dist/widget.js
ARG WT_WIDGET_PATH=/app/src/assets/widget.js

ADD $WT_WIDGET_URL $WT_WIDGET_PATH
RUN chmod 765 $WT_WIDGET_PATH

# Install NPM dependencies
COPY package-lock.json .
COPY package.json .
RUN npm config set maxsockets=5
RUN npm install
RUN rm -f .npmrc

# Build app
COPY . .

# Change version to build id
# This needs to be before build step because version.json is imported in typescript and it must be set at buildtime.
RUN echo "{\"version\": \"$BUILD_ID\"}" > src/assets/version.json

RUN npm run build

# stage 3 - publish app
FROM nginx:1-alpine

COPY --from=release /usr/bin/jq /usr/bin/jq
COPY --from=release /app/dist/web-t-frontend /usr/share/nginx/html
COPY nginx.default.conf /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /usr/bin/entrypoint.sh

ENTRYPOINT [ "/bin/sh", "/usr/bin/entrypoint.sh" ]
CMD ["nginx", "-g", "daemon off;"]
