#!/bin/sh

OVERRIDE=`cat <<JSON
{
  "apiUrl": "$API_URL",
  "support": {
    "supportEmail": "$SUPPORT_EMAIL"
  },
  "translationImport": {
    "maxTranslationImportFileSizeBytes": "$MAX_TRANSLATION_IMPORT_FILE_SIZE_BYTES"
  }
}
JSON
`

echo "Using override config:"
echo $OVERRIDE;

cd /usr/share/nginx/html/assets/
echo $OVERRIDE > override.json
jq '. * input' app-config.json override.json > app-config.json.tmp
mv app-config.json.tmp app-config.json

exec "$@"
