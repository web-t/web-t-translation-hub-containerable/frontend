/**
 * General website pretranslation progress status
 */
export const enum TranslationProgress {
    READY = "READY",
    IN_PROGRESS = "IN_PROGRESS",
    FAILED = "FAILED",
    SUCCESS = "SUCCESS"
}