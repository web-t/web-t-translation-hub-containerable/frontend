/**
 *
 */
export interface UrlStatus {
    /**
     * Website URL that is being pretranslated
     */
    url: string;
    /**
     * Pretranslation status, true = pretranslation success, false = pretranslation fail
     */
    translatedSuccessfully: boolean | null;
    /**
     * List of languages in which website url pretranslation has failed
     */
    failedLanguageList: string[];
}