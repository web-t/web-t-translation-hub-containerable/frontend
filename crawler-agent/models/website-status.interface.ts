import { UrlStatus } from './url-status.interface.js';
import { TranslationProgress } from "../enums/translation-progress.enum.js"

/**
 *
 */
export interface WebsiteStatus {
    /**
     * List of website URLs being pretranslated
     */
    urlList: UrlStatus[];
    /**
     * General website pretranslation status
     */
    generalStatus: TranslationProgress;
}