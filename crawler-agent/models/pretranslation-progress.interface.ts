import { TranslationProgress } from "../enums/translation-progress.enum.js";

/**
 * Pretranslation progress response model
 */
export interface PretranslationProgress {
    /**
     * URL amount already pretranslated
     */
    pretranslatedUrls: number;
    /**
     * Total website URL amount
     */
    totalUrls: number;
    /**
     * General pretranslation status
     */
    status: TranslationProgress;
}