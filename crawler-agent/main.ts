/* eslint-disable prefer-const */
/* eslint-disable jsdoc/require-jsdoc */
/* eslint-disable no-inner-declarations */
/* eslint-disable no-console */
import { getWebsiteUrlList, pretranslateWebsite, } from './helpers/crawlee.helper.js';
import { 
    writeUrlsToFile, 
    writeGeneralStatusToFile,
    updateUrlPretranslationStatus, 
    updateGeneralPretranslationStatus, 
    getWebsitePretranslationStatus, 
    getFailedUrlsOfConfiguration} 
from './helpers/data.helper.js';
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import { TranslationProgress } from './enums/translation-progress.enum.js';
import appConfig from './config/config.js';

const app = express();

const config = {
    name: 'crawler-agent',
    port: 5082,
    host: '0.0.0.0',
    // name: 'crawler-agent-local',
    // port: 5082,
    // host: 'localhost',
};
let serverIsReady = false;
   
app.use(cors()) 
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

/**
 * @param configurationId - string
 * @returns status - TranslationProgress
 */
app.post('/cancel-pretranslation', async function(req, res) {
    try {
        let { configurationId } = req.body;

        console.info(`Pretranslation cancellation request received, Configuration ID: ${configurationId}`);

        // resets pre-translation status back to READY and thus stops pre-translation process
        await updateGeneralPretranslationStatus(configurationId, TranslationProgress.READY);
    
        res.status(200).send();
    }
    catch (e) {
        res.status(500).send(`Could not cancel pre-translation, error: ${e}`);
    }
})

/**
 * @param configurationId - string
 * @param websiteDomain - string
 * @param targetLanguages - string[]
 * @returns status 200 - pretranslation has started
 * @returns status 500 - could not start pretranslation
 */
app.post('/pretranslate-website', async function(req, res) {
    console.info("Received pretranslate request");

    let { configurationId, websiteDomain, targetLanguages } = req.body;
    const targetLanguageList = (targetLanguages as string)?.split(",");

    if (!configurationId || !websiteDomain || !targetLanguages) {
        res.status(400).send("Could not start pretranslation of website due to missing request parameters")
        return; 
    }

    try {
        async function startPretranslation() {
            try {
                await writeGeneralStatusToFile(configurationId);
    
                const urls = await getWebsiteUrlList(configurationId, websiteDomain as string);
        
                console.info(`Crawled urls total: ${urls.length}`)
            
                await writeUrlsToFile(urls, configurationId);
    
                for (const url of urls) {
                    const pretranslationResult = await pretranslateWebsite(configurationId, url, targetLanguageList);        
                    try {
                        await updateUrlPretranslationStatus(configurationId, url, pretranslationResult);
                    }
                    catch (e) {
                        console.error(`Error updating pretranslation status of url ${url}. Error: ${e}`);
                        continue;
                    }
                }
    
                console.info(`Pretranslation has finished! Configuration ID: ${configurationId}`);
    
                await updateGeneralPretranslationStatus(configurationId, TranslationProgress.SUCCESS);
            }
            catch (e) {
                await updateGeneralPretranslationStatus(configurationId, TranslationProgress.FAILED);

                console.error(`Pretranslation has failed with error ${e}. Configuration ID: ${configurationId}`);
            }
        }
    
        const pretranslationStatus = await getWebsitePretranslationStatus(configurationId);

        if (pretranslationStatus.status !== TranslationProgress.READY) {
            res.status(500).send(`Could not start pretranslation due to pretranslation status being ${pretranslationStatus.status}`);
            return;
        }

        process.nextTick(startPretranslation);
    
        res.status(200).send(`Pretranslation for website ${websiteDomain} has started`);
    }
    catch (e) {
        console.error(`Pretranslation could not be started, error: ${e}. Configuration ID: ${configurationId}`);
        
        await updateGeneralPretranslationStatus(configurationId, TranslationProgress.FAILED);

        res.status(500).send(`Pretranslation of website ${websiteDomain} has failed`);
    }
})

/**
 * @param configurationId - string
 * @returns status 200 - pretranslation retry has started
 * @returns status 500 - could not start pretranslation retry
 */
app.post('/retry-pretranslation', async function (req, res) {
    console.info("Received pretranslate retry request");

    let { configurationId } = req.body;

    try {
        async function startPretranslationRetry() {
            try {
                await updateGeneralPretranslationStatus(configurationId, TranslationProgress.IN_PROGRESS);

                const urlListToRetry = await getFailedUrlsOfConfiguration(configurationId);
                
                console.info(`Ready to retry pretranslation of ${urlListToRetry.length} URLs`);
    
                for (const urlItem of urlListToRetry) {
                    const pretranslationResult = await pretranslateWebsite(configurationId, urlItem.url, urlItem.failedLanguageList);
                    try {
                        await updateUrlPretranslationStatus(configurationId, urlItem.url, pretranslationResult);
                    }
                    catch (e) {
                        console.error(`Error updating pretranslation status of url ${urlItem?.url}. Error: ${e}`);
                        continue;
                    }
                }
                console.info(`Pretranslation retry has finished! Configuration ID: ${configurationId}`);
    
                await updateGeneralPretranslationStatus(configurationId, TranslationProgress.SUCCESS);
            }
            catch (e) {
                await updateGeneralPretranslationStatus(configurationId, TranslationProgress.FAILED);
                
                console.error(`Pretranslation retry has failed with error ${e}. Configuration ID: ${configurationId}`);
            }
        }

        const pretranslationStatus = await getWebsitePretranslationStatus(configurationId);

        // pre-translation retry can be done only while status is not reset yet to READY
        if (pretranslationStatus.status !== TranslationProgress.SUCCESS) {
            res.status(500).send(`Could not start pre-translation retry due to pre-translation status being ${pretranslationStatus.status}`);
            return;
        }

        process.nextTick(startPretranslationRetry);
    
        res.status(200).send(`Pretranslation retry for configuration ${configurationId} has started`);
    }
    catch (e) {
        console.error(`Pretranslation retry has failed to start with error ${e}. Configuration ID: ${configurationId}`);
        
        await updateGeneralPretranslationStatus(configurationId, TranslationProgress.FAILED);

        res.status(500).send(`Pretranslation retry of configuration ${configurationId} has failed`);
    }
})

/**
 * @param configurationId - string
 * @returns status - TranslationProgress
 */
app.get('/pretranslation-status', async function(req, res) {
    try {
        const configurationId = req.query.configurationId as string;

        const status = await getWebsitePretranslationStatus(configurationId);
    
        res.status(200).send(JSON.stringify(status));
    }
    catch (e) {
        res.status(500).send(`Could not retrieve pretranslation status, error: ${e}`);
    }
})

/**
 * @param configurationId - string
 */
app.post('/reset-pretranslation-status', async function(req, res) {
    try {
        console.info('Pretranslation status reset request received');

        let { configurationId } = req.body;
    
        const pretranslationStatus = await getWebsitePretranslationStatus(configurationId);

        if (pretranslationStatus.status !== TranslationProgress.IN_PROGRESS) {
            await updateGeneralPretranslationStatus(configurationId, TranslationProgress.READY);

            res.status(200).send();
        }
        else {
            res.status(500).send("Could not reset pre-translation status, pre-translation is in progress!");
        }
    }
    catch (e) {
        res.status(500).send(`Could not reset pretranslation status, error: ${e}`);
    }
})

app.get('/health/live', async (_, res) => {
    let healthcheck = {
        uptime: process.uptime(),
        message: 'OK',
        timestamp: Date.now()
    };
    try {
        res.status(200).send(healthcheck);
    } catch (error) {
        healthcheck.message = error as string;
        res.status(503).send(healthcheck);
    }
});

app.get('/health/ready', async (_, res) => {
    try {
        if (serverIsReady) {
            res.status(200).send("SERVER IS READY");
        }
        else {
            res.status(503).send("SERVER IS NOT READY");
        }
    } catch (error) {
        res.status(503).send(`SERVER IS NOT READY, error: ${error}`);
    }
});

const server = app.listen(config.port, config.host, async function() {
    console.info(`${config.name} running on ${config.host}:${config.port}`);
    serverIsReady = true;
})
server.setTimeout(appConfig.serverTimeoutMs);