/* eslint-disable no-console */
import * as fs from 'fs/promises'
import { TranslationProgress } from "../enums/translation-progress.enum.js"
import { PretranslationProgress } from "../models/pretranslation-progress.interface.js"
import { UrlStatus } from '../models/url-status.interface.js'
import { WebsiteStatus } from '../models/website-status.interface.js'
import { cleanUpGuid } from './general.helper.js'

const getPretranslationProgressFilePath = (configurationId: string) => {
    return `./pretranslation-progress/${cleanUpGuid(configurationId)}.json`;
}

const directoryExists = async (directoryName: string) => {
    let dir;
    try {
        dir = await fs.opendir(directoryName);
        dir?.close();
        
        return true;
    } catch (error) {
        return false;
    }
}

// check whether pretranslation progress directory exists, if no, create it
const checkPretranslationProgressDirectory = async () => {
    const exists = await directoryExists('pretranslation-progress');
    if (!exists) {
        await fs.mkdir('pretranslation-progress');
        console.info('Directory pretranslation-progress has been created');
    }
}

const readFile = async (configurationId: string): Promise<WebsiteStatus | null> => {
    await checkPretranslationProgressDirectory();
    const fileExists = await verifyWebsiteProgressFileExists(configurationId);
    const filePath = getPretranslationProgressFilePath(configurationId);

    if (fileExists) {
        const data: WebsiteStatus = JSON.parse(await fs.readFile(filePath, 'utf-8'));

        return data;
    }

    return null;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const writeFile = async (configurationId: string, data: any) => {
    await checkPretranslationProgressDirectory();
    const filePath = getPretranslationProgressFilePath(configurationId);

    await fs.writeFile(filePath, JSON.stringify(data));
}

const verifyWebsiteProgressFileExists = async (configurationId: string) => {
    const filePath = getPretranslationProgressFilePath(configurationId);

    try {
        await fs.readFile(filePath, 'utf-8');
        return true;
    } catch (error) {
        console.warn(`Pretranslation file ${filePath} does not exist. Error: ${error}.`);
        return false;
    }
}

// returns overall status of website pretranslation
// for configurations which have never been pre-translated, the file won't exist, but they are ready for pre-translation
export const getWebsitePretranslationStatus = async (configurationId: string) => {
    const data = await readFile(configurationId);

    if (data) {
        const response: PretranslationProgress = {
            pretranslatedUrls: data.urlList.filter(url => url.translatedSuccessfully).length,
            totalUrls: data.urlList.length,
            status: data.generalStatus
        };

        return response;
    }
    else {
        return {
            pretranslatedUrls: 0,
            totalUrls: 0,
            status: TranslationProgress.READY
        }
    }
}

export const writeGeneralStatusToFile = async (configurationId: string) => {
    await checkPretranslationProgressDirectory();

    await writeFile(configurationId, {
        urlList: [],
        generalStatus: TranslationProgress.IN_PROGRESS
    });
}

// writes urls to pretranslation progress json file of website that is being pretranslated
export const writeUrlsToFile = async (urlList: string[], configurationId: string) => {
    await checkPretranslationProgressDirectory();
    const filePath = getPretranslationProgressFilePath(configurationId);

    const urlStatusList: UrlStatus[] = [];

    for (const url of urlList) {
        urlStatusList.push({
            url: url,
            translatedSuccessfully: null,
            failedLanguageList: []
        })
    }

    await writeFile(configurationId, {
        urlList: urlStatusList,
        generalStatus: TranslationProgress.IN_PROGRESS,
    });

    console.info(`Created domain pretranslation progress tracking file: ${filePath}`);
}

// updates pretranslation status for specific url of a website
export const updateUrlPretranslationStatus = async (configurationId: string, websiteUrl: string, pretranslationResult: boolean) => {
    const data = await readFile(configurationId);

    if (!data) {
        throw new Error("File does not exist or could not be read, data is null");
    }

    const urlIndex = data.urlList.findIndex(obj => obj.url === websiteUrl);
    
    if (urlIndex !== -1) {
        data.urlList[urlIndex].translatedSuccessfully = pretranslationResult;
    } else {
        console.warn(`URL ${websiteUrl} not found in progress tracking list.`);
    }

    await writeFile(configurationId, data);
}

export const updateUrlFailedLanguageList = async (configurationId: string, websiteUrl: string, languageCode: string) => {
    const data = await readFile(configurationId);

    if (!data) {
        throw new Error("File does not exist or could not be read, data is null");
    }

    const urlIndex = data.urlList.findIndex(obj => obj.url === websiteUrl);

    if (urlIndex !== -1) {
        if (!data.urlList[urlIndex].failedLanguageList.includes(languageCode)) {
            data.urlList[urlIndex].failedLanguageList?.push(languageCode);
        }
    } else {
        console.warn(`URL ${websiteUrl} not found in progress tracking list.`);
    }

    await writeFile(configurationId, data);
}

// updates general pretranslation status once pretranslation has finished or failed
export const updateGeneralPretranslationStatus = async (configurationId: string, pretranslationResult: TranslationProgress) => {
    const data = await readFile(configurationId);

    if (!data) {
        throw new Error("File does not exist or could not be read, data is null");
    }

    data.generalStatus = pretranslationResult;
    
    await writeFile(configurationId, data);
}

// returns url list UrlStatus[] of configuration urls for which pretranslation has failed
export const getFailedUrlsOfConfiguration = async (configurationId: string) => {
    const data = await readFile(configurationId);

    return data?.urlList.filter(url => !url.translatedSuccessfully) || [];
}