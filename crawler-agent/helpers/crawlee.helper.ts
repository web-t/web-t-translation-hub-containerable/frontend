/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
/* eslint-disable jsdoc/require-jsdoc */
import { Dataset } from '@crawlee/puppeteer';
import puppeteer from 'puppeteer';
import { RequestQueue, purgeDefaultStorages, CheerioCrawler, gotScraping } from 'crawlee';
import { Interception, RequestInterceptionManager } from 'puppeteer-intercept-and-modify-requests'
import appConfig from '../config/config.js';
import { cleanUpGuid } from './general.helper.js';
import { getWebsitePretranslationStatus, updateUrlFailedLanguageList } from './data.helper.js';
import { TranslationProgress } from '../enums/translation-progress.enum.js';


const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

// checks whether response status code on page load indicates success
const hasValidStatusCode = async (url: string) => {
    try {
        const response = await fetch(url);

        return response.ok;
    } catch (error) {
        return false;
    }
}

const getWebsiteUrlsByCrawling = async (configurationId: string, websiteUrl: string) => {
    const result: string[] = [];

    const requestQueue = await RequestQueue.open(cleanUpGuid(configurationId));

    const crawler = new CheerioCrawler({
        minConcurrency: appConfig.crawler.minConcurrency,
        maxConcurrency: appConfig.crawler.maxConcurrency,
        maxRequestsPerMinute: appConfig.crawler.maxRequestsPerMinute,
        maxRequestRetries: appConfig.crawler.maxRequestRetries,
        maxRequestsPerCrawl: appConfig.crawler.maxRequestsPerCrawl,
        requestQueue: requestQueue,
        async requestHandler({ request, enqueueLinks }) {
            const { body } = await gotScraping({ url: request.loadedUrl });
            const pageHasValidStatusCode = await hasValidStatusCode(request.loadedUrl);
            
            // if page does not contain definition of website translator, it cannot be pretranslated
            if (!body.includes('WebsiteTranslator')) {
                console.warn(`URL ${request.url} cannot be pretranslated, page does not contain widget setup script.`);
            }
            else if (!pageHasValidStatusCode) {
                console.warn(`URL ${request.url} cannot be pretranslated, the status code it returns is not successful.`);
            }
            // if page does not contain requested configuration id, it cannot be pretranslated
            else if (!body.includes(configurationId)) {
                console.warn(`URL ${request.url} cannot be pretranslated, page does not contain valid configuration ID.`);
                console.warn(`Website ${request.loadedUrl} cannot be pretranslated, page does not contain widget setup script.`)
            }
            // if page does not contain language selection menu, it cannot be pretranslated
            else if (!body.includes('class="website-translator"')) {
                console.warn(`Website ${request.loadedUrl} cannot be pretranslated, page does not contain language selection menu.`)
            }
            else {
                result.push(request.loadedUrl);
            }       
            await enqueueLinks();
        },
        async failedRequestHandler({ request }) {
            // This function is called when the crawling of a request failed too many times
            await Dataset.pushData({
                url: request.url,
                succeeded: false,
                errors: request.errorMessages,
            })
        },
    });

    try {
        await crawler.run([websiteUrl]);
    }
    catch (e) {
        throw new Error(`An error occurred while gathering website ${websiteUrl} urls: ${e}`);
    }
    finally {
        await purgeDefaultStorages();
        await crawler.requestQueue?.drop();
        await crawler.teardown();
    }

    return result;
}

export const getWebsiteUrlList = async (configurationId: string, websiteDomain: string) => {
    let urls: string[] = [];

    urls = await getWebsiteUrlsByCrawling(configurationId, websiteDomain);

    return Array.from(new Set(urls));
}

export const pretranslateWebsite = async (configurationId: string, websiteUrl: string, targetLanguages: string[]) => {
    try {
        // a flag that indicates whether URL pretranslation in at least one language has failed
        let noLanguagesFailed = true; 

        const browser = await puppeteer.launch({
            headless: true, 
            defaultViewport: null, 
            protocolTimeout: 900000,
            args: ['--no-sandbox'] 
        });
        const client: any = await browser.target().createCDPSession();
    
        for (const targLang of targetLanguages) {
            try {
                // checking status during pre-translation process, since pre-translation could have been cancelled
                const generalStatus = await getWebsitePretranslationStatus(configurationId);

                if (generalStatus.status === TranslationProgress.READY) {
                    console.info(`Pre-translation process has been cancelled, configuration ID: ${configurationId}`);

                    await browser.close();
                    break;
                }

                const page = await browser.newPage();
                page.setDefaultTimeout(0);
                page.setDefaultNavigationTimeout(0);
    
                const url = new URL(websiteUrl); 
                url.searchParams.set('lang', targLang.toLowerCase()); 
                const websiteUriWithLang = url.href;
    
                console.log(`Pretranslating url ${websiteUriWithLang}`);

                // this logic requires website translator setup to be available and visible in page source
                const interceptManager = new RequestInterceptionManager(client);
                const htmlBodyInterceptionConfig: Interception = {
                    urlPattern: websiteUriWithLang,
                    resourceType: "Document",
                    modifyResponse: ({ body }) => {                  
                        let modifiedBody = body ?? "";

                        const urlSearch = "WebsiteTranslator.Options.api.url";
                        const scriptEndingSearch = "</script>";

                        const indexOfUrl = modifiedBody.indexOf(urlSearch);
                        const scriptEndingIndex = modifiedBody.indexOf(scriptEndingSearch, indexOfUrl + 1);

                        const replacement = "WebsiteTranslator.Options.translation.translateWholePage = true;\n</script>";

                        // replacing first </script> tag right after website translator widget setup with replacement string
                        modifiedBody = modifiedBody.substring(0, scriptEndingIndex) 
                                        + replacement 
                                        + modifiedBody.substring(scriptEndingIndex + scriptEndingSearch.length);


                        return { body: modifiedBody }
                    },
                }
                await interceptManager.intercept(htmlBodyInterceptionConfig);
     
                await page.goto(websiteUriWithLang);
             
                const eventName = "translation-finished";
        
                await Promise.all([waitForEvent(page, eventName, appConfig.pretranslationTimeoutSec)]);
        
                // wait N seconds before launching page with next language
                await delay(appConfig.pretranslationDelay);
            } catch (error) {
                console.error(`Issue occured while pretranslating website in language ${targLang}: ${error}`);
                noLanguagesFailed = false;

                try {
                    await updateUrlFailedLanguageList(configurationId, websiteUrl, targLang);
                }
                catch (e) {
                    console.error(`Could not update url failed language list, error: ${e}`);
                }
            }
        }
    
        await browser.close();

        return noLanguagesFailed;
    }
    catch (error) {
        console.error(`Issue occured while pretranslating website ${websiteUrl}: ${error}`)
        for (const targLang of targetLanguages) {
            try {
                await updateUrlFailedLanguageList(configurationId, websiteUrl, targLang);
            }
            catch (e) {
                console.error(`Could not update url failed language list, error: ${e}`);
            }
        }
        
        return false;
    }
}

async function waitForEvent(page: puppeteer.Page, eventName: string, seconds: number) {
  const timeoutPromise = (timeout: number) => new Promise((_, reject) => setTimeout(reject, timeout * 1000, "timeout"));

  return Promise.race([
    page.evaluate((eventName) => {
        return new Promise<void>(function(resolve) {
            document.addEventListener(eventName, function() {
                resolve(); // resolves when the event fires
            });
        });
    }, eventName),
    timeoutPromise(seconds), // Timeout promise for n seconds
  ]);
}