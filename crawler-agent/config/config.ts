import dotenv from 'dotenv';

dotenv.config();

/**
 * Crawler agent configuration model
 */
interface Config {
    /**
     * 
     */
    serverTimeoutMs: number,
    /**
     * how many seconds to wait before launching website url with next pretranslation target language in the list
     */
    pretranslationDelay: number,
    /**
     * 
     */
    pretranslationTimeoutSec: number,
    /**
     *
     */
    crawler: {
        /**
         *
         */
        minConcurrency: number,
        /**
         *
         */
        maxConcurrency: number,
        /**
         *
         */
        maxRequestsPerMinute: number,
        /**
         *
         */
        maxRequestRetries: number,
        /**
         *
         */
        maxRequestsPerCrawl: number,
    }
}

const appConfig: Config = {
    serverTimeoutMs: Number(process.env.SERVER_TIMEOUT_MS),
    pretranslationDelay: Number(process.env.PRETRANSLATION_DELAY_MS),
    pretranslationTimeoutSec: Number(process.env.PRETRANSLATION_TIMEOUT_S),
    crawler: {
        minConcurrency: Number(process.env.CRAWLER_MIN_CONCURRENCY),
        maxConcurrency: Number(process.env.CRAWLER_MAX_CONCURRENCY),
        maxRequestsPerMinute: Number(process.env.CRAWLER_MAX_REQ_PER_MIN),
        maxRequestRetries: Number(process.env.CRAWLER_MAX_REQ_RETRIES),
        maxRequestsPerCrawl: Number(process.env.CRAWLER_MAX_REQ_PER_CRAWL),
    }
}

export default appConfig;