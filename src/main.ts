import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { ConfigHelper, CONFIG_TOKEN } from '@tilde-nlp/ngx-config';

// 2. Use the default export
import file from './assets/version.json';
const { version } = file;


// Load configuration before app has been bootstrapped, so that configuration is always available in angular
const configs = [`assets/app-config.json?v=${version}`];
// eslint-disable-next-line @typescript-eslint/no-explicit-any
ConfigHelper.fetchConfigs(configs).then((data: any) => {
  const mergedConfig = ConfigHelper.deepMerge(...data);
  mergedConfig.version = version;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (window as any)[CONFIG_TOKEN] = mergedConfig;

  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
});
