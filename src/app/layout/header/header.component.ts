import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subject, Subscription } from 'rxjs';
import { HeaderService } from 'src/app/services/header.service';
import { UiLanguageService } from 'src/app/layout/header/services/ui-language.service';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';
import { UiLanguage } from 'src/app/core/configuration/models/ui-languages.model';

/**
 * Represents the HeaderComponent.
 * This component displays the header section of the application.
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  userName: string;
  isMobile = false;
  chosenLang: UiLanguage;
  onLangChange: Subscription;
  userIsLoggedIn: boolean;

  readonly languages: Array<UiLanguage> = this.configService.config.uiLanguages;
  private readonly destroy$ = new Subject();

  /**
   * Constructor of the HeaderComponent.
   * @param translate The TranslateService to handle translation.
   * @param uiLanguageService The UiLanguageService to manage user interface languages.
   * @param headerService The HeaderService to provide header configurations.
   * @param configService The AppConfigService to retrieve application configurations.
   */
  constructor(
    public translate: TranslateService,
    private uiLanguageService: UiLanguageService,
    private headerService: HeaderService,
    private configService: AppConfigService
  ) { }

  /**
   * Gets the tab configuration for the header.
   * @returns The tab configuration.
   */
  get tabConfig() {
    return this.headerService.config;
  }

  /**
   * Gets the localized page title to display in the header.
   * @returns The localized page title.
   */
  get pageTitle() {
    return this.headerService.pageTitleLocalizationKey;
  }

  /**
   * Lifecycle hook called when the component is initialized.
   * Configures the default user interface language and subscribes to language change events.
   */
  ngOnInit(): void {
    this.uiLanguageService.configureDefaultLanguage();
    this.chosenLang = this.uiLanguageService.foundLang;

    this.onLangChange = this.translate.onLangChange.subscribe(() => {
      this.changeLanguage();
    });
  }

  /**
   * Changes the user interface language.
   * @param language The selected language to switch to.
   */
  changeLanguage(language?: UiLanguage): void {
    if (language) {
      this.translate.use(language.code);
      this.chosenLang = language;
    }

    this.uiLanguageService.updateLanguage();
    const lang = document.createAttribute('lang');
    lang.value = this.uiLanguageService.foundLang.code;
    document.querySelector('html')?.attributes.setNamedItem(lang);
  }

  /**
   * Lifecycle hook called when the component is destroyed.
   * Cleans up subscriptions to prevent memory leaks.
   */
  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
    this.onLangChange.unsubscribe();
  }

  /**
   * TrackBy function for ngFor to improve performance when rendering the list.
   * @param index The index of the item in the array.
   * @returns The index itself.
   */
  trackByFn(index: number): number {
    return index;
  }

}
