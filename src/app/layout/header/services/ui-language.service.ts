import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';

/**
 * Service responsible for managing the user interface language.
 * It provides methods to configure the default language and update the language setting.
 */
@Injectable({
  providedIn: 'root'
})
export class UiLanguageService {
  readonly languageLocalStorageKey = 'uiLanguage';
  uiLanguages = this.configService.config.uiLanguages;
  // eslint-disable-next-line jsdoc/require-jsdoc
  foundLang: { code: string; displayName: string; };
  readonly defaultLangCode = 'en';

  /**
   * Constructor of the UiLanguageService.
   * @param translate The TranslateService to handle translation.
   * @param configService The AppConfigService to retrieve application configurations.
   */
  constructor(
    public translate: TranslateService,
    private configService: AppConfigService
  ) { }

  /**
   * Configures the default user interface language.
   * It sets the foundLang property to the language in local storage or the user's browser language, or falls back to the second language in the list.
   * It also sets the default language for translation with ngx-translate and sets the user interface language to the found language.
   */
  configureDefaultLanguage() {
    // eslint-disable-next-line jsdoc/require-jsdoc
    this.foundLang = this.uiLanguages.find((language: { code: string | null; }) => language.code === this.getFromLocalStorage()) ?? this.uiLanguages.find((language: { code: string; }) => window.navigator.languages.includes(language.code)) ?? this.uiLanguages[0];
    this.translate.setDefaultLang(this.defaultLangCode);
    this.translate.use(this.foundLang.code);
  }

  /**
   * Updates the user interface language in local storage.
   */
  updateLanguage() {
    localStorage.setItem(this.languageLocalStorageKey, this.translate.currentLang);
  }

  /**
   * Retrieves the user interface language from local storage.
   * @returns The user interface language code stored in local storage.
   */
  getFromLocalStorage() {
    return localStorage.getItem(this.languageLocalStorageKey);
  }
}
