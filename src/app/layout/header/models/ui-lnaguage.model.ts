export interface UiLanguage {
    code: string,
    displayName: string
}