import { Component, OnDestroy, OnInit } from '@angular/core';
import { NotificationMessage, NotificationMessageType } from '@tilde-nlp/ngx-common';
import { openCloseAnimation } from 'src/assets/animations';
import { IntegrationDataService } from 'src/app/services/integration-data.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Subscription } from 'rxjs';
import { TranslationProvider } from 'src/app/features/translation-provider-settings/models/translation-provider.model';
import { Router } from '@angular/router';
import { ProviderService } from 'src/app/features/translation-provider-settings/services/provider.service';

/**
 * Represents the FeaturesComponent.
 * This component displays various features related to translation providers.
 */
@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss'],
  animations: [
    openCloseAnimation
  ]
})
export class FeaturesComponent implements OnInit, OnDestroy {
  messages: NotificationMessage[] = [];

  private readonly noProviderMessage: NotificationMessage = {
    body: 'TRANSLATION_PROVIDER.NOTIFICATION_REASON.NO_PROVIDER_SET',
    type: NotificationMessageType.WARNING
  };

  private readonly errorGettingProviderMessage: NotificationMessage = {
    body: 'TRANSLATION_PROVIDER.NOTIFICATION_REASON.ERROR_GETTING_PROVIDER',
    title: 'TRANSLATION_PROVIDER.NOTIFICATION_TITLE.ERROR_GETTING_PROVIDER',
    type: NotificationMessageType.ERROR
  }
  private notificationSubscription: Subscription;

  /**
   * Constructor of the component.
   * @param providerService The provider service.
   * @param notificationService The notification service used to show notifications.
   * @param integrationDataService The integration data service used to fetch integration data.
   * @param router The Angular router service used for navigation.
   */
  constructor(
    private providerService: ProviderService,
    private notificationService: NotificationService,
    private integrationDataService: IntegrationDataService,
    private router: Router
  ) { }

  /**
   * Checks if a translation provider is set.
   * @returns True if a translation provider is set, otherwise false.
   */
  get isProviderSet() {
    return this.providerService.isProviderSet;
  }

  /**
   * Lifecycle hook called when the component is initialized.
   * Subscribes to router events and fetches the translation provider configuration and integration data.
   * Also, subscribes to notification messages.
   */
  ngOnInit(): void {
    this.router.events.subscribe(() => {
      this.messages = [];
    });

    this.providerService.getConfiguration().subscribe({
      next: (data: TranslationProvider) => {
        if (!data.id) {
          this.notificationService.sendMessage(this.noProviderMessage);
        }
      },
      error: () => {
        this.notificationService.sendMessage(this.errorGettingProviderMessage);
      }
    });

    this.integrationDataService.loadIntegrations();
    this.subscribeNotifications();
  }

  /**
   * Event handler to close a notification message.
   * @param ix The index of the message to be closed.
   */
  onNotificationClose(ix: number) {
    this.messages.splice(ix, 1);
  }

  /**
   * Lifecycle hook called when the component is destroyed.
   * Cleans up subscriptions to prevent memory leaks.
   */
  ngOnDestroy() {
    this.notificationSubscription.unsubscribe();
  }

  /**
   * Private method to subscribe to notification messages.
   * Filters and adds unique messages to the messages array.
   */
  private subscribeNotifications() {
    this.notificationSubscription = this.notificationService.onMessage().subscribe(message => {

      const existingMessage = this.messages.find(msg => msg.title === message.title
        && msg.type === message.type
        && message.body === msg.body
        && JSON.stringify(msg.localizationParams) === JSON.stringify(message.localizationParams));

      if (!existingMessage) {
        this.messages.push(message);
      }
    })
  }

}
