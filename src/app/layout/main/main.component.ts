import { Component } from '@angular/core';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class MainComponent {
  readonly desktopOnlyDescription = 'DESKTOP_ONLY.DESKTOP_ONLY';

  /**
   * @param configService App configuration service
   */
  constructor(private readonly configService: AppConfigService) { }

  /**
   * Getter for the help center URL.
   * @returns The URL of the help center.
   */
  get helpCenterUrl() {
    return this.configService.config.support.helpCenterUrl;
  }

  /**
   * Getter for the about page URL.
   * @returns The URL of the about page.
   */
  get aboutPageUrl() {
    return this.configService.config.support.aboutPageUrl;
  }

}
