import { ClipboardModule } from '@angular/cdk/clipboard';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationMessageModule, SortTranslationsModule, TldLoaderModule } from '@tilde-nlp/ngx-common';
import { MatPaginatorGoToModule } from 'src/app/shared/components/mat-paginator-go-to/mat-paginator-go-to.module';
import { TaggedTextPipeModule } from 'src/app/shared/pipes/tagged-text-pipe/tagged-text-pipe.module';
import { ConfiguratorComponent } from './components/configurator/configurator.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { DetailsComponent } from './components/details/details.component';
import { EngineSelectorComponent } from './components/engine-selector/engine-selector.component';
import { TranslationEditorComponent } from './components/translation-editor/translation-editor.component';
import { WebsiteTranslatorListComponent } from './components/website-translator-list/website-translator-list.component';
import { PretranslationDialogComponent } from './components/pretranslation-dialog/pretranslation-dialog.component';
import { TranslationExportDialogComponent } from './components/translation-export-dialog/translation-export-dialog.component';
import { TranslationImportDialogComponent } from './components/translation-import-dialog/translation-import-dialog.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ActionStatusNotificationComponent } from './components/action-status-notification/action-status-notification.component';

@NgModule({
  declarations: [
    WebsiteTranslatorListComponent,
    ConfiguratorComponent,
    TranslationEditorComponent,
    ConfirmDialogComponent,
    DetailsComponent,
    EngineSelectorComponent,
    PretranslationDialogComponent,
    TranslationExportDialogComponent,
    TranslationImportDialogComponent,
    ActionStatusNotificationComponent
  ],
  imports: [
    CommonModule,
    ClipboardModule,
    FlexLayoutModule,
    FormsModule,
    MatChipsModule,
    MatCheckboxModule,
    MatDialogModule,
    NotificationMessageModule,
    ReactiveFormsModule,
    RouterModule,
    SortTranslationsModule,
    TaggedTextPipeModule,
    TranslateModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTooltipModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorGoToModule,
    TldLoaderModule,
    MatTabsModule,
    MatProgressBarModule
  ],
  exports: [WebsiteTranslatorListComponent]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class WebsiteTranslatorModule { }
