import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

/**
 * Confirmation dialog component for deleting integration.
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  /**
   * @param dialogRef MatDialogRef.
   * @param data MAT_DIALOG_DATA received from parent component.
   * @param data.id Id of integration which will be deleted.
   */
  // eslint-disable-next-line jsdoc/require-jsdoc
  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: { id: string }) { }
}
