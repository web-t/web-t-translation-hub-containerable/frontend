/* eslint-disable jsdoc/require-jsdoc */
import { TrgLangWithDomains } from "./trglang-with-domains.model";

export interface LanguageDirectionsBySrcLang {
    srcLang: string,
    trgLangInfo: Array<TrgLangWithDomains>
}
