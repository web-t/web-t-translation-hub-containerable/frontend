/* eslint-disable jsdoc/require-jsdoc */
import { Language } from "./language.model";

/** Website integration model. */
export interface Website {
  id?: string;
  name?: string;
  srcLang: string;
  languages: Array<Language>;
  siteUrls: Array<string>
}
