/* eslint-disable jsdoc/require-jsdoc */
export interface TrgLangWithDomains {
  domains: Array<string>,
  trgLang: string,
}
