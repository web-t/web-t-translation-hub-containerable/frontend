/* eslint-disable jsdoc/require-jsdoc */
export interface Language {
  trgLang: string
  domain: string
}
