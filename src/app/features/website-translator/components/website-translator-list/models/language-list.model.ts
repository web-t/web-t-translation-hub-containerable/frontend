/* eslint-disable jsdoc/require-jsdoc */
export interface LanguageList {
  [key: string]: Array<string>;
}
