import { Language } from "./language.model";

export interface WebsiteWithId {
  id: string;
  name?: string;
  srcLang: string;
  languages: Array<Language>;
  siteUrls: Array<string>
}