import { System } from "./system.model";

/** Language directions response from api. */
export interface LanguageDirectionResponse {
  // eslint-disable-next-line jsdoc/require-jsdoc
  languageDirections: System[];
}
