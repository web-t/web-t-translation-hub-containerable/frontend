/** Frontend model for displaying integrations in list */
export interface WidgetListItem {
  /** Id */
  id?: string;
  /** Domains concatenated with comma */
  domainsString: string;
  /** Created at date */
  createdAt?: Date;
  /** Integration name */
  name?: string,
  /** Wether integration has some invalid config */
  isMissingLanguageOrDomain?: boolean
}
