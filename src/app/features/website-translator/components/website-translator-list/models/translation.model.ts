/* eslint-disable jsdoc/require-jsdoc */
import { SegmentTranslationOrigin } from "../../translation-editor/models/segment-translation-origin.enum";
import { SegmentTranslationStatus } from "../../translation-editor/models/segment-translation-status.enum";

/** Translation model returned from API. */
export interface Translation {
  id: number;
  text: string; //suggested text
  language: string;
  status: SegmentTranslationStatus;
  origin: SegmentTranslationOrigin;
}
