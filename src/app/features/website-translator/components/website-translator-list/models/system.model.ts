/* eslint-disable jsdoc/require-jsdoc */
/** Single language direction model from API. */
export interface System {
  domain: string,
  srcLang: string,
  trgLang: string,
  providerType: string
}
