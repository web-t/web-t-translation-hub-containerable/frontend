/* eslint-disable jsdoc/require-jsdoc */
import { TrgLangWithDomains } from "./trglang-with-domains.model"

export interface DomainsForTrgLangWithId {
  id?: string,
  domains: TrgLangWithDomains[]
}
