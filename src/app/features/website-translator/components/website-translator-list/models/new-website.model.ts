export interface NewWebsite {
  name: string
  sourceLanguage: string;
  targetLanguages: Array<string>;
  domains: Array<string>;
}
