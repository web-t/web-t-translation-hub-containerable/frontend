/* eslint-disable jsdoc/require-jsdoc */
import { Website } from "./website.model";
/** List of website integrations. */
export interface WebsiteList {
  configurations: Array<Website>
}
