/* eslint-disable jsdoc/require-jsdoc */
import { Language } from "./language.model";

/** Missing language from provider model. */
export interface MissingLanguage {
  id: string;
  missingLanguages: string[];
  missingDomainElementArray: Language[]
}
