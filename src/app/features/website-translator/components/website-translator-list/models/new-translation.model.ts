/* eslint-disable jsdoc/require-jsdoc */
/**
 * New translation API model.
 */
export interface NewTranslation {
  text: string;
  URL: string;
}
