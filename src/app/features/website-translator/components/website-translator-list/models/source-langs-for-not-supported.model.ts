/* eslint-disable jsdoc/require-jsdoc */
export interface SourceLangsForNotSupported {
    srcLang: string,
    trgLangs: string[]
}
