export interface WebsiteUpdate {
  name: string;
  targetLanguages: Array<string>;
  domains: Array<string>;
}
