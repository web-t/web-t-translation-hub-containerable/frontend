import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteTranslatorListComponent } from './website-translator-list.component';

describe('WebsiteTranslatorListComponent', () => {
  let component: WebsiteTranslatorListComponent;
  let fixture: ComponentFixture<WebsiteTranslatorListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebsiteTranslatorListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WebsiteTranslatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
