import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { IntegrationDataService } from 'src/app/services/integration-data.service';
import { LanguageAvailabilityService } from 'src/app/services/language-availability.service';
import { openCloseAnimation } from 'src/assets/animations';
import { WidgetListItem } from './models/widget-list-item.model';


/**
 * Represents the WebsiteTranslatorListComponent.
 * This component displays a list of website integrations and their configurations.
 */
@Component({
  selector: 'app-website-translator-list',
  templateUrl: './website-translator-list.component.html',
  styleUrls: ['./website-translator-list.component.scss'],
  animations: [
    openCloseAnimation
  ]
})

export class WebsiteTranslatorListComponent implements OnInit, OnDestroy {
  widgetList: Array<WidgetListItem>;
  loading = true;

  private readonly destroy$ = new Subject();


  /**
   * Constructor of the component.
   * @param integrationDataService The integration data service used to fetch data.
   * @param configService The configuration service used to fetch app configuration.
   * @param notificationService The notification service used to show notifications.
   * @param languageAvailabilityService The language availability service used to fetch language data.
   */
  constructor(
    private readonly integrationDataService: IntegrationDataService,
    private notificationService: NotificationService,
    private languageAvailabilityService: LanguageAvailabilityService
  ) { }

  /**
   * Lifecycle hook called when the component is initialized.
   * Fetches the integrations data when the component is initialized.
   */
  ngOnInit(): void {
    this.getIntegrations();
  }

  /**
   * Lifecycle hook called when the component is destroyed.
   * Cleans up subscriptions to prevent memory leaks.
   */
  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  /**
   * Private method to fetch the integrations data from the service.
   * Updates the widgetList based on the retrieved data.
   */
  getIntegrations() {
    this.languageAvailabilityService.getIntegrations().subscribe((result) => {
      this.loading = this.integrationDataService.loading;
      if (this.integrationDataService.isError) {
        this.notificationService.sendDefaultErrorMessage();
      } else {
        const websiteIntegrations = result.websiteIntegrations;
        const notSupportedLanguagesOrDomains = result.notSupportedLanguagesOrDomains;

        this.widgetList = websiteIntegrations.configurations.map((item) => ({
          id: item.id,
          name: item.name,
          domainsString: item.siteUrls.join(', '),
          isMissingLanguageOrDomain: notSupportedLanguagesOrDomains.some(element => element.id === item.id),
        }));
      }
    })
  }

}
