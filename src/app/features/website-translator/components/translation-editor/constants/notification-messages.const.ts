import { NotificationMessageType } from "@tilde-nlp/ngx-common";

export const NotificationMessages = {
    loadingErrorMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_DEFAULT',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_LOAD',
        type: NotificationMessageType.ERROR
    },

    notFoundDeleteMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_DELETE_NOT_FOUND',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_DELETE',
        type: NotificationMessageType.ERROR
    },

    deleteErrorMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_DEFAULT',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_DELETE',
        type: NotificationMessageType.ERROR
    },

    defaultErrorAccept: {
        body: 'WIDGET.TEXT_EDIT.ERROR_DEFAULT',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_ACCEPT',
        type: NotificationMessageType.ERROR
    },

    notFoundAcceptErrorMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_ACCEPT_NOT_FOUND',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_ACCEPT',
        type: NotificationMessageType.ERROR
    },

    addErrorDefaultMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_DEFAULT',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_ADD',
        type: NotificationMessageType.ERROR
    },

    badRequestErrorMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_ADD_BAD_REQUEST',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_ADD',
        type: NotificationMessageType.ERROR
    },

    addNotFoundErrorMessage: {
        body: 'WIDGET.TEXT_EDIT.ERROR_ADD_NOT_FOUND',
        title: 'WIDGET.TEXT_EDIT.ERROR_TITLE_ADD',
        type: NotificationMessageType.ERROR
    },
    reviewTemplateMessage: {
        body: 'TRANSLATION_PROVIDER.NOTIFICATION_REASON.NOT_SUPPORTING_ALL_LANGUAGE_PAIRS',
        title: 'TRANSLATION_PROVIDER.NOTIFICATION_TITLE.REVIEW_TEMPLATE',
        type: NotificationMessageType.WARNING
    },
    translationExportErrorMessage: {
        body: 'TRANSLATION_EXPORT.EXPORT_ERROR_BODY',
        title: 'TRANSLATION_EXPORT.EXPORT_ERROR_TITLE',
        type: NotificationMessageType.ERROR
    },
    translationImportErrorMessage: {
        body: 'TRANSLATION_IMPORT.IMPORT_ERROR_BODY',
        title: 'TRANSLATION_IMPORT.IMPORT_ERROR_TITLE',
        type: NotificationMessageType.ERROR
    },
    pretranslationCouldNotStartMessage: {
        body: 'PRETRANSLATION.NOTIFICATIONS.STARTED_ERROR_BODY',
        title: 'PRETRANSLATION.NOTIFICATIONS.STARTED_ERROR_TITLE',
        type: NotificationMessageType.ERROR
    },
    pretranslationRetryErrorMessage: {
        body: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_RETRY_ERROR_BODY',
        title: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_RETRY_ERROR_TITLE',
        type: NotificationMessageType.ERROR
    },
    pretranslationStatusRetrieveErrorMessage: {
        body: 'PRETRANSLATION.NOTIFICATIONS.STATUS_RETRIEVE_ERROR_BODY',
        title: 'PRETRANSLATION.NOTIFICATIONS.STATUS_RETRIEVE_ERROR_TITLE',
        type: NotificationMessageType.ERROR
    },
    importStatusRetrieveErrorMessage: {
        title: 'TRANSLATION_IMPORT.NOTIFICATIONS.STATUS_RETRIEVE_ERROR_TITLE',
        body: 'TRANSLATION_IMPORT.NOTIFICATIONS.STATUS_RETRIEVE_ERROR_BODY',
        type: NotificationMessageType.ERROR
    },
    pretranslationCancelErrorMessage: {
        body: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_CANCEL_ERROR_BODY',
        title: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_CANCEL_ERROR_TITLE',
        type: NotificationMessageType.ERROR
    }
}