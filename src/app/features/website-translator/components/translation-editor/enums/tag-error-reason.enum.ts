/**
 *  Tag error reason.
 */
export enum TagErrorReason {
  INSERT_ALL = 'insert-all',
  TAG_ORDER = 'tag-order',
  OVERLAP = 'tag-overlap'
}
