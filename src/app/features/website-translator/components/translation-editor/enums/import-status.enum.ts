/**
 *  Translation import status
 */
export enum ImportStatus {
    READY = 0,
    IN_PROGRESS = 1,
    SUCCESS = 2,
    FAIL = 3
}