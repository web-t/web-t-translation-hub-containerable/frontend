/**
 *  Website pretranslation status
 */
export enum PretranslationStatus {
    READY = "READY",
    IN_PROGRESS = "IN_PROGRESS",
    FAILED = "FAILED",
    SUCCESS = "SUCCESS"
}
  