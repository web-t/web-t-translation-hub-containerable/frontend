/* eslint-disable jsdoc/require-jsdoc */
/** Segment list filter model, special for translation export filtering. */
export interface SegmentListFilterForExport {
    origin?: number;
    status?: number;
    seo?: boolean;
    targetLanguages?: string[];
}