/* eslint-disable jsdoc/require-jsdoc */
/** Segment list filter model. */
export interface SegmentListFilter {
  [key: string]: string[] | string | undefined | number | boolean;
  lang: string;
  origin?: number;
  status?: number;
  text?: string;
  url?: string;
  seo?: boolean;
  from: number;
  count: number;
}
