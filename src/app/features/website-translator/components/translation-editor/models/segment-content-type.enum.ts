/** Segment content type. */
export enum SegmentContentType {
  Text,
  SEO,
  All
}
