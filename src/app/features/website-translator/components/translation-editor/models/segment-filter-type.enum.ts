/** Segment filter type. */
export enum SegmentFilterType {
  CONTENT_TYPE = "CONTENT_TYPE",
  ORIGIN = "ORIGIN",
  STATUS = "STATUS"
}
