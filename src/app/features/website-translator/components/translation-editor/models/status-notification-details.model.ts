/**
 * Details of status notification, whether for translation import or website pretranslation
 */
export interface StatusNotificationDetails {
    /**
     * Notification title
     */
    title: string;
    /**
     * Notification body content
     */
    body?: string;
    /**
     * Process (import or pretranslation) result type
     */
    isSuccess: boolean;
}