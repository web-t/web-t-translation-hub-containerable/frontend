/** Segment translation status */
export enum SegmentTranslationStatus {
  New,
  Confirmed,  // Reviewed/Accepted
  All
}
