/* eslint-disable jsdoc/require-jsdoc */
import { Translation } from "../../website-translator-list/models/translation.model";

/** Segment object model returned from API. */
export interface Segment {
  id: number;
  text: string; //source text
  uri: string;
  translations: Array<Translation>;
  meta: {
    seo: boolean,
    tag: string,
    attr: string,
    refAttr: string
  }
}
