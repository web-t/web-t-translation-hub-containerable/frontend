/** Translation origin */
export enum SegmentTranslationOrigin {
  MachineTranslation,
  User,   // User suggestion,
  All
}
