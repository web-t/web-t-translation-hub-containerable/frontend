import { Segment } from "./segment.model";

/** Segment response from API */
export interface SegmentResponse {
  /** Total number of segments. */
  totalSegments: number;
  /** Segments */
  segments: Array<Segment>;
}

