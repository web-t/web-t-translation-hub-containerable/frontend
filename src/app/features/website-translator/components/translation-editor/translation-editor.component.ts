
import { SelectionModel } from '@angular/cdk/collections';
import { HttpStatusCode } from '@angular/common/http';
import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { PageEvent } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, interval, Subject, Subscription, takeUntil } from 'rxjs';
import { openCloseAnimation, pulseAnimation } from 'src/assets/animations';
import { SuggestionEditor } from '@tilde-nlp/suggestion-editor';
import { SegmentContentType } from './models/segment-content-type.enum';
import { SegmentFilterType } from './models/segment-filter-type.enum';
import { SegmentListFilter } from './models/segment-list-filter.model';
import { SegmentResponse } from './models/segment-response.model';
import { SegmentTranslationOrigin } from './models/segment-translation-origin.enum';
import { SegmentTranslationStatus } from './models/segment-translation-status.enum';
import { Segment } from './models/segment.model';
import { TagErrorReason } from './enums/tag-error-reason.enum';
import { IntegrationDataService } from 'src/app/services/integration-data.service';
import { WebsiteTranslatorService } from 'src/app/services/website-translator.service';
import { NewTranslation } from '../website-translator-list/models/new-translation.model';
import { Translation } from '../website-translator-list/models/translation.model';
import { Website } from '../website-translator-list/models/website.model';
import { MatPaginatorGoToComponent } from 'src/app/shared/components/mat-paginator-go-to/mat-paginator-go-to.component';
import { NotificationMessage, NotificationMessageType } from '@tilde-nlp/ngx-common';
import { NotificationService } from 'src/app/core/services/notification.service';
import { NotificationMessages } from './constants/notification-messages.const';
import { TranslationExportDialogComponent } from '../translation-export-dialog/translation-export-dialog.component';
import { TranslationImportDialogComponent } from '../translation-import-dialog/translation-import-dialog.component';
import { PretranslationDialogComponent } from '../pretranslation-dialog/pretranslation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { PretranslationStatus } from './enums/pretranslation-status.enum';
import { ImportStatus } from './enums/import-status.enum';
import { StatusNotificationDetails } from './models/status-notification-details.model';
import { PretranslationStatusResponse } from '../pretranslation-dialog/models/pretranslation-status.response.model';


/**
 * Represents the Translation Editor component for managing website translations.
 */
@Component({
  selector: 'app-translation-editor',
  templateUrl: './translation-editor.component.html',
  styleUrls: ['./translation-editor.component.scss'],
  animations: [pulseAnimation, openCloseAnimation]
})
export class TranslationEditorComponent implements OnInit, OnDestroy {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginatorGoToComponent) paginatorGoTo: MatPaginatorGoToComponent;
  @ViewChild('saveButton') saveButton: MatButton;

  type: NotificationMessageType;
  filterOptions = [
    {
      name: SegmentFilterType.CONTENT_TYPE,
      options: [
        { title: 'ALL', value: SegmentContentType[SegmentContentType.All] },
        { title: 'TEXT', value: SegmentContentType[SegmentContentType.Text] },
        { title: 'SEO', value: SegmentContentType[SegmentContentType.SEO] }
      ]
    },
    {
      name: SegmentFilterType.ORIGIN,
      options: [
        { title: 'ALL', value: SegmentTranslationOrigin[SegmentTranslationOrigin.All] },
        { title: 'MT', value: SegmentTranslationOrigin[SegmentTranslationOrigin.MachineTranslation] },
        { title: 'USER', value: SegmentTranslationOrigin[SegmentTranslationOrigin.User] }
      ]
    },
    {
      name: SegmentFilterType.STATUS,
      options: [
        { title: 'ALL', value: SegmentTranslationStatus[SegmentTranslationStatus.All] },
        { title: 'NEW', value: SegmentTranslationStatus[SegmentTranslationStatus.New] },
        { title: 'CONFIRMED', value: SegmentTranslationStatus[SegmentTranslationStatus.Confirmed] }
      ]
    }
  ];
  currentLanguage: string;
  selection = new SelectionModel<Segment>(true, []);
  dataSource: Array<Segment>;
  sourceLanguage: string;
  targetLanguage: string;
  integrationDomains: Array<string> = [];
  targetLanguages: Array<string> = [];
  websiteId: string;
  activeSegment: Segment | undefined;
  activeSegmentTranslation: string | undefined;
  isLoadingResults = true;
  noTranslations = false;
  suggestionEditorComponentMap: Map<HTMLElement, SuggestionEditor>;
  pulseSaveBtn = false;
  resultCount = 0;
  filter: SegmentListFilter;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  searchText = "";
  textFilterChip = [
    { name: "text", value: "", localizationKey: "CONTAINS" },
    { name: "url", value: "", localizationKey: "URL" },
  ];
  filterChip = [
    { name: SegmentContentType[SegmentContentType.Text], active: false, localizationKey: "TEXT" },
    { name: SegmentContentType[SegmentContentType.SEO], active: false, localizationKey: "SEO" },
    { name: SegmentTranslationOrigin[SegmentTranslationOrigin.User], active: false, localizationKey: "USER" },
    { name: SegmentTranslationOrigin[SegmentTranslationOrigin.MachineTranslation], active: false, localizationKey: "MT" },
    { name: SegmentTranslationStatus[SegmentTranslationStatus.New], active: false, localizationKey: "NEW" },
    { name: SegmentTranslationStatus[SegmentTranslationStatus.Confirmed], active: false, localizationKey: "CONFIRMED" },
  ];

  //by default "All" options are selected
  selectedDropdownFilters: { [key: string]: string; } = {
    [SegmentFilterType.CONTENT_TYPE]: SegmentContentType[SegmentContentType.All],
    [SegmentFilterType.ORIGIN]: SegmentTranslationOrigin[SegmentTranslationOrigin.All],
    [SegmentFilterType.STATUS]: SegmentTranslationStatus[SegmentTranslationStatus.All],
  }
  tagWarnings: Array<string> = [];
  // eslint-disable-next-line jsdoc/require-jsdoc
  tagErrors: Array<{ localizationKey: string, tagNr: number }> = []
  isSuggestionEmpty = false;
  integrationName: string | undefined;

  importStatusNotificationDetails: StatusNotificationDetails;
  pretranslationStatusNotificationDetails: StatusNotificationDetails;

  pretranslationStatusResponse: PretranslationStatusResponse | null = null;
  allPretranslationStatuses = PretranslationStatus;
  pretranslationStatusFetchRetryCount = 0;

  importStatus: ImportStatus;
  allImportStatuses = ImportStatus;
  importStatusFetchRetryCount = 0;

  readonly columnsToDisplayHeader = ['select', 'source', 'target'];
  readonly columnsToDisplay = ['select', 'source', 'target', 'status'];
  readonly languageLocalizationKey = 'LANGUAGES.';
  readonly localizationKey = 'WIDGET.TEXT_EDIT.';
  readonly editContainerIdPrefix = 'target-text-';
  readonly helpUrl = 'https://website-translation.language-tools.ec.europa.eu/solutions/universal-plugin_en';

  private pulseCounter: number;
  private suggestionEditor: SuggestionEditor;
  private subscription: Subscription;
  private importStatusSubscription: Subscription | null = null;
  private pretranslationStatusSubscription: Subscription | null = null;
  private readonly maxPulseCount = 1;
  private readonly destroy$ = new Subject();
  private readonly lastTargetLanguageKey = 'lastUsedTargetLanguage:';

  private readonly loadingErrorMessage: NotificationMessage = NotificationMessages.loadingErrorMessage;
  private readonly notFoundDeleteMessage: NotificationMessage = NotificationMessages.notFoundDeleteMessage;
  private readonly deleteErrorMessage: NotificationMessage = NotificationMessages.deleteErrorMessage;
  private readonly defaultErrorAccept: NotificationMessage = NotificationMessages.defaultErrorAccept;
  private readonly notFoundAcceptErrorMessage: NotificationMessage = NotificationMessages.notFoundAcceptErrorMessage;
  private readonly addErrorDefaultMessage: NotificationMessage = NotificationMessages.addErrorDefaultMessage;
  private readonly badRequestErrorMessage: NotificationMessage = NotificationMessages.badRequestErrorMessage;
  private readonly addNotFoundErrorMessage: NotificationMessage = NotificationMessages.addNotFoundErrorMessage;
  private readonly pretranslationRetryErrorMessage: NotificationMessage = NotificationMessages.pretranslationRetryErrorMessage;
  private readonly pretranslationStatusRetrieveErrorMessage: NotificationMessage = NotificationMessages.pretranslationStatusRetrieveErrorMessage;
  private readonly importStatusRetrieveErrorMessage: NotificationMessage = NotificationMessages.importStatusRetrieveErrorMessage;
  private readonly pretranslationCancelErrorMessage: NotificationMessage = NotificationMessages.pretranslationCancelErrorMessage;
  
  /**
   * Creates an instance of TranslationEditorComponent.
   * @param websiteTranslatorService - The service for managing website translations.
   * @param integrationDataService - The service for integration data management.
   * @param dialog - Service to open Material Dialogs
   * @param route - The activated route containing information about the current route.
   * @param translate - The ngx-translate service for language translation.
   * @param notificationService - The service for displaying notifications.
   */
  constructor(
    private readonly websiteTranslatorService: WebsiteTranslatorService,
    private readonly integrationDataService: IntegrationDataService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private readonly notificationService: NotificationService
  ) { }

  /**
   * @returns segmentTranslationOrigin enum.
   */
  get segmentTranslationOrigin() {
    return SegmentTranslationOrigin;
  }

  /**
   *  @returns segment translation status enum.
   */
  get segmentTranslationStatus() {
    return SegmentTranslationStatus;
  }

  /**
   * Event listener for the 'document:suggestion-tag-error' custom event.
   * Handles tag error events in the suggestion editor.
   * @param event - The custom event containing tag error details.
   */
  @HostListener('document:suggestion-tag-error', ['$event']) tagErrorEvent(event: CustomEvent) {
    this.onTagErrorEvent(event);
  }

  /**
   * Event listener for the 'document:is-suggestion-empty' custom event.
   * Handles events indicating whether the suggestion in the editor is empty.
   * @param event - The custom event containing the suggestion emptiness status.
   */
  @HostListener('document:is-suggestion-empty', ['$event']) suggestionEmptyEvent(event: CustomEvent) {
    this.isSuggestionEmpty = event.detail.value;
  }

  /**
   * Initializes the component and subscribes to the ngx-translate language change event and integration list observable, handles the response.
   */
  ngOnInit(): void {
    this.subscription = this.translate.onLangChange.subscribe((lang) => {
      this.currentLanguage = lang.lang;
    })
    this.websiteId = this.route.snapshot.params['id'];
    this.suggestionEditorComponentMap = new Map();

    this.integrationDataService.integrationList$.pipe(takeUntil(this.destroy$))
      .subscribe((integrations: Array<Website>) => {
        const integration: Website | undefined = integrations.find(integration => integration.id === this.websiteId);
        if (integration) {
          const languages: string[] = [];
          integration.languages.forEach(language => languages.push(language.trgLang));

          this.sourceLanguage = integration.srcLang;
          this.integrationName = integration.name;
          this.targetLanguages = languages;
          this.integrationDomains = integration.siteUrls;
          this.filter = {
            lang: this.targetLanguage,
            from: 0,
            count: this.pageSize,
          }

          if (localStorage.getItem(this.lastTargetLanguageKey + this.websiteId)) {
            this.targetLanguage = localStorage.getItem(this.lastTargetLanguageKey + this.websiteId) ?? '';
          } else {
            this.targetLanguage = integration.languages[0].trgLang;
          }
          this.loadSegments();
        }
      });

    this.getPretranslationStatus();
    this.getImportStatus();
  }

  /**
   * Loads segments based on the selected language and filter options.
   */
  loadSegments() {
    this.onCancel();
    this.filter.lang = this.targetLanguage;
    this.filter.from = this.paginatorGoTo ? (this.paginatorGoTo.pageIndex * this.pageSize) : 0;
    this.filter.count = this.pageSize;



    this.isLoadingResults = true;
    this.suggestionEditorComponentMap.clear();

    this.websiteTranslatorService.getSegments(this.websiteId, this.filter).subscribe(
      {
        next: (data: SegmentResponse) => {
          this.resultCount = data.totalSegments;
          this.dataSource = data.segments;

          // if there are filtering results, but they are not returned from backend, we should go to page 1 and load filtered data
          if (this.resultCount > 0 && this.dataSource.length === 0) {
            this.filter.from = 0;
            this.paginatorGoTo.pageIndex = 0;
            this.paginatorGoTo.goTo = 1;
            this.loadSegments();
          }

          if (this.resultCount === 0
            && this.filter.status === undefined
            && this.filter.origin === undefined
            && this.filter.seo === undefined
            && (this.filter.text === '' || this.filter.text === undefined)
            && (this.filter.url === '' || this.filter.url === undefined)) {
            this.noTranslations = true;
          } else {
            this.noTranslations = false;
            // allow target text edit elements to appear in table
            setTimeout(() => {
              this.dataSource.forEach((segment: Segment) => {
                this.createSuggestion(segment.translations[0].text, segment.id)
              });
            }, 0);
          }
        },
        error: () => {
          this.dataSource = [];
          this.resultCount = 0;
          this.notificationService.sendMessage(this.loadingErrorMessage);
        },
        complete: () => {
          this.isLoadingResults = false;
        }
      }
    );
  }

  /**
   * Event handler for focusing on the editor when it receives focus.
   * @param e - The focus event object.
   */
  editorFocused(e: Event) {
    const datasetId = ((e.target as HTMLElement).closest(".suggestion-text") as HTMLElement).dataset.id as string;
    const focusedId = parseInt(datasetId);
    if (this.activeSegment && focusedId === this.activeSegment.id) {
      return;
    }
    const focusedSegment = this.dataSource.find(segment => segment.id === focusedId);

    if (focusedSegment) {
      this.onRowClick(focusedSegment)
    }
  }

  /**
   * Focuses on the editor when a row is clicked.
   * @param segment - The segment object representing the clicked row.
   */
  onRowClick(segment: Segment) {
    if (segment === this.activeSegment) {
      return;
    } else if (this.activeSegment && this.suggestionEditor.getSuggestion() !== this.activeSegmentTranslation) {
      //set caret in editor
      (document.getElementById(this.editContainerIdPrefix + this.activeSegment.id)?.firstChild?.firstChild as HTMLElement).focus();
      this.focusOnSave();
      return;
    } else if (this.activeSegment && this.filter.text) {
      this.suggestionEditor.highlightText(this.filter.text)
    }

    this.tagWarnings = [];
    this.tagErrors = [];
    this.activeSegment = segment;

    if (this.suggestionEditor) {
      this.suggestionEditor.disableEditing();
    }
    this.focusEditor();
    this.activeSegmentTranslation = this.suggestionEditor.getSuggestion()
  }

  /**
   * Focuses the editor for the active segment when a row is clicked.
   */
  focusEditor() {
    const editorElement = document.getElementById(this.editContainerIdPrefix + this.activeSegment?.id)?.firstChild as HTMLElement;
    this.suggestionEditor = this.suggestionEditorComponentMap.get(editorElement) as SuggestionEditor;
    this.suggestionEditor.enableEditing();

    if (editorElement !== document.activeElement) {
      (editorElement.firstChild as HTMLElement).focus();
    }
  }

  /**
   * Cancels the editing of a segment and restores the original suggestion.
   */
  onCancel() {
    if (this.activeSegment) {
      this.createSuggestion(this.activeSegment.translations[0].text, this.activeSegment.id)
    }
    this.activeSegment = undefined;
    this.activeSegmentTranslation = undefined;
  }

  /**
   * Handles the 'animationend' event of the animations.
   * Triggers the pulse effect for the Save button.
   */
  onAnimationEnd() {
    this.pulse();
  }

  /**
   * Called when the user clicks the 'Save' button to save the edited translation.
   * @param segment - The segment object representing the edited segment.
   */
  onSave(segment: Segment): void {
    const editedSuggestion = this.suggestionEditor.getSuggestion();

    const newTranslation: NewTranslation = {
      text: editedSuggestion,
      URL: segment.uri
    }

    segment.translations[0].origin = SegmentTranslationOrigin.User;
    segment.translations[0].text = editedSuggestion;

    this.websiteTranslatorService.addTranslation(this.websiteId, segment.id, this.targetLanguage, newTranslation).subscribe(
      {
        next: (data: Translation) => {
          segment.translations[0].id = data.id;
          this.onAccept(segment)
          this.createSuggestion(segment.translations[0].text, segment.id)
          this.activeSegment = undefined;
          this.activeSegmentTranslation = undefined;
        },
        error: (error) => {

          if (error.status === HttpStatusCode.NotFound) {
            this.notificationService.sendMessage(this.addNotFoundErrorMessage)
          } else if (error.status === HttpStatusCode.BadRequest) {
            this.notificationService.sendMessage(this.badRequestErrorMessage);
          } else {
            this.notificationService.sendMessage(this.addErrorDefaultMessage);
          }
        },
      }
    );
  }

  /**
   * Accepts the edited translation for a segment.
   * @param segment - The segment object representing the accepted segment.
   */
  onAccept(segment: Segment) {
    this.websiteTranslatorService.acceptTranslation(this.websiteId, segment.id, this.targetLanguage, segment.translations[0].id).subscribe(
      {
        next: () => {
          segment.translations[0].status = SegmentTranslationStatus.Confirmed;
        },
        error: (error) => {
          if (error.status === HttpStatusCode.NotFound) {
            this.notificationService.sendMessage(this.notFoundAcceptErrorMessage);
          } else {
            this.notificationService.sendMessage(this.defaultErrorAccept);
          }
        },
      }
    );
  }

  /**
   * Searches segments based on the entered text or URL filter.
   */
  searchByText() {
    this.onCancel();

    if (this.isTextUrl(this.searchText)) {
      this.filter.url = this.searchText
      this.updateChips("url", this.searchText)
    }
    else {
      this.filter.text = this.searchText;
      this.updateChips("text", this.searchText)
    }
    this.loadSegments();
  }

  /**
   * Handles the target language change event and loads segments for the new language.
   * @param newTargetLanguage - The new target language selected by the user.
   */
  onTargetChange(newTargetLanguage: string) {
    this.targetLanguage = newTargetLanguage;
    localStorage.setItem(this.lastTargetLanguageKey + this.websiteId, this.targetLanguage);
    this.paginatorGoTo.pageIndex = 0;
    this.paginatorGoTo.goTo = 1;
    this.loadSegments();
  }

  /**
   * Handles the 'menuOpened' event of the mat-menu.
   * Sets focus to the first radio button in the dropdown menu.
   */
  menuOpened() {
    const firstRadioFilter = Object.entries(this.selectedDropdownFilters)[0][1];
    (document.querySelector(`mat-radio-button input[value=${firstRadioFilter}]`) as HTMLElement).focus();
  }

  /**
   * Called when the filter option is selected for different segment properties.
   * @param filter - The type of filter being selected.
   * @param option - The selected option for the filter.
   */
  onFilterSelect(filter: string, option: string) {
    this.selectedDropdownFilters[filter] = option;

    this.onCancel();
    this.paginatorGoTo.pageIndex = 0;
    this.selection.clear();
    this.updateFilterChips();

    if (option in SegmentContentType && filter === SegmentFilterType.CONTENT_TYPE) {
      if (option === SegmentContentType[SegmentContentType.All]) {
        delete this.filter.seo;
      } else if (SegmentContentType[SegmentContentType.Text] === option) {
        this.filter.seo = false;
      } else if (SegmentContentType[SegmentContentType.SEO] === option) {
        this.filter.seo = true;
      }
    } else if (option in SegmentTranslationOrigin && filter === SegmentFilterType.ORIGIN) {
      if (option === SegmentTranslationOrigin[SegmentTranslationOrigin.All]) {
        delete this.filter.origin;
      } else {
        this.filter.origin = Object.values(SegmentTranslationOrigin).indexOf(option);
      }
    } else if (option in SegmentTranslationStatus && filter === SegmentFilterType.STATUS) {
      if (option === SegmentTranslationStatus[SegmentTranslationStatus.All]) {
        delete this.filter.status;
      } else {
        this.filter.status = Object.values(SegmentTranslationStatus).indexOf(option);
      }
    }

    this.loadSegments();
  }

  /**
   * Handles page change event for pagination.
   * @param pageEvent - The page change event emitted by the paginator.
   */
  handlePageChange(pageEvent: PageEvent) {
    if (this.activeSegment && this.suggestionEditor.getSuggestion() !== this.activeSegmentTranslation) {
      this.paginatorGoTo.paginator.pageIndex = pageEvent.previousPageIndex;
      this.paginatorGoTo.pageIndex = pageEvent.previousPageIndex as number;
      if (pageEvent.previousPageIndex) {
        this.paginatorGoTo.goTo = pageEvent.previousPageIndex + 1;
      }

      this.focusOnSave();
      return;
    }

    this.pageSize = pageEvent.pageSize;
    this.selection.clear();
    this.onCancel();
    this.loadSegments();
  }

  /**
   * Updates the text or URL filter chips based on the user input.
   * @param what - The type of filter (text or URL).
   * @param value - The filter value entered by the user.
   */
  updateChips(what: string, value: string) {
    const i = this.textFilterChip.findIndex(chip => {
      return chip.name == what;
    })
    this.textFilterChip[i].value = value;
  }

  //dropdow chips
  /**
   * Updates the filter chips based on the selected dropdown filters.
   */
  updateFilterChips() {
    // set all false  thne check to true
    this.filterChip.forEach(chip => {
      chip.active = false;
    })

    for (const key in this.selectedDropdownFilters) {
      const selectedFilter = this.selectedDropdownFilters[key];
      const i = this.filterChip.findIndex(chip => {
        return chip.name == selectedFilter;
      })
      if (i === -1) continue;
      this.filterChip[i].active = true;
    }
  }

  /**
   * Removes a filter chip based on the index or the removable chip object.
   * @param index - The index of the filter chip to be removed.
   * @param removableChip - The removable chip object representing the filter.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  removeChip(index: number, removableChip?: any) {
    if (removableChip) {
      const i = this.filterChip.findIndex(chip => {
        return chip.name == removableChip.name;
      })
      this.filterChip[i].active = false;

      for (const [key, value] of Object.entries(this.selectedDropdownFilters)) {
        if (removableChip.name === value) {
          if (removableChip.name in SegmentContentType) {
            this.onFilterSelect(key, SegmentContentType[SegmentContentType.All]);
          } else if (removableChip.name in SegmentTranslationOrigin) {
            this.onFilterSelect(key, SegmentTranslationOrigin[SegmentTranslationOrigin.All]);
          } else if (removableChip.name in SegmentTranslationStatus) {
            this.onFilterSelect(key, SegmentTranslationStatus[SegmentTranslationStatus.All]);
          }
        }
      }
    } else {
      delete this.filter[this.textFilterChip[index].name];
      this.textFilterChip[index].value = "";
      this.loadSegments()
    }
  }

  /**
   * Performs batch accept operation for selected segments.
   */
  batchAccept() {
    this.selection.selected.forEach((segment: Segment) => {
      this.onAccept(segment);
    })
    this.selection.clear()
  }

  /**
   * Performs batch delete operation for selected segments.
   */
  batchDelete() {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const observables: any = [];

    this.selection.selected.forEach((segment: Segment) => {
			const observable = this.websiteTranslatorService.deleteSegment(this.websiteId, segment.id);

			observables.push(observable);
    })

    forkJoin(observables)
    .subscribe({
      next: () => {
        this.selection.clear();
        
        this.paginatorGoTo.pageIndex = 0;
        this.paginatorGoTo.goTo = 1;
        this.loadSegments();
      },
      error: (error) => {
        if (error.status === HttpStatusCode.NotFound) {
          this.notificationService.sendMessage(this.notFoundDeleteMessage)
        } else {
          this.notificationService.sendMessage(this.deleteErrorMessage)
        }
      },
    });
  }

  /**
   * Deletes a single segment from the website translator service.
   * @param segment - The segment object representing the segment to be deleted.
   */
  segmentDelete(segment: Segment) {
    this.websiteTranslatorService.deleteSegment(this.websiteId, segment.id).subscribe(
      {
        next: () => {
          const indexToDelete = this.dataSource.findIndex(dataSourceSegment => dataSourceSegment.id === segment.id);
          this.dataSource.splice(indexToDelete, 1);
          this.table.renderRows();
        },
        error: (error) => {
          if (error.status === HttpStatusCode.NotFound) {
            this.notificationService.sendMessage(this.notFoundDeleteMessage)
          } else {
            this.notificationService.sendMessage(this.deleteErrorMessage)
          }
        },
      }
    );
  }

  /**
   * Checks whether all rows in the table are selected.
   * @returns True if all rows are selected, false otherwise.
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource);
  }

  /**
   * Clears the selection of all rows in the table.
   */
  clearSelection() {
    this.selection.clear();
  }

  /**
   * Handles the 'tag-error' event fired by the SuggestionEditor component.
   * Updates the tag warnings and errors based on the received event data.
   * @param event - The CustomEvent object containing the tag error data.
   */
  onTagErrorEvent(event: CustomEvent) {
    this.tagWarnings = [];
    this.tagErrors = [];

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    if (event.detail.find((x: any) => x.name == TagErrorReason.INSERT_ALL)) {
      this.tagWarnings.push(this.localizationKey + "INSERT_ALL_TAGS")
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const tagOrderProblems = event.detail.find((x: any) => x.name == TagErrorReason.TAG_ORDER)
    if (tagOrderProblems) {
      tagOrderProblems.tagIds.forEach((tagId: string) => {
        this.tagErrors.push({
          localizationKey: this.localizationKey + "TAG_ORDER_ERROR",
          tagNr: parseInt(tagId) + 1
        })
      })
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const tagOverlapProblems = event.detail.find((x: any) => x.name == TagErrorReason.OVERLAP)
    if (tagOverlapProblems) {
      tagOverlapProblems.tagIds.forEach((tagId: string) => {
        this.tagErrors.push({
          localizationKey: this.localizationKey + "TAG_OVERLAP_ERROR",
          tagNr: parseInt(tagId) + 1
        })
      })
    }
  }

  /**
   * Manages tab key navigation in the filter options dropdown.
   * @param filter - The selected filter option.
   * @param position - The position of the selected filter option in the filter list.
   * @param event - The event object representing the tab key press event.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  manageTabKeyNavigation(filter: any, position: number, event: Event) {
    if (filter !== this.filterOptions[position]) {
      event.stopPropagation();
    }
  }

  /**
   * Handles opening of pretranslation dialog
   */
  openPretranslationDialog() {
    const dialogRef = this.dialog.open(PretranslationDialogComponent, {
      data: {
        websiteId: this.websiteId,
        domainList: this.integrationDomains,
        targetLanguages: this.targetLanguages
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getPretranslationStatus();
    });
  }

  /**
   * Handles cancellation of pre-translation
   */
  cancelPretranslation() {
    this.websiteTranslatorService.cancelWebsitePretranslation(this.websiteId).subscribe({
      next: () => {
        this.getPretranslationStatus();
      },
      error: () => {
        this.notificationService.sendMessage(this.pretranslationCancelErrorMessage)
      },
    })
  }

  /**
   * Checks whether pre-translation status subscription is needed and starts it or ends it
   */
  checkPretranslationStatusSubscription() {
    if (this.pretranslationStatusResponse?.status === PretranslationStatus.IN_PROGRESS) {
      if (this.pretranslationStatusSubscription === null) {
        this.pretranslationStatusSubscription = interval(5000)
        .subscribe(() => {
          this.getPretranslationStatus();
        });
      }
    }
    else {
      this.pretranslationStatusSubscription?.unsubscribe();
    }
  }

  /**
   * Retrieves website pretranslation status
   */
  getPretranslationStatus() {
    this.websiteTranslatorService.getWebsitePretranslationStatus(this.websiteId).subscribe(
      {
        next: (response) => {
          this.pretranslationStatusFetchRetryCount = 0;
          this.pretranslationStatusResponse = response;

          const allUrlsPretranslated = 
            this.pretranslationStatusResponse.pretranslatedUrls === this.pretranslationStatusResponse.totalUrls
            && this.pretranslationStatusResponse.totalUrls > 0;

          if (this.pretranslationStatusResponse.status == PretranslationStatus.SUCCESS) {
            this.loadSegments();

            if (allUrlsPretranslated) {
              this.pretranslationStatusNotificationDetails = {
                title: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_SUCCESS_TITLE',
                body: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_SUCCESS_BODY',
                isSuccess: true
              }
            }
          }
          if (this.pretranslationStatusResponse.status == PretranslationStatus.FAILED) {
            this.pretranslationStatusNotificationDetails = {
              title: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_ERROR_TITLE',
              body: 'PRETRANSLATION.NOTIFICATIONS.PRETRANSLATION_ERROR_BODY',
              isSuccess: false
            }
          }

          this.checkPretranslationStatusSubscription();
        },
        error: () => {
          this.pretranslationStatusFetchRetryCount++;
          
          if (this.pretranslationStatusFetchRetryCount > 2) {
            this.notificationService.sendMessage(this.pretranslationStatusRetrieveErrorMessage)
          }
          else {
            this.getPretranslationStatus();
          }
        },
      }
    );
  }

  /**
   * Retry website pretranslation by pretranslating only failed URLs
   */
  retryWebsitePretranslation() {
    this.websiteTranslatorService.retryWebsitePretranslation(this.websiteId).subscribe({
      next: () => {
        this.getPretranslationStatus();
      },
      error: () => {
        this.notificationService.sendMessage(this.pretranslationRetryErrorMessage)
      },
    })
  }

  /**
   * Handles opening of translation import dialog form
   */
  importTranslations() {
    const dialogRef = this.dialog.open(TranslationImportDialogComponent, {
      data: {
        configId: this.websiteId
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getImportStatus();
    });
  }

  /**
   * Exports translations in XLIFF file format
   */
  exportTranslations() {
    this.dialog.open(TranslationExportDialogComponent, {
      data: {
        configId: this.websiteId
      },
      disableClose: true
    });
  }

  /**
   * Checks whether import status subscription is needed and starts it or ends it
   */
  checkImportStatusSubscription() {
    if (this.importStatus === ImportStatus.IN_PROGRESS) {
      if (this.importStatusSubscription === null) {
        this.importStatusSubscription = interval(3000)
        .subscribe(() => {
          this.getImportStatus();
        });
      }
    }
    else {
      this.importStatusSubscription?.unsubscribe();
    }
  }

  /**
   * Retrieves translation import status
   */
  getImportStatus() {
    this.websiteTranslatorService.getImportStatus(this.websiteId)      
    .subscribe({
      next: (response) => {
        this.importStatusFetchRetryCount = 0;
        this.importStatus = response;
        
        if (this.importStatus == ImportStatus.SUCCESS) {
          this.loadSegments();
        }
        if (this.importStatus == ImportStatus.SUCCESS || this.importStatus == ImportStatus.FAIL) {
          this.importStatusNotificationDetails = {
            title: this.importStatus == ImportStatus.FAIL ? 'TRANSLATION_IMPORT.NOTIFICATIONS.IMPORT_FAIL_TITLE' : 'TRANSLATION_IMPORT.NOTIFICATIONS.IMPORT_SUCCESS_TITLE',            
            isSuccess: this.importStatus == ImportStatus.SUCCESS
          }
        }

        this.checkImportStatusSubscription();
      },
      error: () => {
        this.importStatusFetchRetryCount++;
          
        if (this.importStatusFetchRetryCount > 2) {
          this.notificationService.sendMessage(this.importStatusRetrieveErrorMessage)
        }
        else {
          this.getImportStatus();
        }
      },
    })
  }

  /**
   * Returns whether import status notification should be visible
   * @returns boolean
   */
  showImportStatusNotification() {
    return this.importStatus === this.allImportStatuses.SUCCESS || this.importStatus === this.allImportStatuses.FAIL;
  }

  /**
   * Returns whether import status notification should be visible
   * @returns boolean
   */
  showPretranslationStatusNotification() {
    const pretranslationFail = this.pretranslationStatusResponse?.status === this.allPretranslationStatuses.FAILED;
    const pretranslationSuccess = this.pretranslationStatusResponse?.status === this.allPretranslationStatuses.SUCCESS;
    const pretranslationFinishedAllUrls = this.pretranslationStatusResponse?.totalUrls === this.pretranslationStatusResponse?.pretranslatedUrls;

    return pretranslationFail || (pretranslationSuccess && pretranslationFinishedAllUrls);
  }

  /**
   * Returns whether cancel pre-translation button should be visible
   * @returns boolean
   */
  cancelPretranslationButtonVisible() {
    return this.isPretranslationInProgress() && this.pretranslationStatusResponse && this.pretranslationStatusResponse.totalUrls > 0;
  }
  
  /**
   * Returns whether import is currently in progress
   * @returns boolean
   */
  isImportInProgress() {
    return this.importStatus === this.allImportStatuses.IN_PROGRESS;
  }

  /**
   * Returns whether website pre-translation is currently in progress
   * @returns boolean
   */
  isPretranslationInProgress() {
    return this.pretranslationStatusResponse?.status === this.allPretranslationStatuses.IN_PROGRESS;
  }

  /**
   * Returns whether retry pretranslation notification should be shown
   * @returns boolean
   */
  retryPretranslationNotificationVisible() {
    const pretranslationFinishedAllUrls = this.pretranslationStatusResponse?.totalUrls === this.pretranslationStatusResponse?.pretranslatedUrls;
    
    return (this.pretranslationStatusResponse?.status !== this.allPretranslationStatuses.READY 
            && !pretranslationFinishedAllUrls 
            && !this.isImportInProgress()
            && !this.isPretranslationInProgress()
            && !this.showPretranslationStatusNotification()
          );
  }

  /**
   * Dismisses import status notification and sets import status to READY, unsubscribes from status retrieval
   */
  dismissImportStatusNotification() {
    this.websiteTranslatorService.updateImportStatus(this.websiteId, ImportStatus.READY).subscribe({
      next: () => {
        this.getImportStatus();
        this.importStatusSubscription?.unsubscribe();
        this.importStatusSubscription = null;
      },
    })
  }

  /**
   * Dismisses pre-translation status notification and resets website pre-translation status to READY, unsubscribes from status retrieval
   */
  dismissPretranslationStatusNotification() {
    this.websiteTranslatorService.resetWebsitePretranslationStatus(this.websiteId).subscribe({
      next: () => {
        this.getPretranslationStatus();
        this.pretranslationStatusSubscription?.unsubscribe();
        this.pretranslationStatusSubscription = null;
      },
    })
  }

  /**
   * Returns how many of website URLs are pretranslated out of all in percentage format
   * @returns number 0-100
   */
  getPretranslationProgressPercentage() {
    if (this.pretranslationStatusResponse && this.pretranslationStatusResponse?.totalUrls > 0) {
      return Math.round(((this.pretranslationStatusResponse?.pretranslatedUrls / this.pretranslationStatusResponse?.totalUrls)) * 100);
    }

    return 100;
  }

  /**
   * Cleans up resources and unsubscribes from the ngx-translate language change event.
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.destroy$.next(null);
    this.destroy$.complete();
    this.importStatusSubscription?.unsubscribe();
    this.pretranslationStatusSubscription?.unsubscribe();
  }
  

  /**
   * Performs pulsing animation for the save button.
   */
  private pulse() {
    if (this.pulseCounter < this.maxPulseCount) {
      this.pulseSaveBtn = !this.pulseSaveBtn;
      this.pulseCounter += 1;
    }
  }

  /**
   * Focuses on the save button and triggers the pulsing animation.
   */
  private focusOnSave() {
    this.saveButton.focus();
    this.pulseCounter = 0;
    this.pulse();
  }

  /**
   * Creates a suggestion editor for a segment and replaces the target text edit element.
   * @param suggestionHTML - The HTML representing the suggestion.
   * @param segmentId - The ID of the segment.
   */
  private createSuggestion(suggestionHTML: string, segmentId: number) {
    const editor = new SuggestionEditor(
      suggestionHTML,
      false,
      undefined,
      true,
      false,
      'unusedTags-' + segmentId
    )

    const container = document.getElementById(this.editContainerIdPrefix + segmentId);
    const suggestion = editor.getDomRoot();

    (suggestion.firstChild as HTMLElement).addEventListener('focus', (e: Event) => this.editorFocused(e));
    container?.replaceChildren(suggestion);
    this.suggestionEditorComponentMap.set(suggestion, editor);

    if (this.filter.text) {
      editor.highlightText(this.filter.text);
    }

    editor.disableEditing();
  }

  /**
   * Checks whether the entered text is a URL or not.
   * @param text - The text to be checked.
   * @returns True if the text is a URL, false otherwise.
   */
  private isTextUrl(text: string): boolean {
    if (text.startsWith('/')) {
      return true;
    }
    // eslint-disable-next-line no-useless-escape
    const urlPattern = /\b((?:https?:(?:\/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!() \[\]{};: '".,<>?«»“”‘’])|(?:[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\b\/?(?!@)))/gi;
    return urlPattern.test(text);
  }
}
