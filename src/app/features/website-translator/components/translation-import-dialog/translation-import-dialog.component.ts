import { Component, Inject } from '@angular/core';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { WebsiteTranslatorService } from 'src/app/services/website-translator.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';
import { NotificationMessage } from '@tilde-nlp/ngx-common';
import { NotificationMessages } from '../translation-editor/constants/notification-messages.const';

/**
 * Component for handling import of XLIFF translation file
 */
@Component({
  selector: 'app-translation-import-dialog',
  templateUrl: './translation-import-dialog.component.html',
  styleUrls: ['./translation-import-dialog.component.scss']
})
export class TranslationImportDialogComponent {
  fileName: string;
  formData: FormData = new FormData();
  showSizeWarning = false;
  showEmptyFileWarning = false;
  maxAllowedFileSizeInMB: number;

  private readonly XLIFF_TRANSLATION_UNIT_TAG = "trans-unit";

  private readonly translationImportErrorMessage: NotificationMessage = NotificationMessages.translationImportErrorMessage;
  private readonly maxAllowedFileSize = this.configService.config.translationImport.maxTranslationImportFileSizeBytes;

    /**
     * @param dialogRef MatDialogRef.
     * @param data MAT_DIALOG_DATA received from parent component.
     * @param data.configId Id of website (configuration)
     * @param websiteTranslatorService The service for managing website translations.
     * @param notificationService The service for displaying notifications.
     * @param configService - The service providing access to the application's configuration.
     */
    // eslint-disable-next-line jsdoc/require-jsdoc
    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: { configId: string },
    private readonly websiteTranslatorService: WebsiteTranslatorService,
    private readonly notificationService: NotificationService,
    private readonly configService: AppConfigService,
  ) { }

  /**
   * XLIFF upload handler
   * @param fileInputEvent Event which takes place in the DOM
   */
  async xliffInputChange(fileInputEvent: Event) {
    const target = fileInputEvent.target as HTMLInputElement;
    const files = target.files as FileList;

    if (files.length > 0) {
      const file = files[0]
      this.fileName = file.name;

      const fileBuffer = await file.arrayBuffer();
      const newFile = new File([fileBuffer] as BlobPart[], file.name, {
        type: "application/xml"
      })

      const fileText = await file.text();

      if (newFile.size > this.maxAllowedFileSize) {
        this.maxAllowedFileSizeInMB = Number((this.maxAllowedFileSize / (1024 * 1024)).toFixed(2));
        this.showSizeWarning = true;
      } 
      else if (!fileText.includes(this.XLIFF_TRANSLATION_UNIT_TAG)) {
        this.showEmptyFileWarning = true;
      }
      else {
        this.showSizeWarning = false;
        this.showEmptyFileWarning = false;
        this.formData.append("xliffFile", newFile);
      }
    }
  }

  /**
   * Starts translation import process
   */
  startImportProcess() {
    this.formData.append("configId", this.data.configId);

    this.websiteTranslatorService.importTranslations(this.formData).subscribe({
        next: () => {
          this.dialogRef.close();
        },
        error: () => {
          this.notificationService.sendMessage(this.translationImportErrorMessage);
          this.dialogRef.close();
        }
      }
    )
  }
}
