import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationImportDialogComponent } from './translation-import-dialog.component';

describe('TranslationImportDialogComponent', () => {
  let component: TranslationImportDialogComponent;
  let fixture: ComponentFixture<TranslationImportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslationImportDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TranslationImportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
