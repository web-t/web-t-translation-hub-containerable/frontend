import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, UntypedFormArray, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NotificationMessage, NotificationMessageType } from '@tilde-nlp/ngx-common';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { ROUTE_MAP } from 'src/app/core/constants/route-map.const';
import { HeaderService } from 'src/app/services/header.service';
import { IntegrationDataService } from 'src/app/services/integration-data.service';
import { WebsiteTranslatorService } from 'src/app/services/website-translator.service';
import { BreadCrumbConfig } from 'src/app/shared/components/breadcrumb/models/breadcrumb-config';
import { openCloseAnimation } from 'src/assets/animations';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { Website } from '../website-translator-list/models/website.model';
import { NotificationService } from 'src/app/core/services/notification.service';
import { TrgLangWithDomains } from '../website-translator-list/models/trglang-with-domains.model';
import { LanguageAvailabilityService } from 'src/app/services/language-availability.service';


/**
 * Component responsible for configuring the website translator.
 */
@Component({
  selector: 'app-configurator',
  templateUrl: './configurator.component.html',
  styleUrls: ['./configurator.component.scss'],
  animations: [
    openCloseAnimation
  ]
})
export class ConfiguratorComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChildren('domainField') domainField!: QueryList<ElementRef>;

  readonly enteredWebsiteName = "enteredWebsiteName";
  readonly enteredDomains = "enteredDomains";
  readonly selectedSrcLang = "selectedSrcLang";
  readonly selectedTrgLanguages = "selectedTrgLanguages";

  readonly origin = window.location.origin;

  currentLanguage: string;
  sourceLangList: Array<string> = [];
  targetLangList: Array<string> = [];
  generalIntegrationResultScript: string;
  generalIntegrationCode1: string;
  clientID: string;
  isEditMode: boolean;
  editorUrl: string;
  widgetLocationUrl: string;
  widgetForm: UntypedFormGroup;
  selectedTargetLanguages: Array<TrgLangWithDomains>;
  previousSelectedTrgLanguages: string[] = [];
  widgetFormSubscription: Subscription;


  private readonly successMessage: NotificationMessage = {
    title: 'WTW.UPDATE_SUCCESS_MESSAGE_TITLE',
    body: 'WTW.UPDATE_SUCCESS_MESSAGE',
    type: NotificationMessageType.SUCCESS
  }

  private readonly destroy$ = new Subject();
  private readonly breadcrumbConfig: BreadCrumbConfig = {
    elements: [
      {
        translationKey: "WEBSITE_TRANSLATOR.PAGE_TITLE",
        routerLink: [ROUTE_MAP.WEBSITE_TRANSLATOR.MAIN]
      },
      {
        translationKey: "WEBSITE_TRANSLATOR.BREADCRUMB_CREATE"
      }
    ]
  }
  private lastPageTitle: string;
  private srcLangSubscription: Subscription;
  private trgLangSubscription: Subscription;

  /**
   * Creates an instance of ConfiguratorComponent.
   * @param websiteTranslatorService - The website translator service.
   * @param integrationDataService - The integration data service.
   * @param formBuilder - The form builder.
   * @param headerService - The header service.
   * @param route - The activated route.
   * @param router - The router.
   * @param dialog - The material dialog.
   * @param translate - The translate service.
   * @param notificationService - The notification service.
   * @param languageAvailabilityService - The language availability service.
   */
  constructor(
    private readonly websiteTranslatorService: WebsiteTranslatorService,
    private readonly integrationDataService: IntegrationDataService,
    private readonly formBuilder: UntypedFormBuilder,
    private readonly headerService: HeaderService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private languageAvailabilityService: LanguageAvailabilityService
  ) { }

  /**
   * Gets the domain form groups.
   * @returns The form array representing domain form groups.
   */
  get domainFormGroups() {
    return this.widgetForm.get(this.enteredDomains) as UntypedFormArray;
  }

  /**
   * Gets the last domain state.
   * @returns True if the last domain form group is valid or not touched, otherwise false.
   */
  get lastDomainState() {
    return (this.domainFormGroups.controls[this.domainFormGroups.controls.length - 1] as UntypedFormGroup)?.controls?.valid
      || !(this.domainFormGroups.controls[this.domainFormGroups.controls.length - 1] as UntypedFormGroup)?.controls?.touched
  }

  /**
   * Gets the selected target languages with domains form array.
   * @returns The form array representing selected target languages with domains.
   */
  get selectedTrgsWithDomains(): FormArray {
    return this.widgetForm.controls.selectedTrgsWithDomains as FormArray;
  }

  /**
   * Initializes component and sets necessery params.
   */
  ngOnInit(): void {
    this.translate.onLangChange.pipe(takeUntil(this.destroy$)).subscribe((lang) => {
      this.currentLanguage = lang.lang;
    })

    this.setBreadcrumb();
    this.widgetLocationUrl = new URL('assets/widget.js', this.origin).toString();
    this.clientID = this.route.snapshot.params['id'];
    this.isEditMode = !!this.clientID;
    if (this.isEditMode) {
      this.editorUrl = ROUTE_MAP.WEBSITE_TRANSLATOR.DETAILS.TEXT_EDITOR + '/' + this.clientID

      this.generateWidgetScript()
      this.translate.stream(['WTW.GENERAL_INTEGRATION.1_CODE']).pipe(takeUntil(this.destroy$))
        .subscribe(value => {
          this.generalIntegrationCode1 = value['WTW.GENERAL_INTEGRATION.1_CODE'];
        });

      setTimeout(() => {
        document.getElementById('edit-title')?.scrollIntoView({ behavior: 'smooth', block: 'end' })
      });
    }

    this.widgetForm = new FormGroup({
      enteredDomains: new FormArray([this.createDomainFormControl()]),
      selectedTrgLanguages: new FormControl({ value: [], disabled: true }, Validators.required),
      selectedSrcLang: new FormControl('', Validators.required),
      enteredWebsiteName: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150), this.whitespacesOnlyValidator()]),
      selectedTrgsWithDomains: new FormArray([])
    });

    this.languageAvailabilityService.getIntegrations().subscribe((response) => {
      this.sourceLangList = this.languageAvailabilityService.sourceLangList;

      if (this.isEditMode) {
        const integrations = response.websiteIntegrations.configurations;

        this.getWebsiteIntegration(integrations);
      }
    });

    this.srcLangSubscription = this.widgetForm.controls.selectedSrcLang.valueChanges.subscribe(() => {
      if (!this.isEditMode) {
        this.widgetForm.get(this.selectedTrgLanguages)?.reset();
        this.previousSelectedTrgLanguages = [];
        this.selectedTrgsWithDomains.clear();
      }
    });

    this.addLangToEngineComp()
  }

  /**
   * Finds current integration.
   * @param integrations array of integrations
   */
  getWebsiteIntegration(integrations: Array<Website>): void {

    const integration = integrations.find(integration => integration.id === this.clientID);

    if (integration) {
      const languages: string[] = [];
      integration.languages.forEach(language => {
        languages.push(language.trgLang.toUpperCase());
        this.selectedTrgsWithDomains.push(this.createTableRow(language.trgLang, language.domain))
      });

      this.widgetForm.patchValue({
        [this.enteredWebsiteName]: integration.name,
        [this.selectedSrcLang]: integration.srcLang.toUpperCase(),
        [this.selectedTrgLanguages]: languages,
      })


      this.setTargetLanguages();

      //patch first domain and add input fields for all other
      integration.siteUrls.forEach((url: string, index: number) => {
        if (index < 1) {
          this.widgetForm.controls.enteredDomains = this.formBuilder.array([this.createDomainFormControl(url)])
        } else {
          this.domainFormGroups.push(this.createDomainFormControl(url));
        }
      })
    }
    this.widgetForm.get(this.selectedSrcLang)?.disable();
  }

  /**
   * This method subscribes to the changes in the form values of the `widgetForm`. When any form value changes,
   * it updates the `previousSelectedTrgLanguages` array with the currently selected target languages.
   */
  ngAfterViewInit() {
    this.widgetFormSubscription = this.widgetForm.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      const trgs: string[] = [];

      this.selectedTrgsWithDomains.controls.forEach((control: AbstractControl) => {
        trgs.push(control.get('trgLang')?.value);
      });

      this.previousSelectedTrgLanguages = trgs;
    });
  }

  /**
   * Adds a new target language to the engine component.
   */
  addLangToEngineComp(): void {
    const trgs: string[] = [];

    this.selectedTrgsWithDomains.controls.forEach((control: AbstractControl) => {
      trgs.push(control.get('trgLang')?.value);
    });

    this.previousSelectedTrgLanguages = trgs // vajag noteikt, kad forma visa ielādēta, citādāk sačakarējas

    this.trgLangSubscription = this.widgetForm.controls.selectedTrgLanguages.valueChanges.subscribe((selectedLanguages: string[]) => {
      // compare the current selected languages with the previous state
      const addedLanguage = selectedLanguages?.find(lang => !this.previousSelectedTrgLanguages.includes(lang));
      const removedLanguage = this.previousSelectedTrgLanguages.find((lang: string) => !selectedLanguages?.includes(lang));

      if (addedLanguage) {
        const newFormGroup = this.createTableRow(addedLanguage, null);
        this.selectedTrgsWithDomains.push(newFormGroup);
      }

      if (removedLanguage) {
        const selectedTrgsWithDomainsindex = this.selectedTrgsWithDomains.controls.findIndex(control => control.value.trgLang === removedLanguage);
        this.selectedTrgsWithDomains.removeAt(selectedTrgsWithDomainsindex);
      }

      // update previous trglanguage state
      this.previousSelectedTrgLanguages = selectedLanguages?.slice()

    });
  }

  /**
   * Adds a new domain form group to the list of entered domains.
   * It creates a new domain form control and pushes it to the form array.
   */
  addDomainFormGroup(): void {
    this.domainFormGroups.push(this.createDomainFormControl());

    // timeout needed to be able to focus on newly created input element
    setTimeout(() => {
      this.domainField.last.nativeElement.focus();
    }, 0);
  }

  /**
   * Removes or clears a domain form group from the form array.
   * @param i - The index of the domain form group to remove or clear.
   */
  removeOrClearDomain(i: number): void {
    if (this.domainFormGroups.length > 1) {
      this.domainFormGroups.removeAt(i);
    } else {
      this.domainFormGroups.reset();
    }
  }


  /**
   * Creates a form group representing a table row for target language with domain.
   * @param trgLang - The target language.
   * @param domain - The domain.
   * @returns The form group representing the table row.
   */
  createTableRow(trgLang: string, domain: string | null): FormGroup {
    return new FormGroup({
      trgLang: new FormControl(trgLang),
      domain: new FormControl(domain, [
        Validators.required
      ]),
    });
  }

  /**
   * Creates a form control for domain input.
   * @param domain - The initial domain value.
   * @returns The form control representing the domain input.
   */
  createDomainFormControl(domain?: string): UntypedFormControl {
    return new UntypedFormControl(domain ? domain : '', [
      Validators.required,
      Validators.maxLength(2048),
      Validators.pattern('^(http:\\/\\/|https:\\/\\/).*'),
      this.domainOnlyOneValidator(),
      this.whitespacesOnlyValidator()
    ])
  }

  /**
   * Sets the target languages based on the selected source language.
   */
  setTargetLanguages(): void {
    const selectedSrcLang = this.widgetForm.value[this.selectedSrcLang];
    this.targetLangList = this.languageAvailabilityService.searchInfoBySrcLang(selectedSrcLang);
    this.widgetForm.get(this.selectedTrgLanguages)?.enable();
  }

  /**
   * Handles the form submission.
   */
  onSubmit(): void {
    this.widgetForm.get(this.enteredWebsiteName)?.markAsTouched();
    this.widgetForm.get(this.selectedSrcLang)?.markAsTouched();
    this.widgetForm.get(this.selectedTrgLanguages)?.markAsTouched();
    this.domainFormGroups.controls.forEach((control: AbstractControl) => {
      control.markAsTouched();
    })

    // stop here if form is invalid
    if (this.widgetForm.valid) {
      if (this.isEditMode) {
        this.updateIntegration();
      } else {
        this.addIntegration();
      }
    }
  }

  /**
   * Deletes integration.
   */
  deleteIntegration(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        id: this.clientID
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.websiteTranslatorService.deleteWebsiteIntegration(this.clientID).subscribe({
          next: () => {
            this.integrationDataService.deleteIntegration(this.clientID);
            this.router.navigate([ROUTE_MAP.WEBSITE_TRANSLATOR.MAIN]);
          },
          error: () => {
            this.notificationService.sendDefaultErrorMessage();
          }
        });
      }
    });
  }

  /**
   * Handles the cancel action.
   */
  onCancel() {
    this.widgetForm.reset();
  }

  /**
   * Lifecycle hook called when the component is about to be destroyed.
   * Clears up subscriptions.
   */
  ngOnDestroy(): void {
    this.removeBreadcrumb();

    this.destroy$.next(null);
    this.destroy$.complete();

    if (this.srcLangSubscription) {
      this.srcLangSubscription.unsubscribe();
    }

    if (this.trgLangSubscription) {
      this.trgLangSubscription.unsubscribe();
    }

    if (this.widgetFormSubscription) {
      this.widgetFormSubscription.unsubscribe();
    }
  }

  /**
   * Validator function to check if a control value consists of whitespaces only.
   * @returns The validator function.
   */
  private whitespacesOnlyValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;

      if (!value) {
        return null;
      }

      if (control.value?.trim().length === 0) {
        return { consistsOfWhitespacesOnly: true };
      }
      return null;
    }
  }

  /**
   * Validator function to check if the domain contains only one URL (no commas at the end).
   * @returns The validator function.
   */
  private domainOnlyOneValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      if (!value) { return null; }

      const hasSeparator = /^[^,]+$/u.test(value);

      return hasSeparator ? null : { domainOnlyOne: true };
    }
  }

  /**
   * Adds integration.
   */
  private addIntegration() {
    const enteredDomainsArray = this.widgetForm.get(this.enteredDomains)?.value;


    this.websiteTranslatorService.addWebsiteIntegration(
      {
        srcLang: this.widgetForm.get(this.selectedSrcLang)?.value,
        languages: this.selectedTrgsWithDomains.value,
        siteUrls: enteredDomainsArray,
        name: this.widgetForm.get(this.enteredWebsiteName)?.value,
      }
    ).subscribe({
      next: (response: Website) => {
        this.integrationDataService.addIntegration(response);
        this.languageAvailabilityService.requireIntegrationsUpdate();
        this.router.navigate([ROUTE_MAP.WEBSITE_TRANSLATOR.DETAILS.SETTINGS, response.id]);
      },
      error: () => {
        this.notificationService.sendDefaultErrorMessage();
      }
    })
  }


  /**
   * Updates the website integration with the provided data from form.
   */
  private updateIntegration(): void {
    const enteredDomainsArray = this.widgetForm.get(this.enteredDomains)?.value;
    this.websiteTranslatorService.updateWebsiteIntegration(
      this.clientID,
      {
        srcLang: this.widgetForm.get(this.selectedSrcLang)?.value,
        languages: this.selectedTrgsWithDomains.value,
        siteUrls: enteredDomainsArray,
        name: this.widgetForm.get(this.enteredWebsiteName)?.value,
      }
    ).subscribe({
      next: (response: Website) => {
        this.notificationService.sendMessage(this.successMessage);
        this.integrationDataService.updateIntegration(response);
        this.languageAvailabilityService.requireIntegrationsUpdate();
      },
      error: () => {
        this.notificationService.sendDefaultErrorMessage();
      }
    })
  }

  /**
   * Generates translator inclusion script.
   */
  private generateWidgetScript(): void {
    this.generalIntegrationResultScript = `<script type="text/javascript" src="${this.widgetLocationUrl}"></script>
<script>
        WebsiteTranslator.Options.api.clientId = "${this.clientID}";
        WebsiteTranslator.Options.api.url = "${location.origin}";
        WebsiteTranslator.Options.api.version = 2;
        WebsiteTranslator.Initialize()
</script>`;
  }

  /**
   * Sets the breadcrumb configuration.
   */
  private setBreadcrumb() {
    this.lastPageTitle = this.headerService.pageTitleLocalizationKey;
    this.headerService.setBreadcrumbConfig(this.breadcrumbConfig);
  }

  /**
   * Removes the breadcrumb configuration.
   */
  private removeBreadcrumb() {
    this.headerService.setBreadcrumbConfig();
    this.headerService.setPageTitleLocalizationKey(this.lastPageTitle);
  }
}
