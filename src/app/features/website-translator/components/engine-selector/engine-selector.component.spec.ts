import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineSelectorComponent } from './engine-selector.component';

describe('EngineSelectorComponent', () => {
  let component: EngineSelectorComponent;
  let fixture: ComponentFixture<EngineSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineSelectorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EngineSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
