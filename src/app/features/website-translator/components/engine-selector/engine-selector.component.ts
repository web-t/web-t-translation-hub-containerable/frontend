import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TrgLangWithDomains } from '../website-translator-list/models/trglang-with-domains.model';
import { AbstractControl, FormGroup, UntypedFormArray } from '@angular/forms';
import { LanguageAvailabilityService } from 'src/app/services/language-availability.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MissingLanguage } from '../website-translator-list/models/missing-language.model';
import { Language } from '../website-translator-list/models/language.model';

/**
 * Component representing the engine selector used in the website translator widget.
 * This component is responsible for displaying the available target languages and their corresponding engines,
 * as well as handling the selection of target languages and domains.
 */
@Component({
  selector: 'app-engine-selector',
  templateUrl: './engine-selector.component.html',
  styleUrls: ['./engine-selector.component.scss']
})
export class EngineSelectorComponent implements OnInit, OnDestroy {
  @Input() widgetForm: FormGroup;
  srcLang: string;
  selectedTargetLanguages: Array<string> = [];
  displayedColumns: string[] = ['language', 'engine'];
  domainsList: TrgLangWithDomains[];
  clientID: string;
  notSupportedLanguagesOrDomains: MissingLanguage | undefined;
  missingDomains: Language[] | undefined;

  private trgLanguagesSubscription: Subscription;

  /**
   * Constructor for the EngineSelectorComponent.
   * @param languageAvailabilityService - The service used to check language availability.
   * @param route - The activated route service used for accessing the current route information.
   */
  constructor(
    private languageAvailabilityService: LanguageAvailabilityService,
    private route: ActivatedRoute
  ) { }

  /**
   * @returns the selected target languages with domains as an untyped form array.
   */
  get selectedTrgsWithDomains() {
    return this.widgetForm.controls.selectedTrgsWithDomains as UntypedFormArray;
  }

  /**
   * Lifecycle hook that is called after the component is created.
   * Initializes the component by setting up the selected target languages,
   * retrieving language availability data, and subscribing to changes in the selected target languages.
   */
  ngOnInit(): void {
    this.clientID = this.route.snapshot.params['id'];
    this.languageAvailabilityService.getIntegrations().subscribe((data) => {
      const notSupportedLanguagesOrDomains = data.notSupportedLanguagesOrDomains;
      this.notSupportedLanguagesOrDomains = notSupportedLanguagesOrDomains.find(element => element.id === this.clientID);
      this.missingDomains = data.notSupportedLanguagesOrDomains.find(element => element.id === this.clientID)?.missingDomainElementArray;
    });

    this.srcLang = this.widgetForm.controls.selectedSrcLang.value;
    this.setSelectedTargetLanguagesCorrectly()

    this.trgLanguagesSubscription = this.widgetForm.controls.selectedTrgLanguages.valueChanges.subscribe(() => {
      this.setSelectedTargetLanguagesCorrectly();
    })
  }

  /**
   * Helper function to set the selected target languages correctly.
   * Extracts the target languages and their domains from the selectedTrgsWithDomains form control
   * and updates the selectedTargetLanguages and domainsList arrays.
   */
  setSelectedTargetLanguagesCorrectly() {
    const trgs: string[] = [];
    const domains: string[] = [];

    this.selectedTrgsWithDomains.controls.forEach((control: AbstractControl) => {
      trgs.push(control.get('trgLang')?.value.toUpperCase());
      domains.push(control.get('domain')?.value);
    });

    this.selectedTargetLanguages = trgs;
    this.domainsList = this.languageAvailabilityService.findDomainsByTargetLanguage(this.srcLang, this.selectedTargetLanguages);
  }

  /**
   * Check if a given target language is missing or not supported for translation.
   * @param element - The target language to check.
   * @returns - True if the target language is missing or not supported, false otherwise.
   */
  isMissing(element: string) {
    // eslint-disable-next-line jsdoc/require-jsdoc
    return (this.missingDomains && this.missingDomains.some((item: { trgLang: string; }) => item.trgLang.toUpperCase() === element))
      || (this.notSupportedLanguagesOrDomains && this.notSupportedLanguagesOrDomains.missingLanguages.includes(element));
  }

  /**
   * Lifecycle hook that is called before the component is destroyed.
   * Unsubscribes from the target language subscription if it exists.
   */
  ngOnDestroy() {
    if (this.trgLanguagesSubscription) {
      this.trgLanguagesSubscription.unsubscribe();
    }
  }

  /**
   * Delete a target language from the selected target languages.
   * @param trgLang - The target language to remove.
   */
  deleteLang(trgLang: string) {
    const selectedLanguages = this.widgetForm.get('selectedTrgLanguages')?.value;
    const trgLangIndex = selectedLanguages.indexOf(trgLang);

    if (trgLangIndex !== -1) {
      const updatedLanguages = [...selectedLanguages];
      updatedLanguages.splice(trgLangIndex, 1);

      this.widgetForm.controls.selectedTrgLanguages.setValue(updatedLanguages);
    }
  }
}
