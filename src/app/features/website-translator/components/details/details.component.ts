import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { ROUTE_MAP } from 'src/app/core/constants/route-map.const';
import { HeaderService } from 'src/app/services/header.service';
import { IntegrationDataService } from 'src/app/services/integration-data.service';
import { BreadCrumbConfig } from 'src/app/shared/components/breadcrumb/models/breadcrumb-config';
import { TldTabConfig } from 'src/app/shared/components/tld-tabs/models/tld-tab-config.model';
import { Website } from '../website-translator-list/models/website.model';
import { LanguageAvailabilityService } from 'src/app/services/language-availability.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { NotificationMessage } from '@tilde-nlp/ngx-common';
import { NotificationMessages } from '../translation-editor/constants/notification-messages.const';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';


/**
 * Parent component for integration details pages.
 * This component is responsible for displaying the details (as parent component) of a specific website translator integration,
 * including the associated tabs and breadcrumb navigation.
 */
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {
  readonly iconNamespace = 'WEBSITE_TRANSLATOR';
  tabConfig: TldTabConfig = {
    items: []
  };
  breadcrumbConfig: BreadCrumbConfig;
  oldConfig: TldTabConfig;
  id: string;
  name: string;
  notSupportedLanguages: string[] | undefined;

  private readonly destroy$ = new Subject();
  private readonly reviewTemplateMessage: NotificationMessage = NotificationMessages.reviewTemplateMessage;

  /**
   * Constructor for the DetailsComponent.
   * @param header - The header service used for managing the header configuration.
   * @param route - The activated route service used for accessing the current route information.
   * @param integrationDataService - The service used to retrieve integration data.
   * @param languageAvailabilityService - The service used to check language availability.
   * @param notificationService - The notification service used to display messages.
   * @param configService - The app configuration service used to get support URLs.
   */
  constructor(
    public header: HeaderService,
    private route: ActivatedRoute,
    private readonly integrationDataService: IntegrationDataService,
    private languageAvailabilityService: LanguageAvailabilityService,
    private notificationService: NotificationService,
    private configService: AppConfigService
  ) { }

  /**
   * @returns the help center URL from the app configuration.
   */
  get helpCenterUrl() {
    return this.configService.config.support.helpCenterUrl;
  }

  /**
   * Lifecycle hook that is called after the component is created.
   * Retrieves the website ID from the activated route, sets up the tab configuration,
   * creates the breadcrumb configuration, and checks for missing languages or domains.
   */
  ngOnInit(): void {
    this.id = this.route.firstChild?.snapshot.params['id'];

    this.integrationDataService.integrationList$.pipe(takeUntil(this.destroy$))
      .subscribe((integrations: Array<Website>) => {
        this.name = integrations.find(integration => integration.id === this.id)?.name ?? '';
        if (this.name) {
          this.createBreadcrumbConfig();
        }
      });

    this.tabConfig.items = [
      {
        localizationKey: "TABS.GO_BACK",
        route: ROUTE_MAP.WEBSITE_TRANSLATOR.MAIN,
        icon: 'arrow_back',
      },
      {
        localizationKey: "TABS.TEXT_EDITOR_TAB_NAME",
        route: `${ROUTE_MAP.WEBSITE_TRANSLATOR.DETAILS.TEXT_EDITOR}/${this.id}`
      },
      {
        localizationKey: "TABS.SETTINGS_TAB_NAME",
        route: `${ROUTE_MAP.WEBSITE_TRANSLATOR.DETAILS.SETTINGS}/${this.id}`
      },
      {
        localizationKey: "TABS.HELP_TAB_NAME",
        route: this.helpCenterUrl,
        externalLink: true,
        icon: 'help_outlined',
        firstItemToTheRight: true
      }
    ];

    this.oldConfig = this.header.config;
    this.header.setConfig(this.tabConfig);

    this.createBreadcrumbConfig();
    this.checkIfThereIsMissingLanguageOrDomain();
  }

  /**
   * Check if there are missing languages or domains for the website and display a notification if needed.
   * Uses the LanguageAvailabilityService to retrieve the integrations and checks if the current website's ID
   * matches any of the IDs in the list of not supported languages or domains.
   */
  checkIfThereIsMissingLanguageOrDomain() {
    this.languageAvailabilityService.getIntegrations().subscribe(result => {
      const isMissingLanguagesOrDomains = result.notSupportedLanguagesOrDomains.some(element => element.id === this.id);

      if (isMissingLanguagesOrDomains) {
        this.notificationService.sendMessage(this.reviewTemplateMessage);
      }

    })
  }

  /**
   * Lifecycle hook that is called before the component is destroyed.
   * Restores the original header configuration, clears the breadcrumb configuration,
   * and completes the component destruction subject.
   */
  ngOnDestroy(): void {
    this.header.setConfig(this.oldConfig)
    this.header.setBreadcrumbConfig();
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  /**
   * Create the breadcrumb configuration based on the website name.
   * Sets up the breadcrumb configuration with two elements: the "Main" tab and the current website name.
   * The "Main" tab links back to the main website translator page, and the website name serves as a label.
   */
  private createBreadcrumbConfig() {
    this.breadcrumbConfig = {
      elements: [
        {
          translationKey: "TABS.MAIN_TAB_NAME",
          routerLink: [ROUTE_MAP.WEBSITE_TRANSLATOR.MAIN]
        },
        {
          translationKey: this.name
        }
      ]
    }

    this.header.setBreadcrumbConfig(this.breadcrumbConfig);
  }
}
