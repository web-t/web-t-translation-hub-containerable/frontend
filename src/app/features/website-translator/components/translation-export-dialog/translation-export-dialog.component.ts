import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SegmentContentType } from '../translation-editor/models/segment-content-type.enum';
import { SegmentTranslationOrigin } from '../translation-editor/models/segment-translation-origin.enum';
import { SegmentTranslationStatus } from '../translation-editor/models/segment-translation-status.enum';
import { SegmentFilterType } from '../translation-editor/models/segment-filter-type.enum';
import { NotificationMessage } from '@tilde-nlp/ngx-common';
import { NotificationMessages } from '../translation-editor/constants/notification-messages.const';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from 'src/app/core/services/notification.service';
import { WebsiteTranslatorService } from 'src/app/services/website-translator.service';
import { WebsiteList } from '../website-translator-list/models/website-list.model';
import { SegmentListFilterForExport } from '../translation-editor/models/segment-list-filter-for-export.model';
import { MatSelectChange } from '@angular/material/select';

/**
 * Component responsible for filtering translations and exporting them in xliff file format
 */
@Component({
  selector: 'app-translation-export-dialog',
  templateUrl: './translation-export-dialog.component.html',
  styleUrls: ['./translation-export-dialog.component.scss']
})
export class TranslationExportDialogComponent {
  readonly localizationKey = 'WIDGET.TEXT_EDIT.';
  filter: SegmentListFilterForExport = {} as SegmentListFilterForExport;
  targetLangList: Array<string> = [];
  currentLanguage: string;
  resultCount: number;
  
  exportInProgress = false;
  selectedLanguages = new FormControl();
  textFilterChip = [
    { name: "text", value: "", localizationKey: "CONTAINS" },
    { name: "url", value: "", localizationKey: "URL" },
  ];
  filterChip = [
    { name: SegmentContentType[SegmentContentType.Text], active: false, localizationKey: "TEXT" },
    { name: SegmentContentType[SegmentContentType.SEO], active: false, localizationKey: "SEO" },
    { name: SegmentTranslationOrigin[SegmentTranslationOrigin.User], active: false, localizationKey: "USER" },
    { name: SegmentTranslationOrigin[SegmentTranslationOrigin.MachineTranslation], active: false, localizationKey: "MT" },
    { name: SegmentTranslationStatus[SegmentTranslationStatus.New], active: false, localizationKey: "NEW" },
    { name: SegmentTranslationStatus[SegmentTranslationStatus.Confirmed], active: false, localizationKey: "CONFIRMED" },
  ];
  
  //by default "All" options are selected
  selectedDropdownFilters: { [key: string]: string; } = {
    [SegmentFilterType.CONTENT_TYPE]: SegmentContentType[SegmentContentType.All],
    [SegmentFilterType.ORIGIN]: SegmentTranslationOrigin[SegmentTranslationOrigin.All],
    [SegmentFilterType.STATUS]: SegmentTranslationStatus[SegmentTranslationStatus.All],
  }
  
  filterOptions = [
    {
      name: SegmentFilterType.CONTENT_TYPE,
      options: [
        { title: 'ALL', value: SegmentContentType[SegmentContentType.All] },
        { title: 'TEXT', value: SegmentContentType[SegmentContentType.Text] },
        { title: 'SEO', value: SegmentContentType[SegmentContentType.SEO] }
      ]
    },
    {
      name: SegmentFilterType.ORIGIN,
      options: [
        { title: 'ALL', value: SegmentTranslationOrigin[SegmentTranslationOrigin.All] },
        { title: 'MT', value: SegmentTranslationOrigin[SegmentTranslationOrigin.MachineTranslation] },
        { title: 'USER', value: SegmentTranslationOrigin[SegmentTranslationOrigin.User] }
      ]
    },
    {
      name: SegmentFilterType.STATUS,
      options: [
        { title: 'ALL', value: SegmentTranslationStatus[SegmentTranslationStatus.All] },
        { title: 'NEW', value: SegmentTranslationStatus[SegmentTranslationStatus.New] },
        { title: 'CONFIRMED', value: SegmentTranslationStatus[SegmentTranslationStatus.Confirmed] }
      ]
    }
  ];

  private readonly translationExportErrorMessage: NotificationMessage = NotificationMessages.translationExportErrorMessage;
  private readonly loadingErrorMessage: NotificationMessage = NotificationMessages.loadingErrorMessage;

  /**
   * @param dialogRef MatDialogRef.
   * @param data MAT_DIALOG_DATA received from parent component.
   * @param data.configId Id of website (configuration)
   * @param notificationService - The service for displaying notifications.
   * @param websiteTranslatorService - The service for managing website translations.
   */
  constructor(
    // eslint-disable-next-line jsdoc/require-jsdoc
    public dialogRef: MatDialogRef<TranslationExportDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: { configId: string },
    private readonly notificationService: NotificationService,
    private readonly websiteTranslatorService: WebsiteTranslatorService,
  ) { }

  /**
   * Initializes the component and loads all segments
   */
  ngOnInit(): void {
    this.loadWebsiteConfiguration();
  }

  /**
   * Fires on language selection change and loads segments filtered by selected languages
   * @param e MatSelectChange event
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onLanguageSelection(e: MatSelectChange) {
    this.filter.targetLanguages = this.selectedLanguages.value ?? [];
  }

  /**
   * Finds current integration and creates target language list.
   */
  loadWebsiteConfiguration(): void {
    this.websiteTranslatorService.getAllWebsiteIntegrations().subscribe({
      next: (data: WebsiteList) => {
        const currentWebsite = data.configurations.find(config => config.id === this.data.configId);

        if (currentWebsite) {
          currentWebsite.languages.forEach((langDir) => {
            this.targetLangList.push(langDir.trgLang);
          })
        }
      }
    });
  }

  /**
   * Manages tab key navigation in the filter options dropdown.
   * @param filter - The selected filter option.
   * @param position - The position of the selected filter option in the filter list.
   * @param event - The event object representing the tab key press event.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  manageTabKeyNavigation(filter: any, position: number, event: Event) {
    if (filter !== this.filterOptions[position]) {
      event.stopPropagation();
    }
  }

  //dropdow chips
  /**
   * Updates the filter chips based on the selected dropdown filters.
   */
  updateFilterChips() {
    // set all false  thne check to true
    this.filterChip.forEach(chip => {
      chip.active = false;
    })

    for (const key in this.selectedDropdownFilters) {
      const selectedFilter = this.selectedDropdownFilters[key];
      const i = this.filterChip.findIndex(chip => {
        return chip.name == selectedFilter;
      })
      if (i === -1) {
        continue;
      }
      this.filterChip[i].active = true;
    }
  }

  /**
   * Called when the filter option is selected for different segment properties.
   * @param filter - The type of filter being selected.
   * @param option - The selected option for the filter.
   */
  onFilterSelect(filter: string, option: string) {
    this.selectedDropdownFilters[filter] = option;

    this.updateFilterChips();

    if (option in SegmentContentType && filter === SegmentFilterType.CONTENT_TYPE) {
      if (option === SegmentContentType[SegmentContentType.All]) {
        delete this.filter.seo;
      } else if (SegmentContentType[SegmentContentType.Text] === option) {
        this.filter.seo = false;
      } else if (SegmentContentType[SegmentContentType.SEO] === option) {
        this.filter.seo = true;
      }
    } else if (option in SegmentTranslationOrigin && filter === SegmentFilterType.ORIGIN) {
      if (option === SegmentTranslationOrigin[SegmentTranslationOrigin.All]) {
        delete this.filter.origin;
      } else {
        this.filter.origin = Object.values(SegmentTranslationOrigin).indexOf(option);
      }
    } else if (option in SegmentTranslationStatus && filter === SegmentFilterType.STATUS) {
      if (option === SegmentTranslationStatus[SegmentTranslationStatus.All]) {
        delete this.filter.status;
      } else {
        this.filter.status = Object.values(SegmentTranslationStatus).indexOf(option);
      }
    }
  }

  /**
   * Method which fetches exported translation csv file content and downloads it
   */
  exportTranslations() {
    this.exportInProgress = true;
    this.websiteTranslatorService.exportTranslations(this.data.configId, this.filter).subscribe({
      next: (data: Blob) => {
        const tempAnchorTag = document.createElement('a');
        document.body.appendChild(tempAnchorTag);
        
        const fileName = `translations_${this.data.configId}_${this.filter.targetLanguages}.xliff`;
        const url = window.URL.createObjectURL(data);

        tempAnchorTag.href = url;
        tempAnchorTag.download = fileName;
        tempAnchorTag.click();    
    
        URL.revokeObjectURL(url);
        document.body.removeChild(tempAnchorTag);
        this.exportInProgress = false;
      },
      error: () => {
        this.exportInProgress = false;
        this.dialogRef.close();
        this.notificationService.sendMessage(this.translationExportErrorMessage);
      }
    })
  }
}
