import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationExportDialogComponent } from './translation-export-dialog.component';

describe('TranslationExportDialogComponent', () => {
  let component: TranslationExportDialogComponent;
  let fixture: ComponentFixture<TranslationExportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslationExportDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TranslationExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
