import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PretranslationDialogComponent } from './pretranslation-dialog.component';

describe('PretranslationDialogComponent', () => {
  let component: PretranslationDialogComponent;
  let fixture: ComponentFixture<PretranslationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PretranslationDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PretranslationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
