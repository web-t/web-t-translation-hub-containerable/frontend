/** Pretranslation request parameters */
export interface PretranslationRequest {
    /**
     * ID of configuration
     */
    configurationId: string;
    /**
     * Domain of website to pretranslate
     */
    domain: string;
    /**
     * Selected list of language codes to pretranslate website into
     */
    targetLanguages: string[];
}
  