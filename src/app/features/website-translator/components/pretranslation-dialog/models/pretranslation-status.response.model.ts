import { PretranslationStatus } from "../../translation-editor/enums/pretranslation-status.enum";

/** Pretranslation request parameters */
export interface PretranslationStatusResponse {
    /**
     * Amount of pretranslated website urls
     */
    pretranslatedUrls: number;
    /**
     * Total amount of website urls
     */
    totalUrls: number;
    /**
     * General pretranslation status
     */
    status: PretranslationStatus;
}