/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NotificationMessage } from '@tilde-nlp/ngx-common';
import { NotificationService } from 'src/app/core/services/notification.service';
import { WebsiteTranslatorService } from 'src/app/services/website-translator.service';
import { NotificationMessages } from '../translation-editor/constants/notification-messages.const';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { PretranslationRequest } from './models/pretranslation-request.model';

/**
 * Represents pretranslation modal dialog for selecting pretranslation settings and starting the process
 */
@Component({
  selector: 'app-pretranslation-dialog',
  templateUrl: './pretranslation-dialog.component.html',
  styleUrls: ['./pretranslation-dialog.component.scss']
})
export class PretranslationDialogComponent implements OnInit {
  readonly selectedDomain = "selectedDomain";
  readonly selectedTrgLanguages = "selectedTrgLanguages";

  form: UntypedFormGroup;
  websiteIsAvailable: boolean;

  private readonly pretranslationCouldNotStartMessage: NotificationMessage = NotificationMessages.pretranslationCouldNotStartMessage;

  /**
   * @param dialogRef MatDialogRef.
   * @param data MAT_DIALOG_DATA received from parent component.
   * @param data.websiteId Id of configuration
   * @param data.domainList Domain list of configuration
   * @param data.targetLanguages Target language list of configuration
   * @param websiteTranslatorService - The service for managing website translations.
   * @param notificationService - The service for displaying notifications.
   */
  constructor(
    public dialogRef: MatDialogRef<PretranslationDialogComponent>, 
      // eslint-disable-next-line jsdoc/require-jsdoc
    @Inject(MAT_DIALOG_DATA) public data: { websiteId: string, domainList: string[], targetLanguages: string[] },
    private readonly websiteTranslatorService: WebsiteTranslatorService,
    private readonly notificationService: NotificationService
  ) 
  { }

  /**
   * Initializes the component
   */
  ngOnInit(): void {
    this.form = new FormGroup({
      selectedDomain: new FormControl('', Validators.required),
      selectedTrgLanguages: new FormControl([], Validators.required),
    });
  }

  /**
   * Starts website pretranslation
   */
  startWebsitePretranslation() {
    const pretranslationRequest: PretranslationRequest = {
      configurationId: this.data.websiteId,
      domain: this.form.value[this.selectedDomain],
      targetLanguages: this.form.value[this.selectedTrgLanguages]
    };

    this.websiteTranslatorService.startWebsitePretranslation(pretranslationRequest).subscribe(
      {
        next: (response) => {
          if (!response) {
            this.notificationService.sendMessage(this.pretranslationCouldNotStartMessage);
          }

          this.dialogRef.close();
        },
        error: () => {
          this.notificationService.sendMessage(this.pretranslationCouldNotStartMessage);

          this.dialogRef.close();
        },
      }
    );
  }
}
