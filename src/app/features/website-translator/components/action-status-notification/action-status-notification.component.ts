import { Component, EventEmitter, Input, Output } from '@angular/core';
import { StatusNotificationDetails } from '../translation-editor/models/status-notification-details.model';

/**
 * Component responsible for showing status information about specific action (for example, translation import)
 */
@Component({
  selector: 'app-action-status-notification',
  templateUrl: './action-status-notification.component.html',
  styleUrls: ['./action-status-notification.component.scss']
})
export class ActionStatusNotificationComponent {
  @Input() statusNotificationDetails: StatusNotificationDetails;
  @Output() dismissStatusNotification = new EventEmitter();
}
