import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionStatusNotificationComponent } from './action-status-notification.component';

describe('ActionStatusNotificationComponent', () => {
  let component: ActionStatusNotificationComponent;
  let fixture: ComponentFixture<ActionStatusNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionStatusNotificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActionStatusNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
