import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';
import { TranslationProvider } from '../models/translation-provider.model';

/**
 * This service is responsible for managing translation providers and their configurations.
 * It provides methods for creating a new translation provider, fetching the configuration
 * of an existing provider, and checking if a provider is already set.
 * @param http - Angular's HttpClient for making HTTP requests.
 * @param configService - The service providing access to the application's configuration.
 * @see {@link HttpClient} - Angular's HttpClient service for making HTTP requests.
 * @see {@link AppConfigService} - The service providing access to the application's configuration.
 * @see {@link TranslationProvider} - Interface representing a translation provider.
 */
@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  isProviderSet: boolean;

  private readonly apiUrl = this.configService.config.apiUrl;
  private provider: TranslationProvider;

  // eslint-disable-next-line jsdoc/require-jsdoc
  constructor(private http: HttpClient, private configService: AppConfigService) { }

  /**
   * Gets the URL for accessing the configuration of a translation provider.
   *
   * This method constructs and returns the URL for accessing the configuration of a translation provider.
   * It is used in the createProvider and getConfiguration methods to send HTTP requests to the backend
   * API for creating a provider and fetching the configuration, respectively.
   * @returns The URL for accessing the configuration of a translation provider.
   */
  get configurationUrl() {
    return `${this.apiUrl}/configurationservice/provider`;
  }

  /**
   * Creates/updates a new translation provider.
   *
   * This method sends a POST request to the backend API to create a new translation provider with the
   * provided data. Upon a successful response, the provider object is stored locally in the service
   * to avoid making additional API calls to fetch the same data.
   * @param data - The data representing the new translation provider to be created.
   * @returns An Observable of TranslationProvider representing the newly created provider.
   */
  createProvider(data: TranslationProvider): Observable<TranslationProvider> {
    return this.http
      .post<TranslationProvider>(this.configurationUrl, data)
      .pipe(tap((provider: TranslationProvider) => (this.provider = provider)));
  }


  /**
   * Fetches the configuration of the current translation provider.
   *
   * This method sends a GET request to the backend API to fetch the configuration of the current
   * translation provider. If the provider object is already available locally in the service (i.e.,
   * it has been fetched before), it returns an Observable of the stored provider object using the of
   * operator. If the provider object is not available locally, it makes an HTTP request to fetch the
   * data from the API and stores it locally for future use.
   * @returns An Observable of TranslationProvider representing the configuration of the translation provider.
   */
  getConfiguration(): Observable<TranslationProvider> {
    if (this.provider) {
      return of(this.provider)
    }
    return this.http.get<TranslationProvider>(this.configurationUrl).pipe(tap((provider: TranslationProvider) => this.provider = provider));
  }

}
