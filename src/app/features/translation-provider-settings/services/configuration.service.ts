import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';
import { TranslationProvider } from '../models/translation-provider.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  isProviderSet: boolean;

  private readonly apiUrl = this.configService.config.apiUrl;
  private provider: TranslationProvider;

  constructor(private http: HttpClient, private configService: AppConfigService) { }

  get configurationUrl() {
    return `${this.apiUrl}/configurationservice/provider`;
  }

  createProvider(data: TranslationProvider): Observable<TranslationProvider> {
    return this.http
      .post<TranslationProvider>(this.configurationUrl, data)
      .pipe(tap((provider: TranslationProvider) => (this.provider = provider)));
  }

  getConfiguration(): Observable<TranslationProvider> {
    if(this.provider){
      return of(this.provider)
    }
    return this.http.get<TranslationProvider>(this.configurationUrl).pipe(tap((provider: TranslationProvider) => this.provider = provider));
  }

}
