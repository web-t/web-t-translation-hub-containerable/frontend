import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslationProviderSettingsComponent } from './translation-provider-settings.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NotificationMessageModule } from '@tilde-nlp/ngx-common';
import { MatTooltipModule } from '@angular/material/tooltip';




@NgModule({
  declarations: [TranslationProviderSettingsComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatRadioModule,
    MatFormFieldModule,
    FormsModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    NotificationMessageModule,
    MatInputModule,
    MatButtonModule,
  ],
  exports: [TranslationProviderSettingsComponent]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class TranslationProviderSettingsModule { }
