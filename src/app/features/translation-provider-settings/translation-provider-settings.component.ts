import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { NotificationMessage, NotificationMessageType } from '@tilde-nlp/ngx-common';
import { finalize, Subject, Subscription, takeUntil } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { LanguageAvailabilityService } from 'src/app/services/language-availability.service';
import { WebsiteTranslatorService } from 'src/app/services/website-translator.service';
import { openCloseAnimation } from 'src/assets/animations';
import { MTTranslationProviderType } from './enums/mt-translation-provider-type.enum';
import { TranslationProvider } from './models/translation-provider.model';
import { ProviderService } from './services/provider.service';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';

/**
 * Component for managing translation provider settings.
 *
 * This component allows the user to manage translation provider settings. It provides a form for configuring
 * the translation provider type, URL, authentication options (API key or basic authentication), and other
 * relevant options for setting up translation services. The component interacts with the `ConfigurationService`,
 * `NotificationService`, `WebsiteTranslatorService`, and `LanguageAvailabilityService` to handle API requests,
 * display notifications, and update the list of available languages.
 * @see {@link ProviderService} - Service for managing translation provider configurations.
 * @see {@link NotificationService} - Service for displaying notifications to the user.
 * @see {@link WebsiteTranslatorService} - Service for managing website translation data.
 * @see {@link LanguageAvailabilityService} - Service for managing the availability of languages for translation.
 */
@Component({
  selector: 'app-translation-provider-settings',
  templateUrl: './translation-provider-settings.component.html',
  styleUrls: ['./translation-provider-settings.component.scss'],
  animations: [openCloseAnimation],
})
export class TranslationProviderSettingsComponent implements OnInit, OnDestroy {
  isLoading = false;
  hidePassword = true;
  type: NotificationMessageType;
  showCustomProviderForm: boolean;
  howToSetupText: string;
  howToSetUpLocalizationParams: unknown;

  readonly eTranslation = MTTranslationProviderType.ETRANSLATION;
  readonly customProvider = MTTranslationProviderType.CUSTOM;

  readonly basicAuthFormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  readonly baseUrlControl = new FormControl('', [Validators.required])
  readonly apiKeyControl = new FormControl('', [Validators.required])

  readonly translationProviderForm = new FormGroup({
    type: new FormControl(MTTranslationProviderType.ETRANSLATION, { nonNullable: true }),
    url: this.baseUrlControl,
    auth: new FormGroup({
      basic: this.basicAuthFormGroup,
      apiKey: this.apiKeyControl
    }),
  });

  private detectFormValueChangesSubscription: Subscription;

  private readonly destroy$ = new Subject();

  private readonly checkProviderMessage: NotificationMessage = {
    body: 'TRANSLATION_PROVIDER.NOTIFICATION_REASON.CHECK_PROVIDER_INFO',
    title: 'TRANSLATION_PROVIDER.NOTIFICATION_TITLE.CHECK_PROVIDER_INFO',
    type: NotificationMessageType.WARNING
  };

  private readonly wrongProviderChangeMessage: NotificationMessage = {
    body: 'TRANSLATION_PROVIDER.NOTIFICATION_REASON.MT_ENGINES_LOAD_FAIL',
    title: 'TRANSLATION_PROVIDER.NOTIFICATION_TITLE.MT_ENGINES_LOAD_FAIL',
    type: NotificationMessageType.ERROR
  };

  private readonly successProviderChangeMessage: NotificationMessage = {
    title: 'WTW.UPDATE_SUCCESS_MESSAGE_TITLE',
    body: 'TRANSLATION_PROVIDER.NOTIFICATION_REASON.MT_ENGINES_CHANGE_SUCCESS',
    type: NotificationMessageType.SUCCESS
  };

  private readonly howToSetupCustomProviderText = 'TRANSLATION_PROVIDER.HOW_TO_SETUP_CUSTOM';
  private readonly howToSetupETranslationText = 'TRANSLATION_PROVIDER.HOW_TO_SETUP_E_TRANSLATION';

  // eslint-disable-next-line jsdoc/require-jsdoc
  constructor(
    private providerService: ProviderService,
    private notificationService: NotificationService,
    private websiteTranslatorService: WebsiteTranslatorService,
    private languageAvailabilityService: LanguageAvailabilityService,
    private readonly configService: AppConfigService
  ) { }

  /**
   * Gets the `username` form control.
   * @returns The `username` form control instance.
   */
  get username() { return this.translationProviderForm.controls.auth.controls.basic.controls.username; }

  /**
   * Gets the `password` form control.
   * @returns The `password` form control instance.
   */
  get password() { return this.translationProviderForm.controls.auth.controls.basic.controls.password; }

  /**
   * Gets the `providerType` form control.
   * @returns The `providerType` form control instance.
   */
  get providerType() { return this.translationProviderForm.controls.type; }

  /**
   * Gets the `apiKey` form control.
   * @returns The `apiKey` form control instance.
   */
  get apiKey() { return this.translationProviderForm.controls.auth.controls.apiKey }

  /**
   * Gets the `url` form control.
   * @returns The `url` form control instance.
   */
  get url() { return this.translationProviderForm.controls.url }


  /**
   * Initializes the component.
   *
   * This method is called when the component is being initialized. It sets the default values for various form fields
   * and calls the `patchProvider` method to fetch and display the current translation provider configuration.
   */
  ngOnInit(): void {
    this.patchProvider();
  }


  /**
   * Subscribes to form value changes to detect if any changes are made to the provider settings.
   *
   * This method subscribes to the `valueChanges` observable of the `translationProviderForm` and detects if any changes
   * are made to the provider settings. If changes are detected, a warning notification is displayed to prompt the user
   * to check the provider information before saving the changes.
   */
  detectIfFormValueChanges() {
    const initialValue = this.translationProviderForm.value;
    const basic = this.basicAuthFormGroup.value;
    const apikey = this.apiKeyControl.value;
    const baseUrl = this.baseUrlControl.value;

    if ((apikey && baseUrl) || (basic.password && basic.username)) {
      this.detectFormValueChangesSubscription =
        this.translationProviderForm.valueChanges
          .pipe(takeUntil(this.destroy$))
          .subscribe((changedValue) => {
            if (changedValue !== initialValue) {
              this.notificationService.sendMessage(this.checkProviderMessage);
              this.detectFormValueChangesSubscription.unsubscribe();
            }
          });
    }
  }

  /**
   * Handles the form submission when the user saves the translation provider settings.
   *
   * This method is called when the user submits the form to save the translation provider settings. If the form is valid,
   * it calls the `createTranslationProvider` method to create the translation provider based on the user's configuration.
   */
  onSubmit(): void {
    if (this.translationProviderForm.valid) {
      this.createTranslationProvider();
    }
  }

  /**
   * Handles the change event when the user selects a different translation provider type.
   *
   * This method is called when the user selects a different translation provider type (e.g., eTranslation or custom)
   * using the radio buttons in the form. It updates the display of the form fields based on the selected provider type
   * and changes the localization text for the "How to set up" link accordingly.
   * @param event - The MatRadioChange event containing the selected value (translation provider type).
   */
  radioChange(event: MatRadioChange) {
    this.checkProviderType(event.value);
    this.howToSetupTexts();
  }

  /**
   * Cleans up resources and unsubscribes from observables when the component is destroyed.
   *
   * This method is called when the component is being destroyed. It unsubscribes from any active subscriptions to avoid
   * memory leaks and ensure that no further updates are attempted after the component is destroyed.
   */
  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  /**
   * Updates instruction texts based on selected provider type.
   *
   * This method checks currently selected type and updates localization texts accordingly.
   */
  private howToSetupTexts() {
    if (this.showCustomProviderForm) {
      this.howToSetupText = this.howToSetupCustomProviderText;
      this.howToSetUpLocalizationParams = { howToSetUpUrl: this.configService.config.support.howToSetUpUrl };
    } else {
      this.howToSetupText = this.howToSetupETranslationText;
      this.howToSetUpLocalizationParams = { howToSetUpUrl: this.configService.config.support.howToSetUpUrl };
    }
  }

  /**
   * Fetches the current translation provider configuration and updates the form fields.
   *
   * This method fetches the current translation provider configuration from the `ConfigurationService` and updates
   * the form fields to display the retrieved values. It also subscribes to the `valueChanges` of the form to detect
   * if any changes are made to the provider settings.
   */
  private patchProvider() {
    this.providerService
      .getConfiguration()
      .pipe(takeUntil(this.destroy$))
      .subscribe({
        next: (data) => {
          this.checkProviderType(data.type);

          this.translationProviderForm.patchValue(data);

          if (!this.detectFormValueChangesSubscription) {
            this.detectIfFormValueChanges();
          }
          this.howToSetupTexts();
        },
        error: () => {
          this.notificationService.sendDefaultErrorMessage();
        },
      });
  }

  /**
   * Creates a new translation provider based on the user's configuration and updates the language list.
   *
   * This method is called when the user submits the form to save the translation provider settings. It sends a request
   * to the `ConfigurationService` to create a new translation provider with the specified configuration. Upon successful
   * creation, it displays a success notification.
   *  If an error occurs, an error notification is displayed.
   */
  private createTranslationProvider() {
    this.isLoading = true;

    const observer = {
      next: () => {
        this.checkLanguageListResponse();
        this.languageAvailabilityService.requireIntegrationsUpdate();
      },
      error: () => {
        this.notificationService.sendDefaultErrorMessage();
        this.isLoading = false;
      },
    };

    this.providerService
      .createProvider(this.translationProviderForm.value as TranslationProvider)
      .pipe(takeUntil(this.destroy$))
      .subscribe(observer);
  }

  /**
   * Checks the response from the language list API to check if provider is configured correctly.
   */
  private checkLanguageListResponse(): void {
    this.websiteTranslatorService
      .getLanguageList()
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe({
        next: () => {
          this.notificationService.sendMessage(
            this.successProviderChangeMessage
          );
        },
        error: () => {
          this.notificationService.sendMessage(
            this.wrongProviderChangeMessage
          );
        },
      });
  }


  /**
   * Checks the selected translation provider type and updates the form fields accordingly.
   *
   * This method is called when the user selects a different translation provider type (e.g., eTranslation or custom)
   * using the radio buttons in the form. It updates the display of the form fields based on the selected provider type.
   * @param type - The selected translation provider type (e.g., MTTranslationProviderType.ETRANSLATION or MTTranslationProviderType.CUSTOM).
   */
  private checkProviderType(type: MTTranslationProviderType): void {
    if (type === MTTranslationProviderType.CUSTOM) {
      this.showCustomProviderForm = true;
      this.enableCustomProviderFields();
    } else if (type === MTTranslationProviderType.ETRANSLATION) {
      this.showCustomProviderForm = false;
      this.enableETranslationFields();
    }
  }

  /**
   * Enables form fields for custom translation provider settings.
   *
   * This method is called when the user selects the custom translation provider type. It enables the URL and API key
   * form fields and disables the basic authentication form fields.
   */
  private enableCustomProviderFields() {
    this.url.enable();
    this.apiKey.enable();
    this.basicAuthFormGroup.disable();
  }

  /**
   * Enables form fields for eTranslation provider settings.
   *
   * This method is called when the user selects the eTranslation provider type. It enables the basic authentication
   * form fields and disables the URL and API key form fields.
   */
  private enableETranslationFields() {
    this.url.disable();
    this.apiKey.disable();
    this.basicAuthFormGroup.enable();
  }
}
