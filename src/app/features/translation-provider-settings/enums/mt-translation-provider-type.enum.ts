/** MT provider type. */
export enum MTTranslationProviderType {
  CUSTOM = 'Custom',
  ETRANSLATION = 'ETranslation'
}
