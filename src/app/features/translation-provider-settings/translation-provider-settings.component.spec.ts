import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationProviderSettingsComponent } from './translation-provider-settings.component';

describe('TranslationProviderSettingsComponent', () => {
  let component: TranslationProviderSettingsComponent;
  let fixture: ComponentFixture<TranslationProviderSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslationProviderSettingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TranslationProviderSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
