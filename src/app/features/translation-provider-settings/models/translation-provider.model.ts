import { MTTranslationProviderType } from "../enums/mt-translation-provider-type.enum"
import { TranslationProviderAuth } from "./translation-provider-auth.model"

/** Translation provider configuration model. */
export interface TranslationProvider {
  /** Translation provider id. If not set, provider is not yet stored. */
  id?: string,
  /** Type */
  type: MTTranslationProviderType;
  /** Provider url */
  url: string;
  /** Authentication */
  auth: TranslationProviderAuth;
}
