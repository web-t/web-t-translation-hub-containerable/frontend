import { Basic } from "./basic.model"

/** Translation provider authentication model. */
export interface TranslationProviderAuth {
  /** Etranslation auth configuration. */
  basic: Basic
  /** Custom provider auth configuration. */
  apiKey: string
}
