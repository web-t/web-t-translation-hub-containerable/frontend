import { Language } from "./language.model"

export interface ProviderConfiguration {
    id: string
    srcLang: string
    languages: Language[]
}