/** Basic authentication model for etranslation provider. */
export interface Basic {
  /** Username */
  username: string;
  /** Password */
  password: string
}
