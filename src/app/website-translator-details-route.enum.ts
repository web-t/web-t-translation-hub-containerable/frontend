/** Website translator detail routes. */
export enum WebsiteTranslatorDetailRoutes {
  DETAILS = "details",
  TEXT_EDITOR = "text-editor",
  SETTINGS = "edit"
}
