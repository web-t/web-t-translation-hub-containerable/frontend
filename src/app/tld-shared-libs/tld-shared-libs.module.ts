import { ModuleWithProviders, NgModule } from '@angular/core';
import { CONFIG_TOKEN } from '@tilde-nlp/ngx-config';



/**
 * Configures @tilde-nlp library providers
 */
@NgModule({

})
export class TldSharedLibsModule {
  // eslint-disable-next-line jsdoc/require-jsdoc
  static forRoot(): ModuleWithProviders<TldSharedLibsModule> {
    return {
      ngModule: TldSharedLibsModule,
      providers: [
        {
          provide: CONFIG_TOKEN,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          useFactory: () => { return (window as any)[CONFIG_TOKEN]; }
        }
      ]
    };
  }
}

