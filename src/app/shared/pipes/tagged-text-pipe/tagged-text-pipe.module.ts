import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaggedTextPipe } from './tagged-text.pipe';



@NgModule({
  declarations: [
    TaggedTextPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TaggedTextPipe
  ]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class TaggedTextPipeModule { }
