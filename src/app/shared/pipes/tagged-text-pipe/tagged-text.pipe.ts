import { Pipe, PipeTransform } from '@angular/core';
import { SuggestionEditor } from '@tilde-nlp/suggestion-editor';

/**
 * Pipe to transform tagged text into a suggestion editor.
 * It takes the tagged text, processes it using the `SuggestionEditor` from the `@tilde-nlp/suggestion-editor`
 * library, and returns the suggestion editor's HTML.
 */
@Pipe({
  name: 'taggedText'
})
export class TaggedTextPipe implements PipeTransform {

  /**
   * Transforms the tagged text into a suggestion editor.
   * @param text The tagged text to be transformed into a suggestion editor.
   * @param container The container element where the suggestion editor will be rendered.
   * @param textToHighlight Optional. The text to be highlighted in the suggestion editor.
   * @returns The suggestion editor's HTML as a result of transforming the tagged text.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  transform(text: string, container: any, textToHighlight?: string): any {
    return this.createSuggestion(container, text, textToHighlight);
  }

  /**
   * Creates a suggestion editor from the tagged text and renders it in the provided container.
   * @param container The container element where the suggestion editor will be rendered.
   * @param suggestionHTML The tagged text to be transformed into a suggestion editor.
   * @param textToHighlight Optional. The text to be highlighted in the suggestion editor.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private createSuggestion(container: any, suggestionHTML: string, textToHighlight?: string) {
    const editor = new SuggestionEditor(
      suggestionHTML,
      true,
      undefined,
      true
    )

    if (textToHighlight) {
      editor.highlightText(textToHighlight);
    }

    setTimeout(() => {
      container.replaceChildren(editor.getDomRoot());
    }, 0);

  }
}
