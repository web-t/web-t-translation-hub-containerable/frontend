/** Breadcrumb item model */
export interface BreadcrumbElement {
  /** Title */
  translationKey: string;
  /** Routerlink where to direct user on click */
  routerLink?: Array<string>;
  /** Custom callback function on click */
  callback?: () => void;
}
