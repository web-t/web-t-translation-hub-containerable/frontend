import { BreadcrumbElement } from './breadcrumb-element';

/** Breadcrumb configuration model */
export interface BreadCrumbConfig {
  /** Breadcrumb elements */
  elements: Array<BreadcrumbElement>;
}
