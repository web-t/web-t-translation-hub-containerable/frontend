import { Component, Input } from '@angular/core';

import { BreadCrumbConfig } from './models/breadcrumb-config';
import { BreadcrumbElement } from './models/breadcrumb-element';

/**
 * Component to display a breadcrumb navigation.
 */
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent {
  /**
   * Input property to provide the configuration for the breadcrumb.
   */
  @Input() config: BreadCrumbConfig;

  /**
   * Handles the click event on a breadcrumb element.
   * If the element has a callback function, it will be executed.
   * @param element - The clicked breadcrumb element.
   */
  elementClicked(element: BreadcrumbElement) {
    if (element.callback) {
      element.callback();
    }
  }
}
