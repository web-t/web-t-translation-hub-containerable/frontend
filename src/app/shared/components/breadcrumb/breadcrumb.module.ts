import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { BreadcrumbComponent } from './breadcrumb.component';

@NgModule({
  declarations: [BreadcrumbComponent],
  imports: [CommonModule, FlexLayoutModule, RouterModule, TranslateModule],
  exports: [BreadcrumbComponent]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class BreadcrumbModule { }
