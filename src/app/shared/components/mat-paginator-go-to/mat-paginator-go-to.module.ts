import { NgModule } from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import { MatPaginatorGoToComponent } from './mat-paginator-go-to.component';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [MatPaginatorGoToComponent],
  imports: [
    FormsModule,
    MatPaginatorModule,
    MatInputModule,
    TranslateModule
  ],
  exports: [MatPaginatorGoToComponent]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class MatPaginatorGoToModule { }
