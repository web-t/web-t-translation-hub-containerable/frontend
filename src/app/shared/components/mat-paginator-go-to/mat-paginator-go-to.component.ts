import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

/**
 * A custom paginator component with "go to page" functionality for Angular Material Paginator.
 */
@Component({
  selector: 'app-mat-paginator-go-to',
  templateUrl: './mat-paginator-go-to.component.html',
  styleUrls: ['./mat-paginator-go-to.component.scss']
})
export class MatPaginatorGoToComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() disabled = false;
  @Input() hidePageSize = false;
  @Input() pageSizeOptions: number[];
  @Input() showFirstLastButtons = false;
  @Output() page = new EventEmitter<PageEvent>();

  smth: number;
  pageSize: number;
  pageIndex = 0;
  length: number;
  goTo: number;
  pageCount: number;

  /**
   * Sets the page index when it is changed externally.
   * @param pageIndex - The new page index.
   */
  @Input() set pageIndexChanged(pageIndex: number) {
    this.pageIndex = pageIndex;
  }

  /**
   * Sets the length of the paginator when it is changed externally.
   * Also, updates the "go to" value and page count.
   * @param length - The new length of the data.
   */
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input("length") set lengthChanged(length: number) {
    this.length = length;
    this.updateGoto();
  }

  /**
   * Sets the page size when it is changed externally.
   * Also, updates the "go to" value and page count.
   * @param pageSize - The new page size.
   */
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input("pageSize") set pageSizeChanged(pageSize: number) {
    this.pageSize = pageSize;
    this.updateGoto();
  }

  /**
   * Initializes the component and updates the "go to" value and page count.
   */
  ngOnInit() {
    this.updateGoto();
  }

  /**
   * Updates the "go to" value and calculates the page count based on length and page size.
   */
  updateGoto() {
    this.goTo = (this.pageIndex || 0) + 1;
    this.pageCount = Math.ceil(this.length / this.pageSize);
  }

  /**
   * Handles the change event when the paginator is updated.
   * Updates the length, page index, page size, "go to" value, and emits the page event.
   * @param pageEvt - The `PageEvent` object containing updated paginator data.
   */
  paginationChange(pageEvt: PageEvent) {
    this.length = pageEvt.length;
    this.pageIndex = pageEvt.pageIndex;
    this.pageSize = pageEvt.pageSize;
    this.updateGoto();
    this.emitPageEvent(pageEvt);
  }

  /**
   * Handles the "go to" change event.
   * If the entered "go to" page is invalid (out of range), it will be adjusted to the valid range.
   * If the selected page is the same as the current page, no action is taken.
   * @see paginator.pageIndex
   * @see paginator.pageSize
   */
  goToChange() {
    if (this.goTo > this.pageCount) {
      this.goTo = this.pageCount;
    } else if (this.goTo < 1) {
      this.goTo = 1;
    } else if (this.paginator.pageIndex === this.goTo - 1) {
      return;
    }

    const pageIndex = this.paginator.pageIndex;
    this.paginator.pageIndex = this.goTo - 1;
    const event: PageEvent = {
      length: this.paginator.length,
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize,
      previousPageIndex: pageIndex

    };
    this.pageIndex = this.paginator.pageIndex
    this.emitPageEvent(event);
  }

  /**
   * Emits a `PageEvent` using the `page` event emitter.
   * @param pageEvent - The `PageEvent` object to be emitted.
   */
  emitPageEvent(pageEvent: PageEvent) {
    this.page.next(pageEvent);
  }
}
