import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatPaginatorGoToComponent } from './mat-paginator-go-to.component';

describe('MatPaginatorGoToComponent', () => {
  let component: MatPaginatorGoToComponent;
  let fixture: ComponentFixture<MatPaginatorGoToComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatPaginatorGoToComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatPaginatorGoToComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
