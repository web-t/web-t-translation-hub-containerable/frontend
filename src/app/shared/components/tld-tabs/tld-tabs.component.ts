import { Component, Input } from '@angular/core';
import { IsActiveMatchOptions } from '@angular/router';
import { TldTabConfig } from './models/tld-tab-config.model';
import { TldTabItem } from './models/tld-tab-item.model';

/**
 * Component representing a set of tabs with items.
 * Each tab item contains information about the label and the corresponding router link.
 */
@Component({
  selector: 'app-tld-tabs',
  templateUrl: './tld-tabs.component.html',
  styleUrls: ['./tld-tabs.component.scss']
})
export class TldTabsComponent {
  readonly routerLinkActiveOptions: IsActiveMatchOptions = {
    paths: 'exact',
    fragment: 'ignored',
    matrixParams: 'ignored',
    queryParams: 'ignored'
  };
  private _items: TldTabItem[];

  /**
   * @returns tab items.
   */
  get items() { return this._items; }

  /**
   * Sets the configuration for the TLD tabs.
   * @param value - The configuration object containing the TLD tab items.
   */
  @Input() set config(value: TldTabConfig) {
    this._items = value.items ?? [];
  }
}
