import { TldTabItem } from "./tld-tab-item.model";

/** Tab configuration model */
export interface TldTabConfig {
  /** Tab items. */
  items: TldTabItem[];
}
