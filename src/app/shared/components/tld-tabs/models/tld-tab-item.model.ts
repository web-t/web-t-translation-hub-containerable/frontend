/** Tab item configuration model. */
export interface TldTabItem {
  /** Title. */
  localizationKey: string;
  /** Route where tab item is pointing. */
  route: string;
  /**
   * Might be useful to hide some items based on authentication.
   * Since authentication changes when page gets reloaded, each component can set this param based on current moment, when config is created.
   */
  invisible?: boolean;
  /** Material icon name. */
  icon?: string;
  /** Set to true if angular router shouldn't be used. */
  externalLink?: boolean;
  /**
   * if more than one item needs to be placed to the right,
   * add firstItemToTheRight=true for first item in tabConfig.items that needs to be on the other side,
   * f.e., about, help needs to be at the right, you add this to "about" only and about is penultimate in tabConfig.items sequence, last is "help"
   * if just one item, add firstItemToTheRight=true for that.
   */
  firstItemToTheRight?: boolean;
}
