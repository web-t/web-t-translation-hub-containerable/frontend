import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { TldTabsComponent } from './tld-tabs.component';



@NgModule({
  declarations: [
    TldTabsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    TldTabsComponent
  ]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class TldTabsModule { }
