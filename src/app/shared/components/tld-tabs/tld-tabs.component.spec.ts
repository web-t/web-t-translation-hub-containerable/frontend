import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TldTabsComponent } from './tld-tabs.component';

describe('TldTabsComponent', () => {
  let component: TldTabsComponent;
  let fixture: ComponentFixture<TldTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TldTabsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TldTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
