import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationMessageComponent } from './notification-message.component';
import { NotificationMessageModule } from 'ngx-tld-common';



@NgModule({
  declarations: [NotificationMessageComponent],
  imports: [
    CommonModule,
    NotificationMessageModule
  ],
  exports: [NotificationMessageComponent]
})
export class AppNotificationMessageModule { }
