import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NotificationMessage, NotificationMessageType } from 'ngx-tld-common';
import { AppConfigService } from 'src/app/core/configuration/app-config.service';
import { openCloseAnimation } from 'src/assets/animations';
import { ErrorReason } from '../../enums/error-notification/error-reason.enum';
import { ErrorTitle } from '../../enums/error-notification/error-title.enum';
import { SuccessReason } from '../../enums/success-notification/success-reason.enum';
import { SuccessTitle } from '../../enums/success-notification/success-title.enum';
import { WarningReason } from '../../enums/warning-notification/warning-reason.enum';

@Component({
  selector: 'app-notification-message',
  templateUrl: './notification-message.component.html',
  styleUrls: ['./notification-message.component.scss'],
  animations: [
    openCloseAnimation
  ]
})
export class NotificationMessageComponent implements OnInit {
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() onClose: EventEmitter<null> = new EventEmitter();
  @Input() errorTitleKey: ErrorTitle;
  @Input() errorReasonKey: ErrorReason;
  @Input() type: NotificationMessageType;
  @Input() isSuccessMessage = false;

  message: NotificationMessage;
  messages: Array<NotificationMessage> = [];

  readonly localizationKey = 'WIDGET.TEXT_EDIT.';

  private readonly ErrorTitleKeys = {
    [ErrorTitle.LOAD]: `${this.localizationKey}ERROR_TITLE_LOAD`,
    [ErrorTitle.DELETE]: `${this.localizationKey}ERROR_TITLE_DELETE`,
    [ErrorTitle.ADD]: `${this.localizationKey}ERROR_TITLE_ADD`,
    [ErrorTitle.ACCEPT]: `${this.localizationKey}ERROR_TITLE_ACCEPT`,
  };
  private readonly ErrorReasonKeys = {
    [ErrorReason.DEFAULT]: `${this.localizationKey}ERROR_DEFAULT`,
    [ErrorReason.DELETE_NOT_FOUND]: `${this.localizationKey}ERROR_DELETE_NOT_FOUND`,
    [ErrorReason.ADD_NOT_FOUND]: `${this.localizationKey}ERROR_ADD_NOT_FOUND`,
    [ErrorReason.ADD_BAD_REQUEST]: `${this.localizationKey}ERROR_ADD_BAD_REQUEST`,
    [ErrorReason.ACCEPT_NOT_FOUND]: `${this.localizationKey}ERROR_ACCEPT_NOT_FOUND`,
    [ErrorReason.UNAUTHORIZED]: `${this.localizationKey}UNAUTHORIZED`,
  };

  private readonly SuccessReasonKeys = {
    [SuccessReason.DEFAULT]: 'WTW.UPDATE_SUCCESS_MESSAGE'
  };

  private readonly SuccessTitleKeys = {
    [SuccessTitle.SUCCESS]: 'WTW.UPDATE_SUCCESS_MESSAGE_TITLE'
  };

  private readonly WarningReasonKeys = {
    [WarningReason.NO_PROVIDER]: 'TRANSLATION_PROVIDER.ERROR.NO_PROVIDER_SET'
  }

  constructor(private readonly configService: AppConfigService) { }

  get supportEmail() {
    return this.configService.config.support.supportEmail;
  }

  ngOnInit(): void {
    if (this.type === NotificationMessageType.SUCCESS) {
      this.onSuccess()
    } else if (this.type === NotificationMessageType.ERROR) {
      this.onError(this.errorReasonKey, this.errorTitleKey);
    } else if (this.type === NotificationMessageType.WARNING) {
      this.onWarning();
    }
  }

  removeMessage() {
    this.onClose.emit(null);
  }

  private onError(reason: ErrorReason, title?: ErrorTitle): void {
    const message: NotificationMessage = {
      title: title ? this.ErrorTitleKeys[title] : '',
      body: this.ErrorReasonKeys[reason],
      localizationParams: { Email: this.supportEmail },
      type: NotificationMessageType.ERROR
    };

    this.pushAndScroll(message);
  }

  private onSuccess(): void {
    const message: NotificationMessage = {
      title: this.SuccessTitleKeys[SuccessTitle.SUCCESS],
      body: this.SuccessReasonKeys[SuccessReason.DEFAULT],
      localizationParams: { Email: this.supportEmail },
      type: NotificationMessageType.SUCCESS
    };

    this.pushAndScroll(message);
  }

  private onWarning(): void {
    const message: NotificationMessage = {
      title: '',
      body: this.WarningReasonKeys[WarningReason.NO_PROVIDER],
      localizationParams: { Email: this.supportEmail },
      type: NotificationMessageType.WARNING
    };

    this.pushAndScroll(message);
  }

  private pushAndScroll(message: NotificationMessage) {
    this.pushMessage(message);
    document.getElementById('messages')?.scrollIntoView({ block: 'end' });
  }

  private pushMessage(message: NotificationMessage) {
    // pushing only "one of" message in array, so screen is not spammed with messages
    const existingMessage = this.messages.find(msg => msg.title === message.title
      && msg.type === message.type
      && message.body === msg.body
      && JSON.stringify(msg.localizationParams) === JSON.stringify(message.localizationParams));
    if (!existingMessage) {
      this.messages.push(message);
    }
  }

}
