import { Component, Input, OnInit } from '@angular/core';
import { DesktopOnlyConfig } from './desktop-only-config.model';

/**
 * Component to display content that is intended for desktop users only.
 * It receives a configuration object that provides information about the content to display.
 */
@Component({
  selector: 'app-desktop-only',
  templateUrl: './desktop-only.component.html',
  styleUrls: ['./desktop-only.component.scss']
})
export class DesktopOnlyComponent implements OnInit {
  @Input() config: DesktopOnlyConfig;
  descriptionKey: string;
  private readonly defaultKey = 'DESKTOP_ONLY.DESCRIPTION';

  /**
   * @returns the URL for the "Read More" link to an about page, if provided in the configuration.
   */
  get aboutPageUrl() {
    return this.config?.readMoreUrl;
  }

  /**
   * @returns the URL for the "Help Center" link, if provided in the configuration.
   */
  get helpCenterUrl() {
    return this.config?.helpCenterUrl;
  }

  /**
   * Initializes the component and sets the description key based on the configuration or default key.
   */
  ngOnInit(): void {
    this.descriptionKey = this.config?.descriptionKey ?? this.defaultKey;
  }
}
