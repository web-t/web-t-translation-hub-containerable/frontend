/** Desktop only component configuration. */
export interface DesktopOnlyConfig {
  /** Description. */
  descriptionKey?: string;
  /** Read more URL. */
  readMoreUrl: string;
  /** Help center URL. */
  helpCenterUrl: string;
}
