import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { ComponentRef, Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { DesktopOnlyConfig } from './desktop-only-config.model';
import { DesktopOnlyComponent } from './desktop-only.component';

/**
 * Directive to conditionally display content based on the screen size.
 * If the screen size is unsupported (e.g., extra-small or small), the content provided by the
 * `DesktopOnlyConfig` will be displayed using the `DesktopOnlyComponent`. Otherwise, the original
 * content wrapped with this directive will be shown.
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[desktopOnly]'
})
export class DesktopOnlyDirective implements OnInit, OnDestroy {

  @Input() desktopOnly: DesktopOnlyConfig;

  unsupportedScreenSize: string[] = [Breakpoints.XSmall, Breakpoints.Small];

  private readonly destroyed = new Subject<void>();
  private matches!: boolean;

  /**
   * Constructor of the directive.
   * @param templateRef Reference to the template where the directive is used.
   * @param viewContainer Reference to the view container of the template.
   * @param breakpointObserver BreakpointObserver from `@angular/cdk/layout` used to observe
   * the screen size changes.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private breakpointObserver: BreakpointObserver) { }
  // Unsupported breakpoint array. Should be values from @angular/cdk/layout breakpoints.

  /**
   * Lifecycle hook called when the directive is initialized.
   * It subscribes to the breakpoint observer to watch for changes in the screen size and update
   * the view accordingly.
   */
  ngOnInit(): void {
    this.breakpointObserver
      .observe(
        this.unsupportedScreenSize
      )
      .pipe(takeUntil(this.destroyed))
      .subscribe(result => {
        this.updateView(result);
      });
  }

  /**
   * Lifecycle hook called when the directive is destroyed.
   * It completes the `destroyed` subject to unsubscribe from observables.
   */
  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

  /**
   * Updates the view based on the breakpoint state.
   * If the screen size matches an unsupported breakpoint, the desktop-only content will be displayed
   * using the `DesktopOnlyComponent`. Otherwise, the original content wrapped with this directive
   * will be shown.
   * @param result BreakpointState object that contains information about the current breakpoint
   * state.
   */
  private updateView(result: BreakpointState) {
    if (this.matches === result.matches) {
      return;
    }

    this.matches = result.matches;
    this.viewContainer.clear();

    if (result.matches) {
      const component: ComponentRef<DesktopOnlyComponent> = this.viewContainer.createComponent(DesktopOnlyComponent);
      component.instance.config = this.desktopOnly;
    }
    else {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}
