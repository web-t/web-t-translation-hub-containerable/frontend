import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesktopOnlyComponent } from './desktop-only.component';
import { DesktopOnlyDirective } from './desktop-only.directive';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    DesktopOnlyComponent,
    DesktopOnlyDirective
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule
  ],
  exports: [
    DesktopOnlyComponent,
    DesktopOnlyDirective
  ]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class DesktopOnlyModule { }
