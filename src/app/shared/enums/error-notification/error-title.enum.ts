export enum ErrorTitle {
  LOAD = 'LOAD',
  DELETE = 'DELETE',
  ADD = 'ADD',
  ACCEPT = 'ACCEPT'
}
