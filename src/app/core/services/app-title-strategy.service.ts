import { Injectable } from '@angular/core';
import { RouterStateSnapshot, TitleStrategy } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { AppConfigService } from '../configuration/app-config.service';


/**
 * Custom implementation of the Angular TitleStrategy for updating the page title.
 *
 * This service extends the TitleStrategy class provided by Angular and is responsible
 * for updating the page title based on the current router state. It utilizes the
 * TranslateService to translate the page title using the ngx-translate library.
 * @param translateService - The service responsible for handling translations with ngx-translate.
 * @param title - The Angular platform's Title service to update the browser's title.
 * @param config - The service providing access to the application's configuration.
 * @see {@link https://angular.io/api/router/TitleStrategy} - Angular's TitleStrategy class.
 * @see {@link https://github.com/ngx-translate/core} - ngx-translate library for translations.
 * @see {@link Title} - Angular's Title service to update the browser's title.
 * @see {@link AppConfigService} - The service providing access to the application's configuration.
 */
@Injectable()
export class AppTitleStrategy extends TitleStrategy {
  private subscription: Subscription;

  // eslint-disable-next-line jsdoc/require-jsdoc
  constructor(private translateService: TranslateService,
    private readonly title: Title,
    private readonly config: AppConfigService) {
    super();
  }

  /**
   * Overrides the base `updateTitle` method from the TitleStrategy class to update the page title.
   *
   * This method is responsible for updating the page title based on the current router state. It first
   * unsubscribes from any previous subscriptions to avoid memory leaks. It then constructs the translation
   * key for the page title using the `buildTitle` method. If the `buildTitle` method returns a falsy value,
   * indicating that there's no specific translation key for the current route, it falls back to a default
   * translation key ("PAGE_TITLES.MAIN").
   *
   * After obtaining the translation key, it prepares the localization parameters, which includes the
   * application's name from the application's configuration (`configService.config.appName`). It then
   * subscribes to the translation stream of ngx-translate to get the translated page title using the
   * translation key and localization parameters. Finally, it updates the browser's title using the
   * Angular platform's Title service.
   * @param snapshot - The current router state snapshot containing information about the current route.
   * It includes the URL, route parameters, and route data.
   * @see {@link buildTitle} - A helper method to construct the translation key for the page title.
   */
  override updateTitle(snapshot: RouterStateSnapshot): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    const localizationParams = {
      appName: this.config.config.appName
    };

    let title = this.buildTitle(snapshot);
    if (!title) {
      title = "PAGE_TITLES.MAIN";
    }
    this.subscription = this.translateService.stream(title, localizationParams).subscribe((translatedTitle) => {
      this.title.setTitle(translatedTitle);
    })
  }
}
