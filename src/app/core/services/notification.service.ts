import { Injectable } from '@angular/core';
import { NotificationMessage, NotificationMessageType } from '@tilde-nlp/ngx-common';
import { Subject } from 'rxjs';
import { AppConfigService } from '../configuration/app-config.service';

/**
 * Service for displaying notifications and handling messages in the application.
 *
 * This service is responsible for displaying notifications to the user. It uses
 * a Subject to publish notification messages, allowing components to subscribe
 * to new messages and react accordingly. The service also provides methods for
 * sending custom notification messages or a default error message.
 * @param configService - The service providing access to the application's configuration.
 * @see {@link NotificationMessage} - Interface representing a notification message.
 * @see {@link NotificationMessageType} - Enumeration of notification message types.
 * @see {@link Subject} - RxJS Subject used for publishing notification messages.
 * @see {@link AppConfigService} - The service providing access to the application's configuration.
 */
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly messageSubject: Subject<NotificationMessage> = new Subject();
  private readonly defaultErrorMessage: NotificationMessage = {
    body: 'WIDGET.TEXT_EDIT.ERROR_DEFAULT',
    type: NotificationMessageType.ERROR
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  constructor(private readonly configService: AppConfigService) { }

  /**
   * Gets an Observable to subscribe to new notification messages.
   *
   * Components can subscribe to this Observable to receive new notification messages
   * and react accordingly to display the messages to the user.
   * @returns An Observable of NotificationMessage for receiving notification messages.
   */
  onMessage() {
    return this.messageSubject.asObservable();
  }


  /**
   * Sends a custom notification message to be displayed to the user.
   *
   * This method is used to send custom notification messages to the service.
   * The message can include a body and a type, which indicates the type of
   * notification (e.g., success, error, info). The method also ensures that
   * the user is scrolled to the top of the page to ensure the notification
   * is visible.
   * @param message - The custom notification message to be displayed.
   */
  sendMessage(message: NotificationMessage) {
    this.scrollToTop();
    if (!message.localizationParams) {
      message.localizationParams = this.configService.config.support;
    }

    this.messageSubject.next(message);
  }

  /**
   * Sends the default error message to be displayed to the user.
   *
   * This method sends the default error message to the service. The default error
   * message is used when an unexpected error occurs in the application and a specific
   * error message is not provided. It ensures that the user is scrolled to the top
   * of the page to make the error message visible.
   */
  sendDefaultErrorMessage() {
    this.sendMessage(this.defaultErrorMessage);
  }

  /**
   * Scrolls the page to the top.
   *
   * This method is used to scroll the page to the top to ensure that the user sees
   * the notification message when it is displayed. It uses the ".page-wrapper" selector
   * to find the element to scroll. If the element is not found, it scrolls the window to
   * the top.
   */
  private scrollToTop(): void {
    const selector = ".page-wrapper";
    const obj = document.querySelector(selector) ?? window;
    obj.scroll(0, 0);
  }
}
