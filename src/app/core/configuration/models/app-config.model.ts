import { UiLanguage } from "./ui-languages.model";

/**
 * Model for application configuration. Make sure this model is up to date with configuration.json
 */
export interface AppConfig {
  /** Application name. Gets displayed in page title etc. */
  appName: string;
  /** Application version. This gets set when reading config in main.ts */
  version: number;
  /** url, from which to get apps data */
  apiUrl: string,
  /** supported ui languages */
  uiLanguages: Array<UiLanguage>,
  /** Object that stores properties for support */
  support: {
    /** if error occurs, there is an option to contact support */
    supportEmail: string,
    /** url, that leads to about project page */
    aboutPageUrl: string,
    /** url, that leads to help page */
    helpCenterUrl: string,
    /** url, that leads to MT provider setup info page */
    howToSetUpUrl: string
  },
  /**
   * config data for translation import
   */
  translationImport: {
    /**
     * max allowed file size for csv translation file
     */
    maxTranslationImportFileSizeBytes: number
  },
}
