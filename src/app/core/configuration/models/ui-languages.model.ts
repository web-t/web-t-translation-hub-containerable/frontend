/**  Interface for defining configured UI languages. */
export interface UiLanguage {
  /** Language code. */
  code: string,
  /** Languages display name. Usually in native language. */
  displayName: string
}
