import { Inject, Injectable } from '@angular/core';
import { CONFIG_TOKEN } from '@tilde-nlp/ngx-config';
import { AppConfig } from './models/app-config.model';


/**
 * Service created to access app configuration.
 * This service is used as a wrapper for the configuration object to provide typing
 * for the application's configuration. It makes use of the `CONFIG_TOKEN` injection
 * token to retrieve the application's configuration of type `AppConfig`.
 * @param config - The application's configuration object of type `AppConfig`.
 * @see {@link AppConfig} - The interface representing the application's configuration.
 * @see {@link CONFIG_TOKEN} - The injection token used to provide the application's configuration.
 */
@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  // eslint-disable-next-line jsdoc/require-jsdoc
  constructor(@Inject(CONFIG_TOKEN) public readonly config: AppConfig) {
  }
}
