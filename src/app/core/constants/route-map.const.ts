import { TranslationProviderRoutes } from "src/app/features/translation-provider-settings/enums/translation-provider-routes.enum";
import { WebsiteTranslatorDetailRoutes } from "../../website-translator-details-route.enum";
import { WebsiteTranslatorRoutes } from "../../website-translator-routes.enum";

export const ROUTE_MAP = {
    TRANSLATION_PROVIDER: {
        MAIN: TranslationProviderRoutes.MAIN
    },
    WEBSITE_TRANSLATOR: {
        MAIN:  WebsiteTranslatorRoutes.MAIN,
        CREATE: WebsiteTranslatorRoutes.CREATE,
       
        DETAILS: {
            TEXT_EDITOR: `${WebsiteTranslatorDetailRoutes.DETAILS}/${WebsiteTranslatorDetailRoutes.TEXT_EDITOR}`,
            SETTINGS: `${WebsiteTranslatorDetailRoutes.DETAILS}/${WebsiteTranslatorDetailRoutes.SETTINGS}`
        }
    }
}