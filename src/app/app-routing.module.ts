import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './layout/main/main.component';
import { TranslationProviderSettingsComponent } from './features/translation-provider-settings/translation-provider-settings.component';
import { WebsiteTranslatorDetailRoutes } from './website-translator-details-route.enum';
import { WebsiteTranslatorRoutes } from './website-translator-routes.enum';
import { WebsiteTranslatorListComponent } from './features/website-translator/components/website-translator-list/website-translator-list.component';
import { ConfiguratorComponent } from './features/website-translator/components/configurator/configurator.component';
import { DetailsComponent } from './features/website-translator/components/details/details.component';
import { TranslationEditorComponent } from './features/website-translator/components/translation-editor/translation-editor.component';
import { TranslationProviderRoutes } from './features/translation-provider-settings/enums/translation-provider-routes.enum';
import { FeaturesComponent } from './layout/features/features.component';

const routes: Routes = [{
  path: "",
  component: MainComponent,
  children: [
    {
      path: "",
      component: FeaturesComponent,
      children: [
        {
          path: "",
          component: WebsiteTranslatorListComponent,
          title: 'PAGE_TITLES.MAIN'
        },
        {
          path: TranslationProviderRoutes.MAIN, component: TranslationProviderSettingsComponent,
          title: 'PAGE_TITLES.TRANSLATION_PROVIDER'
        },
        {
          path: WebsiteTranslatorRoutes.CREATE, component: ConfiguratorComponent,
          title: 'PAGE_TITLES.CREATE'
        },
        {
          path: WebsiteTranslatorDetailRoutes.DETAILS, component: DetailsComponent,
          children: [
            {
              path: WebsiteTranslatorDetailRoutes.SETTINGS + '/:id', component: ConfiguratorComponent,
              title: 'PAGE_TITLES.SETTINGS'
            },
            {
              path: WebsiteTranslatorDetailRoutes.TEXT_EDITOR + '/:id', component: TranslationEditorComponent,
              title: 'PAGE_TITLES.TEXT_EDITOR'
            },
          ]
        },
      ]
    },
    { path: '**', redirectTo: '' }
  ],
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class AppRoutingModule { }
