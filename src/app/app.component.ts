import { Component, OnInit } from '@angular/core';
import { AppConfigService } from './core/configuration/app-config.service';
import { ROUTE_MAP } from './core/constants/route-map.const';
import { HeaderService } from './services/header.service';
import { TldTabConfig } from './shared/components/tld-tabs/models/tld-tab-config.model';

/**
 * Root component of the application.
 * This component serves as the entry point of the Angular application. It sets up
 * the tab configuration and initializes the header service to display the header
 * with the configured tabs.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  tabConfig: TldTabConfig = {
    items: [],
  }
  pageTitleLocalizationKey = "TABS.PAGE_TITLE";


  /**
   * Creates an instance of AppComponent.
   * @param headerService - The service responsible for managing the header configuration.
   * @param configService - The service providing access to the application's configuration.
   */
  constructor(
    private headerService: HeaderService,
    private configService: AppConfigService
  ) { }

  /**
   * @returns the Help Center URL from the application's configuration.
   */
  get helpCenterUrl() {
    return this.configService.config.support.helpCenterUrl;
  }

  /**
   * @returns  the About Page URL from the application's configuration.
   */
  get aboutPageUrl() {
    return this.configService.config.support.aboutPageUrl;
  }

  /**
   * Lifecycle hook invoked after the component's view has been initialized.
   * Sets up the tab configuration with localized tab names and corresponding routes.
   * Initializes the base configuration for the header service with the tab configuration.
   */
  ngOnInit(): void {

    this.tabConfig.items = [
      {
        localizationKey: "TABS.MAIN_TAB_NAME",
        route: ROUTE_MAP.WEBSITE_TRANSLATOR.MAIN
      },
      {
        localizationKey: "TABS.TRANSLATION_PROVIDER_TAB_NAME",
        route: `${ROUTE_MAP.TRANSLATION_PROVIDER.MAIN}`
      },
      {
        localizationKey: "TABS.ABOUT_TAB_NAME",
        route: this.aboutPageUrl,
        externalLink: true
      },
      {
        localizationKey: "TABS.HELP_TAB_NAME",
        route: this.helpCenterUrl,
        externalLink: true,
        icon: 'help_outlined',
        firstItemToTheRight: true
      }
    ];
    this.initBase(this.tabConfig);
  }

  /**
   * Initializes the base configuration for the header service.
   * @param tabConfig - Tab configuration to be used in the header.
   */
  initBase(tabConfig: TldTabConfig): void {
    this.headerService.setPageTitleLocalizationKey(this.pageTitleLocalizationKey);
    this.tabConfig = tabConfig;
    this.headerService.setConfig(this.tabConfig);
  }


}
