import { TestBed } from '@angular/core/testing';

import { WebsiteTranslatorService } from './website-translator.service';

describe('WebsiteTranslatorService', () => {
  let service: WebsiteTranslatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebsiteTranslatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
