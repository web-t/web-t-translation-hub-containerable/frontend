import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, tap, throwError } from 'rxjs';
import { WebsiteList } from '../features/website-translator/components/website-translator-list/models/website-list.model';
import { Website } from '../features/website-translator/components/website-translator-list/models/website.model';
import { WebsiteTranslatorService } from './website-translator.service';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Service responsible for managing integration data related to the website translator feature.
 * It provides methods to load, add, update, and delete website integrations.
 */
@Injectable({
  providedIn: 'root'
})
export class IntegrationDataService {

  private integrationList = new BehaviorSubject<Array<Website>>([]);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  integrationList$ = this.integrationList.asObservable();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _error: any = undefined;
  private _loading = true;
  private integrations: Array<Website>;

  /**
   * Constructs the IntegrationDataService.
   * @param websiteTranslatorService - The service to interact with the website translator.
   */
  constructor(private websiteTranslatorService: WebsiteTranslatorService) { }

  /**
   * @returns the error state.
   */
  get isError() {
    return this._error;
  }

  /**
   * @returns the loading state.
   */
  get loading() {
    return this._loading;
  }

  /**
   * Loads the website integrations from the server.
   * @returns An observable that emits the loaded website integrations.
   */
  loadIntegrations(): Observable<WebsiteList> {
    return this.websiteTranslatorService.getAllWebsiteIntegrations().pipe(
      tap((data: WebsiteList) => {
        this.integrations = data.configurations;
        this._loading = false;
        this.integrationList.next(this.integrations);
      }),
      catchError((error: HttpErrorResponse) => {
        this._loading = false;
        this._error = error;
        this.integrationList.next([]);
        return throwError(() => error);
      })
    );
  }

  /**
   * Adds a new website integration.
   * @param newWebsite - The website integration to be added.
   */
  addIntegration(newWebsite: Website) {
    this.integrations.unshift(newWebsite);
    this.integrationList.next(this.integrations);
  }

  /**
   * Updates an existing website integration.
   * @param updatedIntegration - The updated website integration.
   */
  updateIntegration(updatedIntegration: Website) {
    const index = this.integrations.findIndex(integration => integration.id === updatedIntegration.id);
    this.integrations[index] = updatedIntegration;
    this.integrationList.next(this.integrations);
  }

  /**
   * Deletes a website integration by its ID.
   * @param id - The ID of the website integration to be deleted.
   */
  deleteIntegration(id: string) {
    const index = this.integrations.findIndex(integration => integration.id === id);
    this.integrations.splice(index, 1);
    this.integrationList.next(this.integrations);
  }
}
