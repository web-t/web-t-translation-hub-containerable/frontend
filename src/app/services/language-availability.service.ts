import { Injectable } from '@angular/core';
import { Observable, catchError, forkJoin, map, shareReplay, throwError } from 'rxjs';
import { WebsiteTranslatorService } from './website-translator.service';
import { LanguageDirectionResponse } from '../features/website-translator/components/website-translator-list/models/language-directions.model';
import { System } from '../features/website-translator/components/website-translator-list/models/system.model';
import { TrgLangWithDomains } from '../features/website-translator/components/website-translator-list/models/trglang-with-domains.model';
import { IntegrationDataService } from './integration-data.service';
import { WebsiteList } from '../features/website-translator/components/website-translator-list/models/website-list.model';
import { MissingLanguage } from '../features/website-translator/components/website-translator-list/models/missing-language.model';
import { NotificationService } from '../core/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Website } from '../features/website-translator/components/website-translator-list/models/website.model';
import { SourceLangsForNotSupported } from '../features/website-translator/components/website-translator-list/models/source-langs-for-not-supported.model';
import { Language } from '../features/website-translator/components/website-translator-list/models/language.model';
import { DomainsForTrgLangWithId } from '../features/website-translator/components/website-translator-list/models/domains-for-trg-lang-with-id.model';

/**
 * Service responsible for managing language availability for website translations.
 * It provides methods to retrieve language information, check for missing languages or domains,
 * and update integration data.
 */
@Injectable({
  providedIn: 'root'
})

export class LanguageAvailabilityService {
  // eslint-disable-next-line jsdoc/require-jsdoc
  notSupported: { id: string; missingLanguages: string[]; }[]
  sourceLangList: string[];
  langDirectionsResponse: System[];

  isReloadRequired = false;

  // eslint-disable-next-line jsdoc/require-jsdoc
  private integrationData: Observable<{ websiteIntegrations: WebsiteList; notSupportedLanguagesOrDomains: MissingLanguage[] }>;

  /**
   * Creates an instance of LanguageAvailabilityService.
   * @param websiteTranslatorService - The WebsiteTranslatorService to fetch language information from the server.
   * @param notificationService - The NotificationService to handle error notifications.
   * @param integrationDataService - The IntegrationDataService to manage website integrations and missing language data.
   */
  constructor(
    private websiteTranslatorService: WebsiteTranslatorService,
    private notificationService: NotificationService, private integrationDataService: IntegrationDataService) { }

  /**
   * Gets the integration data including website integrations and missing languages or domains.
   * If the data is not available or a reload is required, it fetches the data from the server.
   * @returns An observable that emits the integration data.
   */
  // eslint-disable-next-line jsdoc/require-jsdoc
  getIntegrations(): Observable<{ websiteIntegrations: WebsiteList, notSupportedLanguagesOrDomains: MissingLanguage[] }> {
    if (!this.integrationData || this.isReloadRequired) {
      this.integrationData = forkJoin([
        this.websiteTranslatorService.getLanguageList(),
        this.integrationDataService.loadIntegrations(),
      ]).pipe(
        map(res => {
          const integrations = res[1].configurations;
          this.langDirectionsResponse = res[0].languageDirections;

          const sourcelangs = [];
          for (let index = 0; index < integrations.length; index++) {
            const element = integrations[index];
            const list = this.searchInfoBySrcLang(element.srcLang.toUpperCase());
            sourcelangs.push({ srcLang: element.srcLang.toUpperCase(), trgLangs: list })
          }

          const notSupportedLanguagesOrDomains = this.findIfDomainOrLanguageIsMissing(integrations, sourcelangs);
          this.sourceLangList = this.getSrcLangList(res[0]);

          this.isReloadRequired = false;

          return {
            websiteIntegrations: res[1],
            notSupportedLanguagesOrDomains: notSupportedLanguagesOrDomains
          };
        }),
        catchError((error: HttpErrorResponse) => {
          this.notificationService.sendDefaultErrorMessage();
          console.error(error);
          return throwError(() => new Error())
        }),
        shareReplay(1)
      );
    }
    return this.integrationData;
  }

  /**
   * Marks that a reload of integration data is required.
   * This will fetch fresh data from the server when `getIntegrations()` is called next time.
   */
  requireIntegrationsUpdate() {
    this.isReloadRequired = true;
  }

  /**
   * Checks if there are any missing languages or domains for each website integration.
   * @param integrations - An array of Website objects representing website integrations.
   * @param srcLangTargetList - An array of SourceLangsForNotSupported objects containing source language and target language list.
   * @returns An array of MissingLanguage objects representing websites with missing languages or domains.
   */
  findIfDomainOrLanguageIsMissing(integrations: Website[], srcLangTargetList: SourceLangsForNotSupported[]) {
    const notSupportedLanguagesOrDomains: MissingLanguage[] = [];
    const domainsList: DomainsForTrgLangWithId[] = [];

    integrations.forEach((integration) => {
      const matchingSecondObj = srcLangTargetList.find(srcLangElement => srcLangElement.srcLang.toUpperCase() === integration.srcLang.toUpperCase());

      const trgLangList: string[] = [];
      integration.languages.forEach(language => {
        trgLangList.push(language.trgLang.toUpperCase())
      });
      const foundDomains = this.findDomainsByTargetLanguage(integration.srcLang.toUpperCase(), trgLangList);
      domainsList.push({ id: integration.id, domains: foundDomains });

      const matchingDomainItem = domainsList.find(domainElement => domainElement.id === integration.id);

      const missingLanguages: string[] = [];
      const missingDomainElementArray: Language[] = [];

      integration.languages.forEach((lang) => {
        const trgLangExists = matchingSecondObj?.trgLangs.some((trgLangObj: string) => trgLangObj.toUpperCase() === lang.trgLang.toUpperCase());

        let domainExists = false;

        matchingDomainItem?.domains.forEach(domainObj => {
          if (domainObj.domains.includes(lang.domain)) {
            domainExists = true;
          }
        });

        if (!domainExists) {
          missingDomainElementArray.push({ domain: lang.domain, trgLang: lang.trgLang });
        }
        if (!trgLangExists) {
          missingLanguages.push(lang.trgLang);
        }
      });

      if (missingLanguages.length > 0 || missingDomainElementArray.length > 0) {
        if (!integration.id) {
          return;
        }

        notSupportedLanguagesOrDomains.push({ id: integration.id, missingLanguages, missingDomainElementArray });
      }
    })

    return notSupportedLanguagesOrDomains;
  }

  /**
   * Get the list of source languages from the provided data.
   * @param data - The language direction response data.
   * @returns An array containing the unique list of source languages in uppercase.
   */
  getSrcLangList(data: LanguageDirectionResponse) {
    return [...new Set(data.languageDirections.map(system => system.srcLang.toUpperCase()))];
  }

  /**
   * Searches for target languages corresponding to a given source language.
   * @param srcLang - The source language code to search for.
   * @returns An array containing the unique list of target languages in uppercase for the given source language.
   */
  searchInfoBySrcLang(srcLang: string) {
    const sortTrgLangsBySrcLang = this.getTrgsForSrc(srcLang);
    const sortedTrgLangs = this.getUniqueTrgLang(sortTrgLangsBySrcLang);

    return sortedTrgLangs;
  }

  /**
   * Get the target languages for a given source language.
   * @param srcLang - The source language code to filter by.
   * @returns An array containing the list of System objects representing target languages for the given source language.
   */
  getTrgsForSrc(srcLang: string) {
    const sortTrgLangsBySrcLang: System[] = this.langDirectionsResponse.filter(language => language.srcLang.toUpperCase() === srcLang.toUpperCase());
    return sortTrgLangsBySrcLang;
  }

  /**
   * Get the unique list of target languages in uppercase.
   * @param array - The array of System objects to extract target languages from.
   * @returns An array containing the unique list of target languages in uppercase.
   */
  getUniqueTrgLang(array: System[]): string[] {
    return [...new Set(array.map(system => system.trgLang.toUpperCase()))];
  }

  /**
   * Find domains by target language for a given source language and target language list.
   * @param srcLang - The source language code.
   * @param targetLanguages - An array containing the list of target languages.
   * @returns An array of TrgLangWithDomains objects representing target languages with their associated domains.
   */
  findDomainsByTargetLanguage(srcLang: string, targetLanguages: string[]): TrgLangWithDomains[] {
    const srcLangPairs = this.getTrgsForSrc(srcLang);
    const result: TrgLangWithDomains[] = [];

    for (const trgLang of targetLanguages) {
      const domains: string[] = [];

      for (const srcLangPair of srcLangPairs) {
        if (srcLangPair.trgLang.toUpperCase() === trgLang.toUpperCase()) {
          domains.push(srcLangPair.domain);
        }
      }

      result.push({ trgLang, domains });
    }

    return result;
  }


}
