// At some point need to remove this rule and create proper types for all methods
/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfigService } from '../core/configuration/app-config.service';
import { SegmentListFilter } from '../features/website-translator/components/translation-editor/models/segment-list-filter.model';
import { SegmentResponse } from '../features/website-translator/components/translation-editor/models/segment-response.model';
import { LanguageDirectionResponse } from '../features/website-translator/components/website-translator-list/models/language-directions.model';
import { NewTranslation } from '../features/website-translator/components/website-translator-list/models/new-translation.model';
import { Translation } from '../features/website-translator/components/website-translator-list/models/translation.model';
import { WebsiteList } from '../features/website-translator/components/website-translator-list/models/website-list.model';
import { Website } from '../features/website-translator/components/website-translator-list/models/website.model';
import { SegmentListFilterForExport } from '../features/website-translator/components/translation-editor/models/segment-list-filter-for-export.model';
import { PretranslationRequest } from '../features/website-translator/components/pretranslation-dialog/models/pretranslation-request.model';
import { ImportStatus } from '../features/website-translator/components/translation-editor/enums/import-status.enum';
import { PretranslationStatusResponse } from '../features/website-translator/components/pretranslation-dialog/models/pretranslation-status.response.model';

/**
 * Service responsible for handling website translation operations.
 * It provides methods for fetching website integrations, segments,
 * adding translations, accepting translations, and more.
 */
@Injectable({
  providedIn: 'root'
})
export class WebsiteTranslatorService {

  private readonly apiUrl = this.configService.config.apiUrl;

  /**
   * Constructs a new WebsiteTranslatorService instance.
   * @param http - The HttpClient used for making HTTP requests.
   * @param configService - The AppConfigService used for retrieving configuration data.
   */
  constructor(private readonly http: HttpClient, private readonly configService: AppConfigService) { }

  /**
   * @returns the API URL for the Website Translator service.
   */
  get wtApiUrl() {
    return `${this.apiUrl}/configurationservice/configuration`;
  }

  /**
   * @returns the URL for fetching language directions from the MT API Integrator.
   */
  get languageDirectionUrl() {
    return `${this.apiUrl}/mtapiintegrator/translate/language-directions`;
  }

  /**
   * @returns the URL for fetching segments from the Website Translation service.
   */
  get segmentUrl() {
    return `${this.apiUrl}/websitetranslationservice/translate/website`;
  }

  /**
   * @returns the URL for website translation import
   */
  get translationImportUrl() {
    return `${this.apiUrl}/websitetranslationservice/import`;
  }

  /**
   * @returns the URL for website translation export
   */
  get translationExportUrl() {
    return `${this.apiUrl}/websitetranslationservice/export`;
  }

  /**
   * @returns the URL for website pretranslation
   */
  get pretranslationUrl() {
    return `${this.apiUrl}/websitetranslationservice/pretranslate`;
  }
  
  /**
   * Retrieves all website integrations.
   * @returns An observable that emits the list of website integrations.
   */
  getAllWebsiteIntegrations(): Observable<WebsiteList> {
    return this.http.get<WebsiteList>(this.wtApiUrl);
  }

  /**
   * Updates a website integration.
   * @param id - The ID of the website integration to update.
   * @param data - The updated website integration data.
   * @returns An observable that emits the updated website data.
   */
  updateWebsiteIntegration(id: string, data: Website): Observable<Website> {
    return this.http.put<Website>(`${this.wtApiUrl}/${id}`, data);
  }

  /**
   * Adds a new website integration.
   * @param data - The data for the new website integration.
   * @returns An observable that emits the newly created website integration.
   */
  addWebsiteIntegration(data: Website): Observable<Website> {
    return this.http.post<any>(this.wtApiUrl, data);
  }

  /**
   * Deletes a website integration.
   * @param id - The ID of the website integration to delete.
   * @returns An observable that emits the result of the delete operation.
   */
  deleteWebsiteIntegration(id: string): Observable<any> {
    return this.http.delete(`${this.wtApiUrl}/${id}`);
  }

  /**
   * Fetches segments for a website with optional filters.
   * @param websiteId - The ID of the website.
   * @param filters - The filters to apply.
   * @returns An observable that emits the segment response containing the segments and metadata.
   */
  getSegments(websiteId: string, filters: SegmentListFilter): Observable<SegmentResponse> {
    return this.http.get<any>(`${this.segmentUrl}/${websiteId}/segments`, { params: <any>filters });
  }

  /**
   * Adds a new translation for a segment in a specific language.
   * @param websiteId - The ID of the website.
   * @param segmentId - The ID of the segment to translate.
   * @param language - The target language of the translation.
   * @param data - The data for the new translation.
   * @returns An observable that emits the newly created translation.
   */
  addTranslation(websiteId: string, segmentId: number, language: string, data: NewTranslation): Observable<Translation> {
    return this.http.post<any>(`${this.segmentUrl}/${websiteId}/segments/${segmentId}/${language}`, data);
  }

  /**
   * Accepts a translation for a segment in a specific language.
   * @param websiteId - The ID of the website.
   * @param segmentId - The ID of the segment.
   * @param language - The target language of the translation.
   * @param translationId - The ID of the translation to accept.
   * @returns An observable that emits the result of the accept operation.
   */
  acceptTranslation(websiteId: string, segmentId: number, language: string, translationId: number): Observable<any> {
    return this.http.put<any>(`${this.segmentUrl}/${websiteId}/segments/${segmentId}/${language}/translation/${translationId}`, null);
  }

  /**
   * Deletes a segment from a website integration.
   * @param websiteId - The ID of the website.
   * @param segmentId - The ID of the segment to delete.
   * @returns An observable that emits the result of the delete operation.
   */
  deleteSegment(websiteId: string, segmentId: number): Observable<any> {
    return this.http.delete<any>(`${this.segmentUrl}/${websiteId}/segments/${segmentId}`);
  }

  /**
   * Fetches the list of supported language directions.
   * @returns An observable that emits the response containing the supported language directions.
   */
  getLanguageList(): Observable<LanguageDirectionResponse> {
    return this.http.get<LanguageDirectionResponse>(this.languageDirectionUrl);
  }

  /**
   *
   * @param configId Configuration ID of website
   * @param filters Filters to filter out translations for export
   * @returns XLIFF file of translations
   */
  exportTranslations(configId: string, filters: SegmentListFilterForExport) {
    return this.http.get<any>(`${this.translationExportUrl}?configId=${configId}`, {
      params: <any>filters,
      responseType: 'blob' as 'json'
    });
  }

  /**
   * Starts translation import process - parses XLIFF file and saves translations in database
   * @param formData XLIFF file and config id of the website
   * @returns Result of import
   */
  importTranslations(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.translationImportUrl}`, formData);
  }

  /**
   * Updates translation import status
   * @param configId Configuration ID of website
   * @param status Pretranslation status
   * @returns Result of import
   */
  updateImportStatus(configId: string, status: ImportStatus): Observable<any> {
    return this.http.post<any>(`${this.translationImportUrl}/${configId}/import-status?status=${status}`, null);
  }

  /**
   * Retrieves translation import status
   * @param configId Configuration ID of website
   * @returns Result of import
   */
  getImportStatus(configId: string): Observable<any> {
    return this.http.get<any>(`${this.translationImportUrl}/${configId}/import-status`);
  }

  /**
   * Starts website pretranslation process
   * @param pretranslationRequest - Config id, website domain and target languages
   * @returns An observable
   */ 
  startWebsitePretranslation(pretranslationRequest: PretranslationRequest): Observable<any> {
    return this.http.post<any>(`${this.pretranslationUrl}/start-pretranslation`, pretranslationRequest);
  }

  /**
   * Retrieves website pretranslation status
   * @param configurationId - The ID of the website.
   * @returns An observable with PretranslationStatusResponse
   */
  getWebsitePretranslationStatus(configurationId: string): Observable<PretranslationStatusResponse> {
    return this.http.get<PretranslationStatusResponse>(`${this.pretranslationUrl}/${configurationId}/status`);
  }

  /**
   * Starts a retry of website pretranslation
   * @param configurationId - The ID of the website.
   * @returns An observable
   */
  retryWebsitePretranslation(configurationId: string): Observable<any> {
    return this.http.post<any>(`${this.pretranslationUrl}/retry-pretranslation?configurationId=${configurationId}`, null);
  }

  /**
   * Retrieves website pretranslation status
   * @param configurationId - The ID of the website.
   * @returns An observable
   */
  resetWebsitePretranslationStatus(configurationId: string): Observable<any> {
    return this.http.post<any>(`${this.pretranslationUrl}/reset-pretranslation-status?configurationId=${configurationId}`, null);
  }

  /**
   * Cancels website pre-translation
   * @param configurationId - The ID of the website.
   * @returns An observable
   */
  cancelWebsitePretranslation(configurationId: string): Observable<any> {
    return this.http.post<any>(`${this.pretranslationUrl}/cancel-pretranslation?configurationId=${configurationId}`, null);
  }
}
