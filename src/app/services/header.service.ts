import { Injectable } from '@angular/core';
import { BreadCrumbConfig } from '../shared/components/breadcrumb/models/breadcrumb-config';
import { TldTabConfig } from '../shared/components/tld-tabs/models/tld-tab-config.model';

/**
 * Service responsible for managing header-related data and configurations.
 * It provides methods to set and retrieve page title localization key, tab configurations, and breadcrumb configurations.
 */
@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  private _pageTitleLocalizationKey: string;
  private _config: TldTabConfig;
  private _breadcrumbConfig: BreadCrumbConfig;

  /**
   * Gets the localization key for the page title.
   * @returns The localization key for the page title.
   */
  get pageTitleLocalizationKey() {
    return this._pageTitleLocalizationKey;
  }

  /**
   * Gets the breadcrumb configuration for the header.
   * @returns The breadcrumb configuration for the header.
   */
  get breadcrumbConfig() {
    return this._breadcrumbConfig;
  }

  /**
   * Gets the tab configuration for the header.
   * @returns The tab configuration for the header.
   */
  get config() {
    return this._config;
  }

  /**
   * Sets the localization key for the page title.
   * @param key The localization key for the page title.
   */
  setPageTitleLocalizationKey(key: string) {
    this._pageTitleLocalizationKey = key;
  }

  /**
   * Sets the tab configuration for the header.
   * @param config The tab configuration for the header.
   */
  setConfig(config: TldTabConfig) {
    this._config = config;
  }

  /**
   * Sets the breadcrumb configuration for the header.
   * @param config The breadcrumb configuration for the header.
   */
  setBreadcrumbConfig(config?: BreadCrumbConfig) {
    if (config) {
      this._breadcrumbConfig = config;
    }
  }

}
