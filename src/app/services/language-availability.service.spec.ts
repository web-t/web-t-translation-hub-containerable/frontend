import { TestBed } from '@angular/core/testing';

import { LanguageAvailabilityService } from './language-availability.service';

describe('LanguageAvailabilityService', () => {
  let service: LanguageAvailabilityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LanguageAvailabilityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
