import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TldSharedLibsModule } from './tld-shared-libs/tld-shared-libs.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NotificationMessageModule, SortTranslationsModule, MissingTranslationHelper } from '@tilde-nlp/ngx-common';
import { ConfigModule } from '@tilde-nlp/ngx-config';
import { MatMenuModule } from '@angular/material/menu';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainComponent } from './layout/main/main.component';
import { TaggedTextPipeModule } from './shared/pipes/tagged-text-pipe/tagged-text-pipe.module';
import { TldTabsModule } from './shared/components/tld-tabs/tld-tabs.module';
import { TranslationProviderSettingsModule } from './features/translation-provider-settings/translation-provider-settings.module';
import { WebsiteTranslatorModule } from './features/website-translator/website-translator.module';
import { FeaturesComponent } from './layout/features/features.component';
import { AppConfigService } from './core/configuration/app-config.service';
import { TitleStrategy } from '@angular/router';
import { AppTitleStrategy } from 'src/app/core/services/app-title-strategy.service';
import { DesktopOnlyModule } from './shared/desktop-only/desktop-only.module';

/**
  Creates a translation loader for ngx-translate.
 * This factory function is used to create a custom translation loader for ngx-translate.
 * It returns a new instance of TranslateHttpLoader with the specified parameters.
 * The created loader is responsible for loading translation files from the specified URL.
 * @param http - The HttpClient service used to make HTTP requests.
 * @param configService - The service providing access to the application's configuration.
 * @returns A new instance of TranslateHttpLoader configured with the specified URL and version.
 * @see {@link https://github.com/ngx-translate/http-loader} - ngx-translate's HTTP loader for loading translations.
 */
export function createTranslateLoader(http: HttpClient, configService: AppConfigService) {
  return new TranslateHttpLoader(http, `./assets/i18n/`, `.json?v=${configService.config.version}`);
}

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    FeaturesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    TldSharedLibsModule.forRoot(),
    WebsiteTranslatorModule,
    TranslationProviderSettingsModule,
    NotificationMessageModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    SortTranslationsModule,
    TaggedTextPipeModule,
    TldTabsModule,
    ConfigModule.loadConfig(["assets/configuration.json"]),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient, AppConfigService],
      },
      defaultLanguage: 'en',
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MissingTranslationHelper
      }
    }),
    DesktopOnlyModule
  ],
  providers: [{
    provide: TitleStrategy,
    useClass: AppTitleStrategy
  }],
  bootstrap: [AppComponent]
})
// eslint-disable-next-line jsdoc/require-jsdoc
export class AppModule { }
