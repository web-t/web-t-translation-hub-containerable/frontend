import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';

export const routeAnimation = trigger('routeAnimation', [
  transition('void => *', [style({ opacity: 0 }), animate('400ms 150ms ease-in-out', style({ opacity: 1 }))])
]);

export const openCloseAnimation = trigger('openCloseAnimation', [
  transition(':enter', [style({ height: 0 }), animate(500)]),
  transition(':leave', animate(500, style({ height: 0 })))
]);

export const pulseAnimation = trigger('pulse', [
  state("true", style({})),
  state("false", style({})),
  transition('false <=> true', animate('1.5s ease', keyframes(
    [
      style({ offset: 0 }),
      style({ boxShadow: '0 0 0 15px rgb(173, 35, 74, 10%)', offset: 0.7 }),
      style({ boxShadow: '0 0 0 0', offset: 1 }),
    ]
  )))
])
